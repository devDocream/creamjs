define( [
    'util/Date'
],
function datesSpec ( date )
{
    describe( 'checkFormatDate', function instantiation ()
    {

        it( 'European date format : dd-mm-yyyy ', function ()
        {
            expect( date.checkFormatDate( '14-05-2015' ) ).toEqual( true );
            expect( date.checkFormatDate( '14-5-2015' ) ).toEqual( false );
            expect( date.checkFormatDate( '14-30-2015' ) ).toEqual( false );
            expect( date.checkFormatDate( '40-05-2015' ) ).toEqual( false );
            expect( date.checkFormatDate( '1-05-2015' ) ).toEqual( false );
            expect( date.checkFormatDate( '10-05-201' ) ).toEqual( false );
            expect( date.checkFormatDate( '10-05-4010' ) ).toEqual( false );
            expect( date.checkFormatDate( '05-31-2015' ) ).toEqual( false );
        } );

        it( 'European date format with : dd/mm/yyyy', function ()
        {
            expect( date.checkFormatDate( '14/05/2015' ) ).toEqual( true );
            expect( date.checkFormatDate( '14/5/2015' ) ).toEqual( false );
            expect( date.checkFormatDate( '14/30/2015' ) ).toEqual( false );
            expect( date.checkFormatDate( '40/05/2015' ) ).toEqual( false );
            expect( date.checkFormatDate( '1/05/2015' ) ).toEqual( false );
            expect( date.checkFormatDate( '10/05/201' ) ).toEqual( false );
            expect( date.checkFormatDate( '10/05/4010' ) ).toEqual( false );
            expect( date.checkFormatDate( '05/31/2015' ) ).toEqual( false );
        } );

        it( 'US date format : yyyy-mm-dd ', function ()
        {
            expect( date.checkFormatDate( '2015-05-14' ) ).toEqual( true );
            expect( date.checkFormatDate( '2015-5-14' ) ).toEqual( false );
            expect( date.checkFormatDate( '2015-30-14' ) ).toEqual( false );
            expect( date.checkFormatDate( '2015-05-40' ) ).toEqual( false );
            expect( date.checkFormatDate( '2015-1-05' ) ).toEqual( false );
            expect( date.checkFormatDate( '201-05-10' ) ).toEqual( false );
            expect( date.checkFormatDate( '4010-05-10' ) ).toEqual( false );
            expect( date.checkFormatDate( '2015-31-05' ) ).toEqual( false );
        } );

        it( 'US date format with : yyyy/mm/dd', function ()
        {
            expect( date.checkFormatDate( '2015/05/14' ) ).toEqual( true );
            expect( date.checkFormatDate( '2015/5/14' ) ).toEqual( false );
            expect( date.checkFormatDate( '2015/30/14' ) ).toEqual( false );
            expect( date.checkFormatDate( '2015/05/40' ) ).toEqual( false );
            expect( date.checkFormatDate( '2015/05/1' ) ).toEqual( false );
            expect( date.checkFormatDate( '201/05/10' ) ).toEqual( false );
            expect( date.checkFormatDate( '4010/05/10' ) ).toEqual( false );
            expect( date.checkFormatDate( '2015/31/05' ) ).toEqual( false );
        } );

        it( 'Force JS standard date format  : yyyy-mm-dd', function ()
        {
            expect( date.checkFormatDate( '2015/05/14', 'standard' ) ).toEqual( false );
            expect( date.checkFormatDate( '14/05/2015', 'standard' ) ).toEqual( false );
            expect( date.checkFormatDate( '14-05-2015', 'standard' ) ).toEqual( false );
            expect( date.checkFormatDate( '2015-05-14', 'standard') ).toEqual( true );
        } );

    } );

    describe( 'checkValidFormatDate', function instantiation ()
    {

        it( 'JS standard date format  : yyyy-mm-dd', function ()
        {
            expect( date.checkValidFormatDate( '2015/05/14' ) ).toEqual( false );
            expect( date.checkValidFormatDate( '14/05/2015' ) ).toEqual( false );
            expect( date.checkValidFormatDate( '14-05-2015' ) ).toEqual( false );
            expect( date.checkValidFormatDate( '2015-05-14' ) ).toEqual( true );
        } );

    } );

    describe( 'checkValidFormatDateTime', function instantiation ()
    {

        it( 'JS standard date time format  : yyyy-mm-dd h|hh:mm:ss', function ()
        {
            expect( date.checkValidFormatDateTime( '2015-05-14 20:12:56' ) ).toEqual( true );
            expect( date.checkValidFormatDateTime( '2015-5-14 20:12:56' ) ).toEqual( false );
            expect( date.checkValidFormatDateTime( '2015-30-14 20:12:56' ) ).toEqual( false );
            expect( date.checkValidFormatDateTime( '2015-12-14 20:12:56' ) ).toEqual( true );
            expect( date.checkValidFormatDateTime( '2015-12-14 20:1:56' ) ).toEqual( false );
            expect( date.checkValidFormatDateTime( '2015-12-14 20:12:1' ) ).toEqual( false );
            expect( date.checkValidFormatDateTime( '2015-12-14 20:12:70' ) ).toEqual( false );
        } );

        it( 'JS standard date time format  : yyyy-mm-dd h|hh:mm:ss am|pm', function ()
        {
            expect( date.checkValidFormatDateTime( '2015-05-14 12:12:56 pm' ) ).toEqual( true );
            expect( date.checkValidFormatDateTime( '2015-5-14 12:12:56 pm' ) ).toEqual( false );
            expect( date.checkValidFormatDateTime( '2015-30-14 12:12:56 pm' ) ).toEqual( false );
            expect( date.checkValidFormatDateTime( '2015-12-14 12:12:56 pm' ) ).toEqual( true );
            expect( date.checkValidFormatDateTime( '2015-12-14 12:12:70 pm' ) ).toEqual( false );
            expect( date.checkValidFormatDateTime( '2015-12-14 12:12:1 pm' ) ).toEqual( false );
        } );

    } );

    describe( 'checkFormatTime', function instantiation ()
    {
        it( '24h Time format : hh:mm/h:mm ', function ()
        {
            expect( date.checkFormatTime( '12:12' ) ).toEqual( true );
            expect( date.checkFormatTime( '20:14' ) ).toEqual( true );
            expect( date.checkFormatTime( '25:12' ) ).toEqual( false );
            expect( date.checkFormatTime( '5:12' ) ).toEqual( true );
            expect( date.checkFormatTime( '20:70' ) ).toEqual( false );
            expect( date.checkFormatTime( '20:7' ) ).toEqual( false );
            expect( date.checkFormatTime( '2:7' ) ).toEqual( false );
            expect( date.checkFormatTime( '2:07' ) ).toEqual( true );
        } );

        it( '12h Time format : hh:mm|h:mm am|pm ', function ()
        {
            expect( date.checkFormatTime( '12:12 pm' ) ).toEqual( true );
            expect( date.checkFormatTime( '20:14 pm' ) ).toEqual( false );
            expect( date.checkFormatTime( '25:12 pm' ) ).toEqual( false );
            expect( date.checkFormatTime( '5:12 am' ) ).toEqual( true );
            expect( date.checkFormatTime( '20:70 pm' ) ).toEqual( false );
            expect( date.checkFormatTime( '20:7 pm' ) ).toEqual( false );
            expect( date.checkFormatTime( '2:7 am' ) ).toEqual( false );
            expect( date.checkFormatTime( '2:07 am' ) ).toEqual( true );
            expect( date.checkFormatTime( '14:05:25' ) ).toEqual( true );
        } );

        it( '24h Time format with second: hh:mm:ss/h:mm:ss', function ()
        {
            expect( date.checkFormatTime( '12:12:12' ) ).toEqual( true );
            expect( date.checkFormatTime( '20:14:70' ) ).toEqual( false );
            expect( date.checkFormatTime( '25:12:1' ) ).toEqual( false );
        } );

        it( '12h Time format with seconds : hh:mm|h:mm am|pm ', function ()
        {
            expect( date.checkFormatTime( '12:12:56 pm' ) ).toEqual( true );
            expect( date.checkFormatTime( '12:12:70 pm' ) ).toEqual( false );
            expect( date.checkFormatTime( '12:12:1 pm' ) ).toEqual( false );
            expect( date.checkFormatTime( '20:14:70 pm' ) ).toEqual( false );
            expect( date.checkFormatTime( '25:12:1 pm' ) ).toEqual( false );
        } );


    } );

    describe( 'checkFormatDateTime', function instantiation ()
    {




        it( 'yyyy-mm-dd h|hh:mm:ss' , function ()
        {
            expect( date.checkFormatDateTime( '2015-05-14 18:12:56' ) ).toEqual( true );
            expect( date.checkFormatDateTime( '2015-5-14 12:12:56' ) ).toEqual( false );
            expect( date.checkFormatDateTime( '2015-30-14 12:12:56' ) ).toEqual( false );
            expect( date.checkFormatDateTime( '2015-11-14 20:12:56' ) ).toEqual( true );
            expect( date.checkFormatDateTime( '2015-14-14 29:12:70' ) ).toEqual( false );
            expect( date.checkFormatDateTime( '2015-14-14 1:12:1' ) ).toEqual( false );
        } );
        it( 'yyyy-mm-dd h|hh:mm:ss am|pm' , function ()
        {
            expect( date.checkFormatDateTime( '2015-05-14 12:12:56 pm' ) ).toEqual( true );
            expect( date.checkFormatDateTime( '2015-5-14 12:12:56 pm' ) ).toEqual( false );
            expect( date.checkFormatDateTime( '2015-30-14 12:12:56 pm' ) ).toEqual( false );
            expect( date.checkFormatDateTime( '2015-11-14 12:12:56 pm' ) ).toEqual( true );
            expect( date.checkFormatDateTime( '2015-14-14 12:12:70 pm' ) ).toEqual( false );
            expect( date.checkFormatDateTime( '2015-14-14 12:12:1 pm' ) ).toEqual( false );
        } );

        it( 'yyyy/mm/dd h|hh:mm:ss' , function ()
        {
            expect( date.checkFormatDateTime( '2015/05/14 12:12:25' ) ).toEqual( true );
            expect( date.checkFormatDateTime( '2015/5/14 12:12:25' ) ).toEqual( false );
            expect( date.checkFormatDateTime( '2015/30/14 12:12:25' ) ).toEqual( false );

        } );

        it( 'yyyy/mm/dd h|hh:mm:ss am|pm' , function ()
        {
            expect( date.checkFormatDateTime( '2015/12/14 12:12:56 pm' ) ).toEqual( true );
            expect( date.checkFormatDateTime( '2015/14/14 12:12:70 pm' ) ).toEqual( false );
            expect( date.checkFormatDateTime( '2015/14/14 12:12:1 pm' ) ).toEqual( false );
            expect( date.checkFormatDateTime( '2015/12/14 12:12:70 pm' ) ).toEqual( false );
        } );
        it( 'dd-mm-yyyy h|hh:mm:ss' , function ()
        {
            expect( date.checkFormatDateTime( '13-05-2015 12:12:12' ) ).toEqual( true );
            expect( date.checkFormatDateTime( '13-05-201520:14:70' ) ).toEqual( false );
            expect( date.checkFormatDateTime( '13-05-2015 25:12:1' ) ).toEqual( false );

        } );
        it( 'dd-mm-yyyy h|hh:mm:ss am|pm' , function ()
        {
            expect( date.checkFormatDateTime( '14-05-2015 12:12:56 pm' ) ).toEqual( true );
            expect( date.checkFormatDateTime( '14-5-2015 12:12:56 pm' ) ).toEqual( false );
            expect( date.checkFormatDateTime( '14-30-2015 12:12:56 pm' ) ).toEqual( false );

        } );
        it( 'dd/mm/yyyy h|hh:mm:ss' , function ()
        {
            expect( date.checkFormatDateTime( '14/05/2015 12:12:25' ) ).toEqual( true );
            expect( date.checkFormatDateTime( '14/5/2015 12:12:25' ) ).toEqual( false );
            expect( date.checkFormatDateTime( '14/30/2015 12:12:25' ) ).toEqual( false );

        } );
        it( 'dd/mm/yyyy h|hh:mm:ss am|pm' , function ()
        {
            expect( date.checkFormatDateTime( '14/10/2015 12:12:12' ) ).toEqual( true );
            expect( date.checkFormatDateTime( '14/10/2015 20:14:70' ) ).toEqual( false );
            expect( date.checkFormatDateTime( '14/10/2015 25:12:1' ) ).toEqual( false );
        } );

    } );


}
);
