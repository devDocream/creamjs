define( [
        'util/Assert'
],
function assertSpec ( Assert )
{
    describe( 'isBlank', function instantiation ()
    {

        it( 'Only accept string', function ()
        {
            expect( Assert.isBlank() ).toEqual(  false );
            expect( Assert.isBlank( null ) ).toEqual(  false );
            expect( Assert.isBlank( 15 ) ).toEqual(  false );
            expect( Assert.isBlank( 15.8 ) ).toEqual(  false );
            expect( Assert.isBlank( [] ) ).toEqual(  false );
            expect( Assert.isBlank( [ 'hola', 'adios' ] ) ).toEqual(  false );
            expect( Assert.isBlank( false ) ).toEqual(  false );
            expect( Assert.isBlank( true ) ).toEqual(  false );
            expect( Assert.isBlank( {} ) ).toEqual(  false );

        } );

        it( 'If empty string then true', function ()
        {
            expect( Assert.isBlank( '' ) ).toEqual( true );

        } );

        it( 'If not empty string then false', function ()
        {
            expect( Assert.isBlank( 'hola' ) ).toEqual(  false );

        } );

    } );

    describe( 'notBlank', function instantiation ()
    {

        it( 'Only accept string', function ()
        {
            expect( Assert.notBlank() ).toEqual(  false );
            expect( Assert.notBlank( null ) ).toEqual(  false );
            expect( Assert.notBlank( 15 ) ).toEqual(  false );
            expect( Assert.notBlank( 15.8 ) ).toEqual(  false );
            expect( Assert.notBlank( [] ) ).toEqual(  false );
            expect( Assert.notBlank( [ 'hola', 'adios' ] ) ).toEqual(  false );
            expect( Assert.notBlank( false ) ).toEqual(  false );
            expect( Assert.notBlank( true ) ).toEqual(  false );
            expect( Assert.notBlank( {} ) ).toEqual(  false );

        } );

        it( 'If empty string then false', function ()
        {
            expect( Assert.notBlank( '' ) ).toEqual( false );

        } );

        it( 'If not empty string then true', function ()
        {
            expect( Assert.notBlank( 'hola' ) ).toEqual(  true );

        } );

    } );

    describe( 'isNull', function instantiation ()
    {
        it( 'If null then true', function ()
        {
            expect( Assert.isNull( null ) ).toEqual( true );

        } );

        it( 'Should not admit undefined', function ()
        {
            expect( Assert.isNull(  ) ).toEqual( false );

        } );


        it( 'If not null then false', function ()
        {
            expect( Assert.isNull( 'hola' ) ).toEqual(  false );

        } );

    } );

    describe( 'notNull', function instantiation ()
    {
        it( 'If null then false', function ()
        {
            expect( Assert.notNull( null ) ).toEqual( false );

        } );

        it( 'If not null or undefined then true', function ()
        {
            expect( Assert.notNull( 'hola' ) ).toEqual(  true );
            expect( Assert.notNull(  ) ).toEqual(  true );
        } );

    } );
    
    describe( 'isFalse', function instantiation ()
    {

        it( 'Only accept boolean', function ()
        {
            expect( Assert.isFalse() ).toEqual(  false );
            expect( Assert.isFalse( null ) ).toEqual(  false );
            expect( Assert.isFalse( 15 ) ).toEqual(  false );
            expect( Assert.isFalse( 15.8 ) ).toEqual(  false );
            expect( Assert.isFalse( [] ) ).toEqual(  false );
            expect( Assert.isFalse( [ 'hola', 'adios' ] ) ).toEqual(  false );
            expect( Assert.isFalse( 'hola' ) ).toEqual(  false );
            expect( Assert.isFalse( '' ) ).toEqual(  false );
            expect( Assert.isFalse( {} ) ).toEqual(  false );

        } );

        it( 'If true then false', function ()
        {
            expect( Assert.isFalse( true ) ).toEqual( false );

        } );

        it( 'If false then true', function ()
        {
            expect( Assert.isFalse( false ) ).toEqual(  true );

        } );

    } );

    describe( 'isTrue', function instantiation ()
    {

        it( 'Only accept boolean', function ()
        {
            expect( Assert.isTrue() ).toEqual(  false );
            expect( Assert.isTrue( null ) ).toEqual(  false );
            expect( Assert.isTrue( 15 ) ).toEqual(  false );
            expect( Assert.isTrue( 15.8 ) ).toEqual(  false );
            expect( Assert.isTrue( [] ) ).toEqual(  false );
            expect( Assert.isTrue( [ 'hola', 'adios' ] ) ).toEqual(  false );
            expect( Assert.isTrue( 'hola' ) ).toEqual(  false );
            expect( Assert.isTrue( '' ) ).toEqual(  false );
            expect( Assert.isTrue( {} ) ).toEqual(  false );

        } );

        it( 'If true then true', function ()
        {
            expect( Assert.isTrue( true ) ).toEqual( true );

        } );

        it( 'If false then false', function ()
        {
            expect( Assert.isTrue( false ) ).toEqual(  false );

        } );

    } );

    describe( 'isNumber', function instantiation ()
    {

        it( 'Only accept float or integer', function ()
        {
            expect( Assert.isNumber() ).toEqual(  false );
            expect( Assert.isNumber( null ) ).toEqual(  false );
            expect( Assert.isNumber( true ) ).toEqual(  false );
            expect( Assert.isNumber( false ) ).toEqual(  false );
            expect( Assert.isNumber( [] ) ).toEqual(  false );
            expect( Assert.isNumber( [ 'hola', 'adios' ] ) ).toEqual(  false );
            expect( Assert.isNumber( 'hola' ) ).toEqual(  false );
            expect( Assert.isNumber( '' ) ).toEqual(  false );
            expect( Assert.isNumber( '15.8' ) ).toEqual(  false );
            expect( Assert.isNumber( '15' ) ).toEqual(  false );
            expect( Assert.isNumber( {} ) ).toEqual(  false );

        } );

        it( 'If float then true', function ()
        {
            expect( Assert.isNumber( 15.8 ) ).toEqual( true );

        } );

        it( 'If integer then true', function ()
        {
            expect( Assert.isNumber( 15 ) ).toEqual(  true );

        } );

    } );

    describe( 'isString', function instantiation ()
    {

        it( 'Only accept string', function ()
        {
            expect( Assert.isString() ).toEqual(  false );
            expect( Assert.isString( null ) ).toEqual(  false );
            expect( Assert.isString( true ) ).toEqual(  false );
            expect( Assert.isString( false ) ).toEqual(  false );
            expect( Assert.isString( [] ) ).toEqual(  false );
            expect( Assert.isString( [ 'hola', 'adios' ] ) ).toEqual(  false );
            expect( Assert.isString( 15.8 ) ).toEqual(  false );
            expect( Assert.isString( 15 ) ).toEqual(  false );
            expect( Assert.isString( {} ) ).toEqual(  false );

        } );

        it( 'If string then true', function ()
        {
            expect( Assert.isString( 'hola' ) ).toEqual( true );
            expect( Assert.isString( '15.8' ) ).toEqual( true );
            expect( Assert.isString( 'true' ) ).toEqual( true );
            expect( Assert.isString( '' ) ).toEqual(  true );

        } );

    } );

    describe( 'isInteger', function instantiation ()
    {


        it( 'Only accept integer if strict mode', function ()
        {
            expect( Assert.isInteger() ).toEqual(  false );
            expect( Assert.isInteger( null, true ) ).toEqual(  false );
            expect( Assert.isInteger( true , true) ).toEqual(  false );
            expect( Assert.isInteger( false , true) ).toEqual(  false );
            expect( Assert.isInteger( [], true ) ).toEqual(  false );
            expect( Assert.isInteger( [ 'hola', 'adios' ], true ) ).toEqual(  false );
            expect( Assert.isInteger( 15.8, true ) ).toEqual(  false );
            expect( Assert.isInteger( '15.8', true ) ).toEqual(  false );
            expect( Assert.isInteger( '15' , true) ).toEqual(  false );
            expect( Assert.isInteger( 'hola', true ) ).toEqual(  false );
            expect( Assert.isInteger( {} , true) ).toEqual(  false );

        } );

        it( 'Only accept integer and integer string if not strict', function ()
        {

            expect( Assert.isInteger( 15.8 ) ).toEqual(  false );
            expect( Assert.isInteger( '15.8' ) ).toEqual(  false );
            expect( Assert.isInteger( 'true' ) ).toEqual(  false );
            expect( Assert.isInteger( true ) ).toEqual(  false );
            expect( Assert.isInteger( '15' ) ).toEqual(  true );
            expect( Assert.isInteger( 15 ) ).toEqual(  true );

        } );

        it( 'If integer then true', function ()
        {
            expect( Assert.isInteger( 15 ) ).toEqual( true );
            expect( Assert.isInteger( 1000 ) ).toEqual( true );

        } );
    } );

    describe( 'isDecimal', function instantiation ()
    {


        it( 'Only accept string', function ()
        {
            expect( Assert.isDecimal() ).toEqual(  false );
            expect( Assert.isDecimal( null ) ).toEqual(  false );
            expect( Assert.isDecimal( true ) ).toEqual(  false );
            expect( Assert.isDecimal( false ) ).toEqual(  false );
            expect( Assert.isDecimal( [] ) ).toEqual(  false );
            expect( Assert.isDecimal( [ 'hola', 'adios' ], true ) ).toEqual(  false );
            expect( Assert.isDecimal( 15.8 ) ).toEqual(  false );
            expect( Assert.isDecimal( 15 ) ).toEqual(  false );
            expect( Assert.isDecimal( '15.8' ) ).toEqual(  false );
            expect( Assert.isDecimal( '15' ) ).toEqual(  false );
            expect( Assert.isDecimal( 'hola' ) ).toEqual(  false );
            expect( Assert.isDecimal( {} ) ).toEqual(  false );
            expect( Assert.isDecimal( 'asss,823' ) ).toEqual(  false );
            expect( Assert.isDecimal( '85,ass' ) ).toEqual(  false );
            expect( Assert.isDecimal( 'sas,ass' ) ).toEqual(  false );

        } );

        it( 'String with "integer,integer" ' , function ()
        {
            expect( Assert.isDecimal( '134,23223' ) ).toEqual(  true );
            expect( Assert.isDecimal( '15,342' ) ).toEqual(  true );
            expect( Assert.isDecimal( '15,3' ) ).toEqual(  true );

        } );
    } );

    describe( 'isFunction', function instantiation ()
    {


        it( 'Only accept function', function ()
        {
            expect( Assert.isFunction() ).toEqual(  false );
            expect( Assert.isFunction( null ) ).toEqual(  false );
            expect( Assert.isFunction( true ) ).toEqual(  false );
            expect( Assert.isFunction( false ) ).toEqual(  false );
            expect( Assert.isFunction( [] ) ).toEqual(  false );
            expect( Assert.isFunction( [ 'hola', 'adios' ], true ) ).toEqual(  false );
            expect( Assert.isFunction( 15.8 ) ).toEqual(  false );
            expect( Assert.isFunction( 15 ) ).toEqual(  false );
            expect( Assert.isFunction( '15.8' ) ).toEqual(  false );
            expect( Assert.isFunction( '15' ) ).toEqual(  false );
            expect( Assert.isFunction( 'hola' ) ).toEqual(  false );
            expect( Assert.isFunction( {} ) ).toEqual(  false );

        } );

        it( 'If function then tru" ' , function ()
        {
            var fn = function hola(){},
                fn2 = { fn: function(){}};

            expect( Assert.isFunction( function(){} ) ).toEqual(  true );
            expect( Assert.isFunction( fn2.fn) ).toEqual(  true );
            expect( Assert.isFunction( fn ) ).toEqual(  true );

        } );
    } );

    describe( 'isFloat', function instantiation ()
    {

        it( 'Only accept float if strict mode', function ()
        {
            expect( Assert.isFloat() ).toEqual(  false );
            expect( Assert.isFloat( null, true ) ).toEqual(  false );
            expect( Assert.isFloat( true , true) ).toEqual(  false );
            expect( Assert.isFloat( false , true) ).toEqual(  false );
            expect( Assert.isFloat( [], true ) ).toEqual(  false );
            expect( Assert.isFloat( [ 'hola', 'adios' ], true ) ).toEqual(  false );
            expect( Assert.isFloat( '15.8', true ) ).toEqual(  false );
            expect( Assert.isFloat( '15' , true) ).toEqual(  false );
            expect( Assert.isFloat( 'hola', true ) ).toEqual(  false );
            expect( Assert.isFloat( {} , true) ).toEqual(  false );

        } );

        it( 'Only accept float and float string if not strict', function ()
        {

            expect( Assert.isFloat( 15.8 ) ).toEqual( true );
            expect( Assert.isFloat( '15.8' ) ).toEqual(  true );
            expect( Assert.isFloat( 'true' ) ).toEqual(  false );
            expect( Assert.isFloat( true ) ).toEqual(  false );
            expect( Assert.isFloat( '15' ) ).toEqual(  false );
            expect( Assert.isFloat( 15 ) ).toEqual(  false );

        } );

        it( 'If float then true', function ()
        {
            expect( Assert.isFloat( 15.45 ) ).toEqual( true );
            expect( Assert.isFloat( 1000.34454 ) ).toEqual( true );

        } );
    } );

    describe( 'isBoolean', function instantiation ()
    {

        it( 'Only accept boolean', function ()
        {
            expect( Assert.isBoolean() ).toEqual(  false );
            expect( Assert.isBoolean( null ) ).toEqual(  false );
            expect( Assert.isBoolean( 15 ) ).toEqual(  false );
            expect( Assert.isBoolean( 15.38 ) ).toEqual(  false );
            expect( Assert.isBoolean( [] ) ).toEqual(  false );
            expect( Assert.isBoolean( [ 'hola', 'adios' ]) ).toEqual(  false );
            expect( Assert.isBoolean( '15.8' ) ).toEqual(  false );
            expect( Assert.isBoolean( '15' ) ).toEqual(  false );
            expect( Assert.isBoolean( 'hola' ) ).toEqual(  false );
            expect( Assert.isBoolean( {} ) ).toEqual(  false );

        } );

        it( 'If boolean then true', function ()
        {

            expect( Assert.isBoolean( true ) ).toEqual( true );
            expect( Assert.isBoolean( false ) ).toEqual(  true );
            expect( Assert.isBoolean( 'true' ) ).toEqual(  false );
            expect( Assert.isBoolean( 'false' ) ).toEqual(  false );
            expect( Assert.isBoolean( 1 ) ).toEqual(  false );
            expect( Assert.isBoolean( 0 ) ).toEqual(  false );

        } );
    } );

    describe( 'isBooleanText', function instantiation ()
    {

        it( 'Only accept boolean string', function ()
        {
            expect( Assert.isBooleanText() ).toEqual(  false );
            expect( Assert.isBooleanText( null ) ).toEqual(  false );
            expect( Assert.isBooleanText( 15 ) ).toEqual(  false );
            expect( Assert.isBooleanText( 15.38 ) ).toEqual(  false );
            expect( Assert.isBooleanText( [] ) ).toEqual(  false );
            expect( Assert.isBooleanText( [ 'hola', 'adios' ]) ).toEqual(  false );
            expect( Assert.isBooleanText( '15.8' ) ).toEqual(  false );
            expect( Assert.isBooleanText( '15' ) ).toEqual(  false );
            expect( Assert.isBooleanText( 'hola' ) ).toEqual(  false );
            expect( Assert.isBooleanText( {} ) ).toEqual(  false );

        } );

        it( 'If boolean then true', function ()
        {

            expect( Assert.isBooleanText( true ) ).toEqual( false );
            expect( Assert.isBooleanText( false ) ).toEqual( false );
            expect( Assert.isBooleanText( 'true' ) ).toEqual(  true );
            expect( Assert.isBooleanText( 'false' ) ).toEqual(  true );
            expect( Assert.isBooleanText( 1 ) ).toEqual(  false );
            expect( Assert.isBooleanText( 0 ) ).toEqual(  false );

        } );
    } );

    describe( 'isArray', function instantiation ()
    {


        it( 'Only accept array', function ()
        {
            expect( Assert.isArray() ).toEqual(  false );
            expect( Assert.isArray( null ) ).toEqual(  false );
            expect( Assert.isArray( true ) ).toEqual(  false );
            expect( Assert.isArray( false ) ).toEqual(  false );
            expect( Assert.isArray( function(){} ) ).toEqual(  false );
            expect( Assert.isArray( 15.8 ) ).toEqual(  false );
            expect( Assert.isArray( 15 ) ).toEqual(  false );
            expect( Assert.isArray( '15.8' ) ).toEqual(  false );
            expect( Assert.isArray( '15' ) ).toEqual(  false );
            expect( Assert.isArray( 'hola' ) ).toEqual(  false );
            expect( Assert.isArray( {} ) ).toEqual(  false );

        } );

        it( 'If array then true" ' , function ()
        {

            expect( Assert.isArray( [] ) ).toEqual( true );
            expect( Assert.isArray( [ 'hola', 'adios' ] ) ).toEqual( true );

        } );
    } );

    describe( 'isObject', function instantiation ()
    {


        it( 'Only accept object', function ()
        {
            expect( Assert.isObject() ).toEqual(  false );
            expect( Assert.isObject( null ) ).toEqual(  false );
            expect( Assert.isObject( true ) ).toEqual(  false );
            expect( Assert.isObject( false ) ).toEqual(  false );
            expect( Assert.isObject( function(){} ) ).toEqual(  false );
            expect( Assert.isObject( 15.8 ) ).toEqual(  false );
            expect( Assert.isObject( 15 ) ).toEqual(  false );
            expect( Assert.isObject( '15.8' ) ).toEqual(  false );
            expect( Assert.isObject( '15' ) ).toEqual(  false );
            expect( Assert.isObject( 'hola' ) ).toEqual(  false );
            expect( Assert.isObject( [] ) ).toEqual( false );
            expect( Assert.isObject( [ 'hola', 'adios' ] ) ).toEqual( false );


        } );

        it( 'If object then true" ' , function ()
        {
            expect( Assert.isObject( {} ) ).toEqual(  true );
            expect( Assert.isObject( { hola: 1, adios: 2} ) ).toEqual(  true );

        } );
    } );

    describe( 'isUndefined', function instantiation ()
    {


        it( 'Only accept undefined', function ()
        {

            expect( Assert.isUndefined( null ) ).toEqual(  false );
            expect( Assert.isUndefined( true ) ).toEqual(  false );
            expect( Assert.isUndefined( false ) ).toEqual(  false );
            expect( Assert.isUndefined( function(){} ) ).toEqual(  false );
            expect( Assert.isUndefined( 15.8 ) ).toEqual(  false );
            expect( Assert.isUndefined( 15 ) ).toEqual(  false );
            expect( Assert.isUndefined( '15.8' ) ).toEqual(  false );
            expect( Assert.isUndefined( '15' ) ).toEqual(  false );
            expect( Assert.isUndefined( 'hola' ) ).toEqual(  false );
            expect( Assert.isUndefined( [] ) ).toEqual( false );
            expect( Assert.isUndefined( [ 'hola', 'adios' ] ) ).toEqual( false );
            expect( Assert.isUndefined( {} ) ).toEqual( false );
            expect( Assert.isUndefined( { hola: 1, adios: 2} ) ).toEqual(  false );


        } );

        it( 'If undefined then true" ' , function ()
        {

            var aux;
            expect( Assert.isUndefined() ).toEqual(  true );
            expect( Assert.isUndefined( aux ) ).toEqual(  true );

        } );
    } );

    describe( 'notUndefined', function instantiation ()
    {


        it( 'Only accept not undefined', function ()
        {

            expect( Assert.notUndefined( null ) ).toEqual(  true );
            expect( Assert.notUndefined( true ) ).toEqual(  true);
            expect( Assert.notUndefined( false ) ).toEqual(  true );
            expect( Assert.notUndefined( function(){} ) ).toEqual( true );
            expect( Assert.notUndefined( 15.8 ) ).toEqual( true );
            expect( Assert.notUndefined( 15 ) ).toEqual(  true );
            expect( Assert.notUndefined( '15.8' ) ).toEqual(  true );
            expect( Assert.notUndefined( '15' ) ).toEqual( true );
            expect( Assert.notUndefined( 'hola' ) ).toEqual(  true );
            expect( Assert.notUndefined( [] ) ).toEqual( true );
            expect( Assert.notUndefined( [ 'hola', 'adios' ] ) ).toEqual(true );
            expect( Assert.notUndefined( {} ) ).toEqual( true );
            expect( Assert.notUndefined( { hola: 1, adios: 2} ) ).toEqual( true  );


        } );

        it( 'If undefined then false" ' , function ()
        {

            var aux;
            expect( Assert.notUndefined() ).toEqual(  false );
            expect( Assert.notUndefined( aux ) ).toEqual(  false );

        } );
    } );

    describe( 'isLower', function instantiation ()
    {
        it( 'Only accept string', function ()
        {

            expect( Assert.isLower() ).toEqual(  false ); 
            expect( Assert.isLower( null ) ).toEqual(  false );
            expect( Assert.isLower( true ) ).toEqual(  false);
            expect( Assert.isLower( false ) ).toEqual(  false );
            expect( Assert.isLower( function(){} ) ).toEqual( false );
            expect( Assert.isLower( 15.8 ) ).toEqual( false );
            expect( Assert.isLower( 15 ) ).toEqual(  false );
            expect( Assert.isLower( [] ) ).toEqual( false );
            expect( Assert.isLower( [ 'hola', 'adios' ] ) ).toEqual(false );
            expect( Assert.isLower( {} ) ).toEqual( false );
            expect( Assert.isLower( { hola: 1, adios: 2} ) ).toEqual( false  );


        } );

        it( 'If lower case string then true" ' , function ()
        {
            expect( Assert.isLower( 'HolaAdios' ) ).toEqual( false  );
            expect( Assert.isLower( 'HOLAADIOS' ) ).toEqual( false  );
            expect( Assert.isLower( 'HOLA_ADIOS' ) ).toEqual( false  );
            expect( Assert.isLower( 'holadios' ) ).toEqual( true  );
            expect( Assert.isLower( 'hola_adios' ) ).toEqual( true  );
            expect( Assert.isLower( 'hola adios' ) ).toEqual( true  );

        } );
    } );

    describe( 'isUpper', function instantiation ()
    {
        it( 'Only accept string', function ()
        {

            expect( Assert.isUpper() ).toEqual(  false );
            expect( Assert.isUpper( null ) ).toEqual(  false );
            expect( Assert.isUpper( true ) ).toEqual(  false);
            expect( Assert.isUpper( false ) ).toEqual(  false );
            expect( Assert.isUpper( function(){} ) ).toEqual( false );
            expect( Assert.isUpper( 15.8 ) ).toEqual( false );
            expect( Assert.isUpper( 15 ) ).toEqual(  false );
            expect( Assert.isUpper( [] ) ).toEqual( false );
            expect( Assert.isUpper( [ 'hola', 'adios' ] ) ).toEqual(false );
            expect( Assert.isUpper( {} ) ).toEqual( false );
            expect( Assert.isUpper( { hola: 1, adios: 2} ) ).toEqual( false  );


        } );

        it( 'If upper case string then true" ' , function ()
        {
            expect( Assert.isUpper( 'HolaAdios' ) ).toEqual( false  );
            expect( Assert.isUpper( 'HOLAADIOS' ) ).toEqual( true  );
            expect( Assert.isUpper( 'HOLA_ADIOS' ) ).toEqual( true  );
            expect( Assert.isUpper( 'holadios' ) ).toEqual( false  );
            expect( Assert.isUpper( 'hola_adios' ) ).toEqual( false  );
            expect( Assert.isUpper( 'hola adios' ) ).toEqual( false  );

        } );
    } );

    describe( 'isEmail', function instantiation ()
    {


        it( 'Only accept string', function ()
        {

            expect( Assert.isEmail( ) ).toEqual(  false );
            expect( Assert.isEmail( null ) ).toEqual(  false );
            expect( Assert.isEmail( true ) ).toEqual(  false);
            expect( Assert.isEmail( false ) ).toEqual(  false );
            expect( Assert.isEmail( function(){} ) ).toEqual( false );
            expect( Assert.isEmail( 15.8 ) ).toEqual( false );
            expect( Assert.isEmail( 15 ) ).toEqual(  false );
            expect( Assert.isEmail( '15.8' ) ).toEqual(  false );
            expect( Assert.isEmail( '15' ) ).toEqual( false );
            expect( Assert.isEmail( 'hola' ) ).toEqual(  false );
            expect( Assert.isEmail( function(){} ) ).toEqual(  false );
            expect( Assert.isEmail( [] ) ).toEqual( false );
            expect( Assert.isEmail( [ 'hola', 'adios' ] ) ).toEqual(false );
            expect( Assert.isEmail( {} ) ).toEqual( false );
            expect( Assert.isEmail( { hola: 1, adios: 2} ) ).toEqual( false  );


        } );

        it( 'If valid email then true" ' , function ()
        {
            expect( Assert.isEmail( 'aaa@r') ).toEqual(  false );
            expect( Assert.isEmail( 'aaa@r.com' ) ).toEqual(  true );
            expect( Assert.isEmail( 'aaar.com' ) ).toEqual(  false );
            expect( Assert.isEmail( '@ar.com' ) ).toEqual(  false );

        } );
    } );

    describe( 'inLength', function instantiation ()
    {


        it( '1: string, 2: integer. Required the first two params', function ()
        {
            expect( Assert.inLength( ) ).toEqual(  false );
            expect( Assert.inLength( null ) ).toEqual(  false );
            expect( Assert.inLength( true ) ).toEqual(  false);
            expect( Assert.inLength( false ) ).toEqual(  false );
            expect( Assert.inLength( function(){} ) ).toEqual( false );
            expect( Assert.inLength( 15.8 ) ).toEqual( false );
            expect( Assert.inLength( 15 ) ).toEqual(  false );
            expect( Assert.inLength( '15.8' ) ).toEqual(  false );
            expect( Assert.inLength( '15' ) ).toEqual( false );
            expect( Assert.inLength( 'hola' ) ).toEqual(  false );
            expect( Assert.inLength( function(){} ) ).toEqual(  false );
            expect( Assert.inLength( [] ) ).toEqual( false );
            expect( Assert.inLength( [ 'hola', 'adios' ] ) ).toEqual(false );
            expect( Assert.inLength( {} ) ).toEqual( false );
            expect( Assert.inLength( { hola: 1, adios: 2} ) ).toEqual( false  );

            expect( Assert.inLength( 'hola' ) ).toEqual(  false );
            expect( Assert.inLength( 'hola', null ) ).toEqual(  false );
            expect( Assert.inLength( 'hola', true ) ).toEqual(  false);
            expect( Assert.inLength( 'hola', false ) ).toEqual(  false );
            expect( Assert.inLength( 'hola', function(){} ) ).toEqual( false );
            expect( Assert.inLength( 'hola', 15.8 ) ).toEqual( false );
            expect( Assert.inLength( 'hola', '15.8' ) ).toEqual(  false );
            expect( Assert.inLength( 'hola', '15' ) ).toEqual( false );
            expect( Assert.inLength( 'hola', 'hola' ) ).toEqual(  false );
            expect( Assert.inLength( 'hola',  function(){} ) ).toEqual(  false );
            expect( Assert.inLength( 'hola', [] ) ).toEqual( false );
            expect( Assert.inLength( 'hola', [ 'hola', 'adios' ] ) ).toEqual(false );
            expect( Assert.inLength( 'hola', {} ) ).toEqual( false );
            expect( Assert.inLength( 'hola', { hola: 1, adios: 2} ) ).toEqual( false  );
        } );

        it( 'If only min param, the string length should be equal to min param', function ()
        {
            expect( Assert.inLength( 'hola', 4 ) ).toEqual(  true );
            expect( Assert.inLength( 'hola', 3 ) ).toEqual(  false );
         } );

        it( 'If min and max param, the string length should be between min and max', function ()
        {
            expect( Assert.inLength( 'hola', 4 , 7 ) ).toEqual(  true );
            expect( Assert.inLength( 'hola', 3, 9  ) ).toEqual(  true );
            expect( Assert.inLength( 'hola', 3, 4  ) ).toEqual(  true );
            expect( Assert.inLength( 'hola', 0, 1  ) ).toEqual(  false );
            expect( Assert.inLength( 'hola', 50, 100  ) ).toEqual(  false );
         } );

        it( 'If min and max param, and msg params is true, return string message', function ()
        {
            expect( Assert.inLength( 'hola', 4, '', true) ).toEqual(  true );
            expect( Assert.inLength( 'hola', 3, '', true ) ).toEqual( 'exact' );
            expect( Assert.inLength( 'hola', 4 , 7, true ) ).toEqual(  true );
            expect( Assert.inLength( 'hola', 3, 9 , true ) ).toEqual(  true );
            expect( Assert.inLength( 'hola', 3, 4 , true ) ).toEqual(  true );
            expect( Assert.inLength( 'hola', 0, 1 , true ) ).toEqual(  'max' );
            expect( Assert.inLength( 'hola', 50, 100 , true ) ).toEqual(  'min' );
         } );

    } );

    describe( 'isURL', function instantiation ()
    {


        it( 'Only accept string', function ()
        {

            expect( Assert.isURL( ) ).toEqual(  false );
            expect( Assert.isURL( null ) ).toEqual(  false );
            expect( Assert.isURL( true ) ).toEqual(  false);
            expect( Assert.isURL( false ) ).toEqual(  false );
            expect( Assert.isURL( function(){} ) ).toEqual( false );
            expect( Assert.isURL( 15.8 ) ).toEqual( false );
            expect( Assert.isURL( 15 ) ).toEqual(  false );
            expect( Assert.isURL( '15.8' ) ).toEqual(  false );
            expect( Assert.isURL( '15' ) ).toEqual( false );
            expect( Assert.isURL( 'hola' ) ).toEqual(  false );
            expect( Assert.isURL( function(){} ) ).toEqual(  false );
            expect( Assert.isURL( [] ) ).toEqual( false );
            expect( Assert.isURL( [ 'hola', 'adios' ] ) ).toEqual(false );
            expect( Assert.isURL( {} ) ).toEqual( false );
            expect( Assert.isURL( { hola: 1, adios: 2} ) ).toEqual( false  );


        } );

        it( 'If valid URL then true" ' , function ()
        {
            expect( Assert.isURL( 'www.google.es') ).toEqual(  true);
            expect( Assert.isURL( 'google.es' ) ).toEqual(  true );
            expect( Assert.isURL( 'maps.google.es' ) ).toEqual(  true);
            expect( Assert.isURL( 'http://google.es' ) ).toEqual(  true );
            expect( Assert.isURL( 'http://maps.google.es' ) ).toEqual(  true );
            expect( Assert.isURL( 'http://www.google.es') ).toEqual(   true );
            expect( Assert.isURL( 'https://google.es' ) ).toEqual(  true);
            expect( Assert.isURL( 'https://www.google.es') ).toEqual(   true);
            expect( Assert.isURL( 'https://maps.google.es' ) ).toEqual(  true );

            expect( Assert.isURL( 'htps://maps.google.es' ) ).toEqual(  false );
            expect( Assert.isURL( 'https://maps.google' ) ).toEqual(  true );
            expect( Assert.isURL( 'https://www.maps.google' ) ).toEqual(  true );
            expect( Assert.isURL( 'http://www.maps' ) ).toEqual(  true );
            expect( Assert.isURL( 'http/www.maps.com' ) ).toEqual(  false );
        } );
    } );
    
    describe( 'isIP', function instantiation ()
    {


        it( 'Only accept string', function ()
        {

            expect( Assert.isIP( ) ).toEqual(  false );
            expect( Assert.isIP( null ) ).toEqual(  false );
            expect( Assert.isIP( true ) ).toEqual(  false);
            expect( Assert.isIP( false ) ).toEqual(  false );
            expect( Assert.isIP( function(){} ) ).toEqual( false );
            expect( Assert.isIP( 15.8 ) ).toEqual( false );
            expect( Assert.isIP( 15 ) ).toEqual(  false );
            expect( Assert.isIP( '15.8' ) ).toEqual(  false );
            expect( Assert.isIP( '15' ) ).toEqual( false );
            expect( Assert.isIP( 'hola' ) ).toEqual(  false );
            expect( Assert.isIP( function(){} ) ).toEqual(  false );
            expect( Assert.isIP( [] ) ).toEqual( false );
            expect( Assert.isIP( [ 'hola', 'adios' ] ) ).toEqual(false );
            expect( Assert.isIP( {} ) ).toEqual( false );
            expect( Assert.isIP( { hola: 1, adios: 2} ) ).toEqual( false  );


        } );

        it( 'If valid URL then true" ' , function ()
        {
            expect( Assert.isIP( '192.168.1.123') ).toEqual(  true);
            expect( Assert.isIP( '124.258.34.5' ) ).toEqual(  false );
            expect( Assert.isIP( '124.258.34.5222' ) ).toEqual(  false );
            expect( Assert.isIP( '124.254.34.5' ) ).toEqual(  true );

            expect( Assert.isIP( '192.12' ) ).toEqual(  false );
            expect( Assert.isIP( '192-34-5-67' ) ).toEqual(  false );
            expect( Assert.isIP( '192.13.21' ) ).toEqual(  false);
            expect( Assert.isIP( '123.12.34.56.67' ) ).toEqual( false);
            expect( Assert.isIP( '192.168.2.x' ) ).toEqual(  false );
        } );
    } );
    describe( 'isRange', function instantiation ()
    {
        it( 'String|Date|Number. Required the first three params, and equal type', function ()
        {
            expect( Assert.inRange( ) ).toEqual(  false );
            expect( Assert.inRange( null ) ).toEqual(  false );
            expect( Assert.inRange( true ) ).toEqual(  false);
            expect( Assert.inRange( false ) ).toEqual(  false );
            expect( Assert.inRange( function(){} ) ).toEqual( false );
            expect( Assert.inRange( 15.8 ) ).toEqual( false );
            expect( Assert.inRange( 15 ) ).toEqual(  false );
            expect( Assert.inRange( '15.8' ) ).toEqual(  false );
            expect( Assert.inRange( '15' ) ).toEqual( false );
            expect( Assert.inRange( 'hola' ) ).toEqual(  false );
            expect( Assert.inRange( function(){} ) ).toEqual(  false );
            expect( Assert.inRange( [] ) ).toEqual( false );
            expect( Assert.inRange( [ 'hola', 'adios' ] ) ).toEqual(false );
            expect( Assert.inRange( {} ) ).toEqual( false );
            expect( Assert.inRange( { hola: 1, adios: 2} ) ).toEqual( false  );

            expect( Assert.inRange( 'hola' ) ).toEqual(  false );
            expect( Assert.inRange( 'hola', null ) ).toEqual(  false );
            expect( Assert.inRange( 'hola', true ) ).toEqual(  false);
            expect( Assert.inRange( 'hola', false ) ).toEqual(  false );
            expect( Assert.inRange( 'hola', function(){} ) ).toEqual( false );
            expect( Assert.inRange( 'hola', 15.8 ) ).toEqual( false );
            expect( Assert.inRange( 'hola', '15.8' ) ).toEqual(  false );
            expect( Assert.inRange( 'hola', '15' ) ).toEqual( false );
            expect( Assert.inRange( 'hola', 'hola' ) ).toEqual(  false );
            expect( Assert.inRange( 'hola',  function(){} ) ).toEqual(  false );
            expect( Assert.inRange( 'hola', [] ) ).toEqual( false );
            expect( Assert.inRange( 'hola', [ 'hola', 'adios' ] ) ).toEqual(false );
            expect( Assert.inRange( 'hola', {} ) ).toEqual( false );
            expect( Assert.inRange( 'hola', { hola: 1, adios: 2} ) ).toEqual( false  );

            expect( Assert.inRange( 'hola','hola' ) ).toEqual(  false );
            expect( Assert.inRange( 'hola','hola', null ) ).toEqual(  false );
            expect( Assert.inRange( 'hola','hola', true ) ).toEqual(  false);
            expect( Assert.inRange( 'hola','hola', false ) ).toEqual(  false );
            expect( Assert.inRange( 'hola','hola', function(){} ) ).toEqual( false );
            expect( Assert.inRange( 'hola','hola', 15.8 ) ).toEqual( false );
            expect( Assert.inRange( 'hola','hola',  function(){} ) ).toEqual(  false );
            expect( Assert.inRange( 'hola','hola', [] ) ).toEqual( false );
            expect( Assert.inRange( 'hola','hola', [ 'hola', 'adios' ] ) ).toEqual(false );
            expect( Assert.inRange( 'hola','hola', {} ) ).toEqual( false );
            expect( Assert.inRange( 'hola','hola', { hola: 1, adios: 2} ) ).toEqual( false  );

            expect( Assert.inRange( new Date('2014-11-11'),'hola', 'hola' ) ).toEqual( false  );

            expect( Assert.inRange( 15,'hola', 18 ) ).toEqual( false  );

        } );

        it( 'In range dates', function ()
        {
            expect( Assert.inRange( new Date('2014-11-11'), new Date('2014-12-11'), new Date('2014-10-11') ) ).toEqual(  false );
            expect( Assert.inRange( new Date('2014-11-11'), new Date('2014-12-11'), new Date('2014-12-15') ) ).toEqual( false );
            expect( Assert.inRange( new Date('2014-11-11'), new Date('2014-11-11'), new Date('2014-11-11') ) ).toEqual( true );
            expect( Assert.inRange( new Date('2014-11-11'), new Date('2014-10-11'), new Date('2014-12-11') ) ).toEqual(  true );
        } );

        it( 'In range string', function ()
        {
            expect( Assert.inRange( 'bb', 'aa' , 'cc' ) ).toEqual(  true );
            expect( Assert.inRange( 'bb', 'cc' , 'dd' ) ).toEqual( false );
            expect( Assert.inRange( 'bb', 'aa', 'bb'  ) ).toEqual(  true );
            expect( Assert.inRange( 'bb', 'bb', 'bb'  ) ).toEqual(  true );
            expect( Assert.inRange( 'bb', 'bb', 'cc'  ) ).toEqual(  true );
            expect( Assert.inRange( 'bb', 'zz', 'aa'  ) ).toEqual(  false );
        } );

        it( 'In range number', function ()
        {
            expect( Assert.inRange( 5, 5 , 5 ) ).toEqual(  true );
            expect( Assert.inRange( 5, 3 , 5 ) ).toEqual(  true );
            expect( Assert.inRange( 5, 5, 7  ) ).toEqual(  true );
            expect( Assert.inRange( 5, 7, 3  ) ).toEqual(  false );
            expect( Assert.inRange( 5, 0, 2  ) ).toEqual(  false );
            expect( Assert.inRange( 5, 'zz', 'aa'  ) ).toEqual(  false );

            expect( Assert.inRange( 5.8, 0.8, 18.9  ) ).toEqual(  true );
            expect( Assert.inRange( 5, 5.8, 8.9  ) ).toEqual(  false );
            expect( Assert.inRange( 5.9, 4, 20.4  ) ).toEqual(  true);

        } );

        it( 'Return message if msg param is passed and is not a valid range', function ()
        {
            expect( Assert.inRange( 'bb', 'aa' , 'cc', true ) ).toEqual(  true );
            expect( Assert.inRange( 'bb', 'cc' , 'dd' , true) ).toEqual( 'min' );
            expect( Assert.inRange( 'bb', 'aa', 'bb' , true ) ).toEqual(  true );
            expect( Assert.inRange( 'bb', 'bb', 'bb' , true ) ).toEqual(  true );
            expect( Assert.inRange( 'bb', 'bb', 15 , true ) ).toEqual(  'invalid' );
            expect( Assert.inRange( 'bb', 'zz', 'aa' , true ) ).toEqual(  'max' );

        } );

    } );

    describe( 'equalTo', function instantiation ()
    {


        it( 'Compare Dates, Arrays, Numbers <=> Strings, booleans, functions, . Not Objects', function ()
        {

            expect( Assert.equalTo( ) ).toEqual(  true );
            expect( Assert.equalTo( null , null ) ).toEqual(  true );
            expect( Assert.equalTo( true, true ) ).toEqual(  true  );
            expect( Assert.equalTo( false, false ) ).toEqual(  true  );
            expect( Assert.equalTo( function(){},function(){ console.log('hola')}  ) ).toEqual( false  );
            expect( Assert.equalTo( function(){},function(){}  ) ).toEqual( true  );
            expect( Assert.equalTo( 15.8 ,15.8) ).toEqual( true  );
            expect( Assert.equalTo( 15, '15') ).toEqual(  true  );
            expect( Assert.equalTo( 15.8, '15.8' ) ).toEqual(  true  );
            expect( Assert.equalTo( '15', '15' ) ).toEqual( true  );
            expect( Assert.equalTo( 'hola', 'adiuos') ).toEqual(  false  );
            expect( Assert.equalTo( [], []) ).toEqual( true );
            expect( Assert.equalTo( [], [ 'adios' ]) ).toEqual( false );
            expect( Assert.equalTo( [ 'hola', 'adios' ] , [ 'hola', 'adios' ] ) ).toEqual( true );
            expect( Assert.equalTo( {} ,{}  ) ).toEqual( true );
            expect( Assert.equalTo( {} ,{ hola: 1 }  ) ).toEqual( true );
            expect( Assert.equalTo( { hola: 1, adios: 2},{ hola: 1, adios: 2} ) ).toEqual( true  );
            var d = new Date();
            expect( Assert.equalTo(d ,d) ).toEqual( true  );
            expect( Assert.equalTo(d ,new Date( '2015-12-12' ) ) ).toEqual( false  );
            expect( Assert.equalTo( '2015-12-12' ,new Date( '2015-12-12' ) ) ).toEqual( false  );


        } );

    } );

    describe( 'identicalTo', function instantiation ()
    {


        it( 'Compare Dates, Numbers <=> Strings, booleans . Not Objects, arrays, functions', function ()
        {

            var fn = function(){},
                fn2 = function(){ console.log('hola')},
                arr = [],
                arr1 = [],
                arr2= [ 'hola',  'adios'];
            expect( Assert.identicalTo( ) ).toEqual(  true );
            expect( Assert.identicalTo( null , null ) ).toEqual(  true );
            expect( Assert.identicalTo( true, true ) ).toEqual(  true  );
            expect( Assert.identicalTo( false, false ) ).toEqual(  true  );
            expect( Assert.identicalTo( fn, fn2  ) ).toEqual( false  );
            expect( Assert.identicalTo( fn,  fn  ) ).toEqual( true  );
            expect( Assert.identicalTo( 15.8 ,15.8) ).toEqual( true  );
            expect( Assert.identicalTo( 15, '15') ).toEqual(  false );
            expect( Assert.identicalTo( 15.8, '15.8' ) ).toEqual(  false  );
            expect( Assert.identicalTo( '15', '15' ) ).toEqual( true  );
            expect( Assert.identicalTo( 'hola', 'adiuos') ).toEqual(  false  );
            expect( Assert.identicalTo( arr, arr) ).toEqual( true );
            expect( Assert.identicalTo( arr, arr1) ).toEqual( false );
            expect( Assert.identicalTo( arr, arr2) ).toEqual( false );
            expect( Assert.identicalTo( [ 'hola', 'adios' ] , [ 'hola', 'adios' ] ) ).toEqual( false );
            expect( Assert.identicalTo( {} ,{}  ) ).toEqual( false );
            expect( Assert.identicalTo( {} , { hola: 1 }  ) ).toEqual( false );
            expect( Assert.identicalTo( { hola: 1, adios: 2},{ hola: 1, adios: 3} ) ).toEqual( false  );
            var d = new Date();
            expect( Assert.identicalTo(d ,d) ).toEqual( true  );
            expect( Assert.identicalTo(d ,new Date( '2015-12-12' ) ) ).toEqual( false  );
            expect( Assert.identicalTo('2015-12-12' ,new Date( '2015-12-12' ) ) ).toEqual( false  );


        } );

    } );

    describe( 'isDate', function instantiation ()
    {


        it( 'Is date object then true', function ()
        {

            expect( Assert.isDate( ) ).toEqual(  false );
            expect( Assert.isDate( null ) ).toEqual(  false );
            expect( Assert.isDate( true ) ).toEqual(  false);
            expect( Assert.isDate( false ) ).toEqual(  false );
            expect( Assert.isDate( function(){} ) ).toEqual( false );
            expect( Assert.isDate( 15.8 ) ).toEqual( false );
            expect( Assert.isDate( 15 ) ).toEqual(  false );
            expect( Assert.isDate( '15.8' ) ).toEqual(  false );
            expect( Assert.isDate( '15' ) ).toEqual( false );
            expect( Assert.isDate( 'hola' ) ).toEqual(  false );
            expect( Assert.isDate( function(){} ) ).toEqual(  false );
            expect( Assert.isDate( [] ) ).toEqual( false );
            expect( Assert.isDate( [ 'hola', 'adios' ] ) ).toEqual(false );
            expect( Assert.isDate( {} ) ).toEqual( false );
            expect( Assert.isDate( { hola: 1, adios: 2} ) ).toEqual( false  );
            expect( Assert.isDate( '2015-12-12' ) ).toEqual( false  );

            expect( Assert.isDate( new Date() ) ).toEqual( true  );
            expect( Assert.isDate( new Date( '2015-12-12' ) ) ).toEqual( true );
            expect( Assert.isDate( new Date( '2015-30-30' ) ) ).toEqual( true  );
        } );

      
    } );

    describe( 'isValidDate', function instantiation ()
    {


        it( 'If date object is valid then true', function ()
        {

            expect( Assert.isValidDate( ) ).toEqual(  false );
            expect( Assert.isValidDate( null ) ).toEqual(  false );
            expect( Assert.isValidDate( true ) ).toEqual(  false);
            expect( Assert.isValidDate( false ) ).toEqual(  false );
            expect( Assert.isValidDate( function(){} ) ).toEqual( false );
            expect( Assert.isValidDate( 15.8 ) ).toEqual( false );
            expect( Assert.isValidDate( 15 ) ).toEqual(  false );
            expect( Assert.isValidDate( '15.8' ) ).toEqual(  false );
            expect( Assert.isValidDate( '15' ) ).toEqual( false );
            expect( Assert.isValidDate( 'hola' ) ).toEqual(  false );
            expect( Assert.isValidDate( function(){} ) ).toEqual(  false );
            expect( Assert.isValidDate( [] ) ).toEqual( false );
            expect( Assert.isValidDate( [ 'hola', 'adios' ] ) ).toEqual(false );
            expect( Assert.isValidDate( {} ) ).toEqual( false );
            expect( Assert.isValidDate( { hola: 1, adios: 2} ) ).toEqual( false  );

            expect( Assert.isValidDate( new Date() ) ).toEqual( true  );
            expect( Assert.isValidDate( new Date( '2015-12-12' ) ) ).toEqual( true );
            expect( Assert.isValidDate( new Date( '2015-30-30' ) ) ).toEqual( false  );
            expect( Assert.isValidDate( new Date( '12-12-2015' ) ) ).toEqual( false  );
            expect( Assert.isValidDate( new Date( '12/12/2015' ) ) ).toEqual( true  );
            expect( Assert.isValidDate( new Date( '12/30/2015' ) ) ).toEqual( true  );
            expect( Assert.isValidDate( new Date( '30/12/2015' ) ) ).toEqual( true );
            expect( Assert.isValidDate( new Date( '2015/12/12' ) ) ).toEqual( true );

        } );


    } );

    describe( 'is', function instantiation ()
    {


        it( 'Check Boolean, Number, String, Function, Object, Date', function ()
        {

            expect( Assert.is( ) ).toEqual(  false );
            expect( Assert.is( null, null ) ).toEqual(  false );
            expect( Assert.is( true, 'Boolean' ) ).toEqual(  true );
            expect( Assert.is( 15.8, 'Float' ) ).toEqual( false );
            expect( Assert.is( 15.8, 'Number' ) ).toEqual( true );
            expect( Assert.is( 15, 'Number' ) ).toEqual(  true );
            expect( Assert.is( '15.8', 'Number' ) ).toEqual(  false );
            expect( Assert.is( 'hola', 'String' ) ).toEqual(  true );
            expect( Assert.is( function(){}, 'Function') ).toEqual( true );
            expect( Assert.is( [ 'hola', 'adios' ], 'Array' ) ).toEqual( true );
            expect( Assert.is( [ 'hola', 'adios' ], 'Object' ) ).toEqual( false );
            expect( Assert.is( {}, 'Object' ) ).toEqual( true );
            expect( Assert.is( { hola: 1, adios: 2}, 'Array') ).toEqual( false  );
            expect( Assert.is( new Date(), 'Date' ) ).toEqual( true  );
            expect( Assert.is( new Date(), 'Object' ) ).toEqual( false  );

        } );


    } );

    describe( 'isChoices', function instantiation ()
    {


        it( 'First argument not undefined and second an array', function ()
        {

            expect( Assert.inChoices( ) ).toEqual(  false );
            expect( Assert.inChoices( null ) ).toEqual(  false );
            expect( Assert.inChoices( true ) ).toEqual(  false);
            expect( Assert.inChoices( false ) ).toEqual(  false );
            expect( Assert.inChoices( function(){} ) ).toEqual( false );
            expect( Assert.inChoices( 15.8 ) ).toEqual( false );
            expect( Assert.inChoices( 15 ) ).toEqual(  false );
            expect( Assert.inChoices( '15.8' ) ).toEqual(  false );
            expect( Assert.inChoices( '15' ) ).toEqual( false );
            expect( Assert.inChoices( 'hola' ) ).toEqual(  false );
            expect( Assert.inChoices( function(){} ) ).toEqual(  false );
            expect( Assert.inChoices( [] ) ).toEqual( false );
            expect( Assert.inChoices( [ 'hola', 'adios' ] ) ).toEqual(false );
            expect( Assert.inChoices( {} ) ).toEqual( false );
            expect( Assert.inChoices( { hola: 1, adios: 2} ) ).toEqual( false  );

            expect( Assert.inChoices( ) ).toEqual(  false );
            expect( Assert.inChoices( 'hola', null) ).toEqual(  false );
            expect( Assert.inChoices(  'hola', true ) ).toEqual(  false);
            expect( Assert.inChoices(  'hola', false ) ).toEqual(  false );
            expect( Assert.inChoices(  'hola', function(){} ) ).toEqual( false );
            expect( Assert.inChoices(  'hola', 15.8 ) ).toEqual( false );
            expect( Assert.inChoices(  'hola', 15 ) ).toEqual(  false );
            expect( Assert.inChoices(  'hola', '15.8' ) ).toEqual(  false );
            expect( Assert.inChoices( 'hola',  '15' ) ).toEqual( false );
            expect( Assert.inChoices(  'hola', 'hola' ) ).toEqual(  false );
            expect( Assert.inChoices(  'hola', function(){} ) ).toEqual(  false );
            expect( Assert.inChoices(  'hola', {} ) ).toEqual( false );
            expect( Assert.inChoices(  'hola', { hola: 1, adios: 2} ) ).toEqual( false  );


        } );
        
        it( 'If value is in array then true [String, Number, Boolean, null]" ' , function ()
        {
            expect( Assert.inChoices( '192.168.1.123', []) ).toEqual( false );
            expect( Assert.inChoices( '124.258.34.5', ['124.258.34.5', '124.258']) ).toEqual(  true );
            expect( Assert.inChoices( 15, [22,  '15', 21 ]) ).toEqual(  false );
            expect( Assert.inChoices( 15.8, [16,17,18] ) ).toEqual(  false );
            expect( Assert.inChoices( 15, [22,  15, 21 ]) ).toEqual(  true );
            expect( Assert.inChoices( true, [false, false ]) ).toEqual(  false);
            expect( Assert.inChoices( true, [false, false, true ]) ).toEqual( true);
            expect( Assert.inChoices( null, [false, null, true ]) ).toEqual( true );
            expect( Assert.inChoices( { hola:1 }, [ {adios: 1}, { hola:1} ]) ).toEqual(  false );
            expect( Assert.inChoices( new Date('2015-12-12'), [ new Date('2015-12-10'), new Date('2015-12-12') ]) ).toEqual(  false );
        } );

    } );

    describe( 'isCount', function instantiation ()
    {
        it( '1: array, 2: integer. Required the first two params', function ()
        {
            expect( Assert.inCount( ) ).toEqual(  false );
            expect( Assert.inCount( null ) ).toEqual(  false );
            expect( Assert.inCount( true ) ).toEqual(  false);
            expect( Assert.inCount( false ) ).toEqual(  false );
            expect( Assert.inCount( function(){} ) ).toEqual( false );
            expect( Assert.inCount( 15.8 ) ).toEqual( false );
            expect( Assert.inCount( 15 ) ).toEqual(  false );
            expect( Assert.inCount( '15.8' ) ).toEqual(  false );
            expect( Assert.inCount( '15' ) ).toEqual( false );
            expect( Assert.inCount( 'hola' ) ).toEqual(  false );
            expect( Assert.inCount( function(){} ) ).toEqual(  false );
            expect( Assert.inCount( [] ) ).toEqual( false );
            expect( Assert.inCount( [ 'hola', 'adios' ] ) ).toEqual(false );
            expect( Assert.inCount( {} ) ).toEqual( false );
            expect( Assert.inCount( { hola: 1, adios: 2} ) ).toEqual( false  );

            expect( Assert.inCount( [] ) ).toEqual(  false );
            expect( Assert.inCount( [], null ) ).toEqual(  false );
            expect( Assert.inCount( [], true ) ).toEqual(  false);
            expect( Assert.inCount( [], false ) ).toEqual(  false );
            expect( Assert.inCount( [], function(){} ) ).toEqual( false );
            expect( Assert.inCount( [], 15.8 ) ).toEqual( false );
            expect( Assert.inCount( [], '15.8' ) ).toEqual(  false );
            expect( Assert.inCount( [], '15' ) ).toEqual( false );
            expect( Assert.inCount( [], [] ) ).toEqual(  false );
            expect( Assert.inCount( [],  function(){} ) ).toEqual(  false );
            expect( Assert.inCount( [], [] ) ).toEqual( false );
            expect( Assert.inCount( [], [ 'hola', 'adios' ] ) ).toEqual(false );
            expect( Assert.inCount( [], {} ) ).toEqual( false );
            expect( Assert.inCount( [], { hola: 1, adios: 2} ) ).toEqual( false  );

        } );

        it( 'Array length = min param', function ()
        {
            expect( Assert.inCount( [ '1' , '2'], 0 ) ).toEqual(  false );
            expect( Assert.inCount( [ '1' , '2'], 1 ) ).toEqual(  false );
            expect( Assert.inCount( [ '1' , '2'], 2 ) ).toEqual(  true );
        } );

        it( 'Array length between min and max', function ()
        {
            expect( Assert.inCount( [ '1' , '2'], 0, 3 ) ).toEqual(  true );
            expect( Assert.inCount( [ '1' , '2'], 2, 5 ) ).toEqual(  true );
            expect( Assert.inCount( [ '1' , '2'], 0, 2 ) ).toEqual(  true );
            expect( Assert.inCount( [ '1' , '2'], 3, 7 ) ).toEqual(  false );
            expect( Assert.inCount( [ '1' , '2'], 0, 1 ) ).toEqual(  false );
        } );

        it( 'Return message if msg param is passed and is array length is not bteween min and max', function ()
        {
            expect( Assert.inCount( [ '1' , '2'], 0, 3, true ) ).toEqual(  true );
            expect( Assert.inCount( [ '1' , '2'], 2, 5, true ) ).toEqual(  true );
            expect( Assert.inCount( [ '1' , '2'], 0, 2, true ) ).toEqual(  true );
            expect( Assert.inCount( [ '1' , '2'], 3, 7, true ) ).toEqual(  'min' );
            expect( Assert.inCount( [ '1' , '2'], 0, 1 , true) ).toEqual(  'max' );
            expect( Assert.inCount( [ '1' , '2'], 0, '' , true) ).toEqual(  'exact' );
            expect( Assert.inCount( [ '1' , '2'], 2, '' , true) ).toEqual(  true );

        } );

    } );

}
);
