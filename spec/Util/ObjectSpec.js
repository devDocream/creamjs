define( [
        'util/Object'
    ],
    function stringSpec ( Obj )
    {
        describe( 'Keys suite', function instantiation ()
        {

            it( 'Pass undefined = empty array', function ()
            {
                expect( Obj.keys() ).toEqual( [] );
            } )

            it( 'Pass null = empty array', function ()
            {
                expect( Obj.keys( null ) ).toEqual( [] );
            } )

            it( 'Pass number = empty array', function ()
            {
                expect( Obj.keys( 15 ) ).toEqual( [] );
                expect( Obj.keys( 15.8 ) ).toEqual( [] );
            } )

            it( 'Pass empty array = empty array', function ()
            {
                expect( Obj.keys( [] ) ).toEqual( [] );
            } );

            it( 'Pass array = empty array', function ()
            {
                expect( Obj.keys( [ 'hola', 'adios' ] ) ).toEqual( [] );
            } );

            it( 'Pass boolean = empty array', function ()
            {
                expect( Obj.keys( false ) ).toEqual( [] );
                expect( Obj.keys( true ) ).toEqual( [] );
            } );

            it( 'Pass string = empty array', function ()
            {
                expect( Obj.keys( 'hola' ) ).toEqual( [] );

            } );

            it( 'Pass empty object  = empty array', function ()
            {
                expect( Obj.keys( {} ) ).toEqual( [] );

            } );

            var test = {
                        test1 : 'hola',
                        test2 : 'adios'
            }
            it( 'Pass  object ', function ()
            {
                expect( Obj.keys( test ) ).toEqual( [ 'test1', 'test2' ] );

            } );

        } );

        describe( 'Keyslength suite', function instantiation ()
        {

            it( 'Pass undefined = zero', function ()
            {
                expect( Obj.keysLength() ).toEqual( 0 );
            } )

            it( 'Pass null = zero', function ()
            {
                expect( Obj.keysLength( null ) ).toEqual( 0 );
            } )

            it( 'Pass number = zero', function ()
            {
                expect( Obj.keysLength( 15 ) ).toEqual( 0 );
                expect( Obj.keysLength( 15.8 ) ).toEqual( 0 );
            } )

            it( 'Pass zero = zero', function ()
            {
                expect( Obj.keysLength( [] ) ).toEqual( 0 );
            } );

            it( 'Pass array = zero', function ()
            {
                expect( Obj.keysLength( [ 'hola', 'adios' ] ) ).toEqual( 0 );
            } );

            it( 'Pass boolean = zero', function ()
            {
                expect( Obj.keysLength( false ) ).toEqual( 0 );
                expect( Obj.keysLength( true ) ).toEqual( 0 );
            } );

            it( 'Pass string = zero', function ()
            {
                expect( Obj.keysLength( 'hola' ) ).toEqual( 0 );

            } );

            it( 'Pass empty object  = zero', function ()
            {
                expect( Obj.keysLength( {} ) ).toEqual( 0 );

            } );

            var test = {
                test1 : 'hola',
                test2 : 'adios'
            };

            it( 'Pass  object ', function ()
            {
                expect( Obj.keysLength( test ) ).toEqual( 2 );

            } );

        } );


        describe( 'GetGlobal suite', function instantiation ()
        {

            it( 'Pass undefined = empty object', function ()
            {
                expect( Obj.getGlobal() ).toEqual( {} );
            } )

            it( 'Pass null = empty object', function ()
            {
                expect( Obj.getGlobal( null ) ).toEqual( {} );
            } )

            it( 'Pass number = empty object', function ()
            {
                expect( Obj.getGlobal( 15 ) ).toEqual( {} );
                expect( Obj.getGlobal( 15.8 ) ).toEqual( {} );
            } )

            it( 'Pass empty array = empty object', function ()
            {
                expect( Obj.getGlobal( [] ) ).toEqual( {} );
            } );

            it( 'Pass array = empty object', function ()
            {
                expect( Obj.getGlobal( [ 'hola', 'adios' ] ) ).toEqual( {} );
            } );

            it( 'Pass boolean = empty object', function ()
            {
                expect( Obj.getGlobal( false ) ).toEqual( {} );
                expect( Obj.getGlobal( true ) ).toEqual( {} );
            } );



            it( 'Pass empty string = empty object', function ()
            {
                expect( Obj.getGlobal( '' ) ).toEqual(  {} );

            } );

            it( 'Pass empty object  = empty object', function ()
            {
                expect( Obj.getGlobal( {} ) ).toEqual( {} );

            } );

            var test = {
                test1 : 'hola',
                test2 : 'adios'
            };

            it( 'Pass string and value  = window object', function ()
            {
                expect( Obj.getGlobal( 'hola', test ) ).toEqual( test );
                expect( Obj.getGlobal( 'hola', test ) ).toEqual( window.hola );

            } );

            it( 'Pass two levels string  = window object', function ()
            {
                expect( Obj.getGlobal( 'adios.hola', test ) ).toEqual( test );
                expect( Obj.getGlobal( 'adios.hola', test ) ).toEqual( window.adios.hola );

            } );

            it( 'Pass three levels string  = window object', function ()
            {
                expect( Obj.getGlobal( 'buenas.que.tal', test ) ).toEqual( test );
                expect( Obj.getGlobal( 'buenas.que.tal', test ) ).toEqual( window.buenas.que.tal );

            } );

        } );

        describe( 'GetClassName suite', function instantiation ()
        {

            it( 'Pass undefined = empty string', function ()
            {
                expect( Obj.getClassName() ).toEqual( '' );
            } )

            it( 'Pass null = empty string', function ()
            {
                expect( Obj.getClassName( null ) ).toEqual( '' );
            } )

            it( 'Pass number = empty string', function ()
            {
                expect( Obj.getClassName( 15 ) ).toEqual( '' );
                expect( Obj.getClassName( 15.8 ) ).toEqual( '' );
            } )

            it( 'Pass empty array = empty string', function ()
            {
                expect( Obj.getClassName( [] ) ).toEqual( '' );
            } );

            it( 'Pass array = empty string', function ()
            {
                expect( Obj.getClassName( [ 'hola', 'adios' ] ) ).toEqual( '' );
            } );

            it( 'Pass boolean = empty string', function ()
            {
                expect( Obj.getClassName( false ) ).toEqual( '' );
                expect( Obj.getClassName( true ) ).toEqual( '' );
            } );

            it( 'Pass empty string = empty string', function ()
            {
                expect( Obj.getClassName( '' ) ).toEqual(  '' );

            } );


            it( 'Pass string ', function ()
            {
                expect( Obj.getClassName( 'hola' ) ).toEqual( 'hola' );

            } );

            it( 'Pass two levels string', function ()
            {
                expect( Obj.getClassName( 'adios.hola' ) ).toEqual( 'adios-hola' );

            } );

            it( 'Pass three levels string', function ()
            {
                expect( Obj.getClassName( 'Buenas.que.Tal' ) ).toEqual( 'buenas-que-tal' );

            } );

        } );
        
    }
);
