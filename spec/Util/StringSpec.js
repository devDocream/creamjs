define( [
    'util/String'
],
function stringSpec ( String )
{
    describe( 'Capitalize suite', function instantiation ()
    {

        it( 'Pass undefined = empty string', function ()
        {
            expect( String.capitalize() ).toEqual( '' );
        } )

        it( 'Pass null = empty string', function ()
        {
            expect( String.capitalize( null ) ).toEqual( '' );
        } )

        it( 'Pass number = empty string', function ()
        {
            expect( String.capitalize( 15 ) ).toEqual( '' );
            expect( String.capitalize( 15.8 ) ).toEqual( '' );
        } )

        it( 'Pass array = empty string', function ()
        {
            expect( String.capitalize( [] ) ).toEqual( '' );
        } );

        it( 'Pass boolean = empty string', function ()
        {
            expect( String.capitalize( false ) ).toEqual( '' );
            expect( String.capitalize( true ) ).toEqual( '' );
        } );

        it( 'Pass Object = empty string', function ()
        {
            expect( String.capitalize( {} ) ).toEqual( '' );

        } );

        it( 'Pass spaced string', function ()
        {
            expect( String.capitalize( 'hola que tal' ) ).toEqual( 'Hola que tal' );
        } );

        it( 'Pass camelized string', function ()
        {
            expect( String.capitalize( 'HolaPepito' ) ).toEqual( 'HolaPepito' );
        } );

        it( 'Pass capitalized string', function ()
        {
            expect( String.capitalize( 'Holapepito' ) ).toEqual( 'Holapepito' );
        } );

        it( 'Pass uncapitalized string', function ()
        {
            expect( String.capitalize( 'holapepito' ) ).toEqual( 'Holapepito' );
        } );
    } );

    describe( 'Replace suite', function instantiation ()
    {

        it( 'Pass undefined = empty string', function ()
        {
            expect( String.replace() ).toEqual( '' );
        } )

        it( 'Pass null = empty string', function ()
        {
            expect( String.replace( null ) ).toEqual( '' );
        } )

        it( 'Pass number = empty string', function ()
        {
            expect( String.replace( 15 ) ).toEqual( '' );
            expect( String.replace( 15.8 ) ).toEqual( '' );
        } )

        it( 'Pass array = empty string', function ()
        {
            expect( String.replace( [] ) ).toEqual( '' );
        } );

        it( 'Pass boolean = empty string', function ()
        {
            expect( String.replace( false ) ).toEqual( '' );
            expect( String.replace( true ) ).toEqual( '' );
        } );

        it( 'Pass Object = empty string', function ()
        {
            expect( String.replace( {} ) ).toEqual( '' );

        } );



        it( 'Pass only string', function ()
        {
            expect( String.replace( 'hola que tal' ) ).toEqual( '' );
        } );

        it( 'Pass string and search string', function ()
        {
            expect( String.replace( 'HolaPepito', 'Pe' ) ).toEqual( '' );
        } );



        it( 'search parameter. Pass undefined = empty string', function ()
        {
            expect( String.replace('hola') ).toEqual( '' );
        } )

        it( 'search parameter. Pass null = empty string', function ()
        {
            expect( String.replace( 'hola', null ) ).toEqual( '' );
        } )

        it( 'search parameter. Pass number = empty string', function ()
        {
            expect( String.replace( 'hola', 15 ) ).toEqual( '' );
            expect( String.replace( 'hola', 15.8 ) ).toEqual( '' );
        } )

        it( 'search parameter. Pass array = empty string', function ()
        {
            expect( String.replace( 'hola', [] ) ).toEqual( '' );
        } );

        it( 'search parameter. Pass boolean = empty string', function ()
        {
            expect( String.replace( 'hola', false ) ).toEqual( '' );
            expect( String.replace( 'hola', true ) ).toEqual( '' );
        } );

        it( 'search parameter. Pass Object = empty string', function ()
        {
            expect( String.replace('hola', {} ) ).toEqual( '' );

        } );



        it( 'replacement parameter. Pass undefined = empty string', function ()
        {
            expect( String.replace( 'hola', 'adios' ) ).toEqual( '' );
        } )

        it( 'replacement parameter. Pass null = empty string', function ()
        {
            expect( String.replace( 'hola', 'adios', null ) ).toEqual( '' );
        } )

        it( 'replacement parameter. Pass number = empty string', function ()
        {
            expect( String.replace( 'hola', 'adios', 15 ) ).toEqual( '' );
            expect( String.replace( 'hola', 'adios', 15.8 ) ).toEqual( '' );
        } )

        it( 'replacement parameter. Pass array = empty string', function ()
        {
            expect( String.replace( 'hola', 'adios', [] ) ).toEqual( '' );
        } );

        it( 'replacement parameter. Pass boolean = empty string', function ()
        {
            expect( String.replace( 'hola', 'adios', false ) ).toEqual( '' );
            expect( String.replace( 'hola', 'adios', true ) ).toEqual( '' );
        } );

        it( 'replacement parameter. Pass Object = empty string', function ()
        {
            expect( String.replace('hola', 'adios', {} ) ).toEqual( '' );

        } );



        it( 'Single replacement', function ()
        {
            expect( String.replace('hola', 'ol', 'll' ) ).toEqual( 'hlla' );

        } );

        it( 'Multiple replacement', function ()
        {
            expect( String.replace('holahola', 'ol', 'll' ) ).toEqual( 'hllahlla' );

        } );
    } );

    describe( 'Underscorize suite', function instantiation ()
    {

        it( 'Pass undefined = empty string', function ()
        {
            expect( String.underscorize() ).toEqual( '' );
        } )

        it( 'Pass null = empty string', function ()
        {
            expect( String.underscorize( null ) ).toEqual( '' );
        } )

        it( 'Pass number = empty string', function ()
        {
            expect( String.underscorize( 15 ) ).toEqual( '' );
            expect( String.underscorize( 15.8 ) ).toEqual( '' );
        } )

        it( 'Pass array = empty string', function ()
        {
            expect( String.underscorize( [] ) ).toEqual( '' );
        } );

        it( 'Pass boolean = empty string', function ()
        {
            expect( String.underscorize( false ) ).toEqual( '' );
            expect( String.underscorize( true ) ).toEqual( '' );
        } );

        it( 'Pass Object = empty string', function ()
        {
            expect( String.underscorize( {} ) ).toEqual( '' );

        } );

        it( 'Pass spaced string', function ()
        {
            expect( String.underscorize( 'hola que tal' ) ).toEqual( 'hola que tal' );
        } );

        it( 'Pass camelized string', function ()
        {
            expect( String.underscorize( 'HolaPepito' ) ).toEqual( 'holapepito' );
        } );

        it( 'Pass capitalized string', function ()
        {
            expect( String.underscorize( 'Holapepito' ) ).toEqual( 'holapepito' );
        } );

        it( 'Pass uncapitalized string', function ()
        {
            expect( String.underscorize( 'holapepito' ) ).toEqual( 'holapepito' );
        } );

        it( 'With separator', function ()
        {
            expect( String.underscorize( 'Hola Pepito', ' ' ) ).toEqual( 'hola_pepito' );
        } );

        it( 'Default separator', function ()
        {
            expect( String.underscorize( 'Hola.Pepito' ) ).toEqual( 'hola_pepito' );
        } );

    } );

    describe( 'dasherize suite', function instantiation ()
    {

        it( 'Pass undefined = empty string', function ()
        {
            expect( String.dasherize() ).toEqual( '' );
        } )

        it( 'Pass null = empty string', function ()
        {
            expect( String.dasherize( null ) ).toEqual( '' );
        } )

        it( 'Pass number = empty string', function ()
        {
            expect( String.dasherize( 15 ) ).toEqual( '' );
            expect( String.dasherize( 15.8 ) ).toEqual( '' );
        } )

        it( 'Pass array = empty string', function ()
        {
            expect( String.dasherize( [] ) ).toEqual( '' );
        } );

        it( 'Pass boolean = empty string', function ()
        {
            expect( String.dasherize( false ) ).toEqual( '' );
            expect( String.dasherize( true ) ).toEqual( '' );
        } );

        it( 'Pass Object = empty string', function ()
        {
            expect( String.dasherize( {} ) ).toEqual( '' );

        } );

        it( 'Pass spaced string', function ()
        {
            expect( String.dasherize( 'hola que tal' ) ).toEqual( 'hola que tal' );
        } );

        it( 'Pass camelized string', function ()
        {
            expect( String.dasherize( 'HolaPepito' ) ).toEqual( 'holapepito' );
        } );

        it( 'Pass capitalized string', function ()
        {
            expect( String.dasherize( 'Holapepito' ) ).toEqual( 'holapepito' );
        } );

        it( 'Pass uncapitalized string', function ()
        {
            expect( String.dasherize( 'holapepito' ) ).toEqual( 'holapepito' );
        } );

        it( 'With separator', function ()
        {
            expect( String.dasherize( 'Hola Pepito', ' ' ) ).toEqual( 'hola-pepito' );
            expect( String.dasherize( 'Hola.Pepito' ) ).toEqual( 'hola-pepito' );
        } );

    } );


}
);
