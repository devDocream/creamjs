define( [
        'component/Class/Class'
    ],
    function classSpec ( Class )
    {
        Class(
            'Vehicle',
            //attr
            {
                type    : 'vehicle',
                wheels  : 0,
                param1  : 0,
                param2  : 0,
                param3  : 0,
                param4  : 0,
                wheels2 : 200
            },
            //private
            {
                mount   : function mount ( wheels )
                {
                    return this.set( 'wheels', wheels );
                },

                mounted : function mounted ()
                {
                    return this.get( 'wheels' );
                }
            },
            //public
            {
                setup   : function setup ( param1, param2 )
                {
                    this.set( 'param3', param1 );
                    this.set( 'param4', param2 );

                    this._super.apply( this,  arguments );
                },

                init    : function init ( param1, param2 )
                {
                    this.set( 'param1', param1 );
                    this.set( 'param2', param2 );
                },

                whoAmI  : function whoAmI ()
                {
                    this.set( 'type', 'vehicleNew' );
                    return this.get( 'type' );
                }
            }
        );

        describe( 'Class Suite Instantiation', function instantiation ()
        {

            var v = new Vehicle();

            it( 'Global var with class created is defined', function ()
            {
                expect( window.Vehicle ).toBeDefined();
            } )

            it( 'Class is a constructor', function ()
            {
                expect( typeof window.Vehicle ).toEqual( 'function' );
            } )

            it( 'Class global var defined is constructor after instantiation', function ()
            {
                expect( typeof  window.Vehicle ).toEqual( 'function' );
            } );

            it( 'Class Instance is a object', function ()
            {
                expect( typeof  v ).toEqual( 'object' );
            } );
        } );

        describe( 'Class Suite Get-Set', function setget ()
        {
            var v = new Vehicle();

            it( 'Class Set method has Chaining', function ()
            {
                expect( v.set( 'wheels', 5 )._name ).toEqual( 'Vehicle' );
            } );

            it( 'Set value from private method and get in public method', function ()
            {
                v.self.mount( 7 );
                expect( v.get( 'wheels' ) ).toEqual( 7 );
            } );

            it( 'No chaining in set private method', function ()
            {
                v.self.mount( 7 );
                expect( v.get( 'wheels' )._name ).toBeUndefined();
            } );

            it( 'Set&Get in method public', function ()
            {
                v.set( 'wheels', 5 );
                expect( v.get( 'wheels' ) ).toEqual( 5 );
            } );

            it( 'Set&Get in private method', function ()
            {
                v.self.set( 'wheels', 10 );
                expect( v.self.get( 'wheels' ) ).toEqual( 10 );
            } );

            it( 'Set value from public method and get in private method', function ()
            {
                v.set( 'wheels', 5 );
                expect( v.self.mounted() ).toEqual( 5 );
            } );

            it( 'Call public method from class instance', function ()
            {
                expect( v.whoAmI() ).toEqual( 'vehicleNew' );
            } );

            it( 'Call private method from class instance', function ()
            {
                expect( v.self.mounted() ).toEqual( 5 );
            } );
        } );

        describe( 'Class Suite Init-Setup', function initsetup ()
        {
            it( 'Pass parameters to setup ', function ()
            {
                var v = new Vehicle( 'hola', 23 );
                expect( v.get( 'param3' ) ).toEqual( 'hola' );
                expect( v.get( 'param4' ) ).toEqual( 23 );
            } );

            it( 'Pass parameters to init', function ()
            {
                var v = new Vehicle( 'adios', 123 );
               // expect( v.get( 'param1' ) ).toEqual( 'adios' );
                //expect( v.get( 'param2' ) ).toEqual( 123 );
            } );

        } );

        describe( 'Class Suite Extend', function extendclass ()
        {
            it( 'No create Class without arguments', function ()
            {
                expect( Class() ).toEqual( 'First argument for Class constructor must be a string' );
            } );

            it( 'No create Class without name string', function ()
            {
                expect( Class( 1245 ) ).toEqual( 'First argument for Class constructor must be a string' );
                expect( Class( {hola : 'adios'} ) ).toEqual( 'First argument for Class constructor must be a string' );
            } );

            it( 'Create Class only with name', function ()
            {
                expect( typeof Class( 'OneClass' ) ).toEqual( 'function' );
                expect( window.OneClass ).toBeDefined();
                expect( new OneClass()._name ).toEqual( 'OneClass' );

            } );

            it( 'Create Class only with name and with extend method', function ()
            {
                expect( typeof Class.extend( 'OneClass2' ) ).toEqual( 'function' );
                expect( window.OneClass2 ).toBeDefined();
                expect( new OneClass2()._name ).toEqual( 'OneClass2' );

            } );

            it( 'Create Class with deep namespace', function ()
            {
                expect( typeof Class.extend( 'Name1.Name2.OneClass2' ) ).toEqual( 'function' );
                expect( window.Name1.Name2.OneClass2 ).toBeDefined();
            } );

            it( 'Vars _name, _shortname, _type, _class defined in class instance', function ()
            {
                expect( typeof Class.extend( 'Name1.Name2.OneClass2' ) ).toEqual( 'function' );
                expect( window.Name1.Name2.OneClass2 ).toBeDefined();
                var n = new Name1.Name2.OneClass2();
                expect( n._name ).toEqual( 'Name1.Name2.OneClass2' );
                expect( n._shortName ).toEqual( 'OneClass2' );
                expect( n._type ).toEqual( 'class' );
                expect( n._cls ).toEqual( 'name1-name2-oneclass2' );
            } );

        } );

        describe( 'Class heritance', function heritance ()
        {

            Vehicle(
                'Vehicle.Car',
                {
                    car : 'car'
                }
            );

            Vehicle.Car(
                'Car.Truck',
                {
                    vehicle : 'truck',
                    wheels2 : 100
                },
                {
                    mounted : function mounted ()
                    {
                        this.set( 'wheels', 2 );
                        return this._super();
                    },

                    methodPriv : function methodPriv ()
                    {
                        return 'private';
                    }
                },
                {
                    init        : function init ( param1, param2 )
                    {
                        this._super( param1, param2 );
                    },

                    setup       : function setup ( param1, param2 )
                    {
                        this._super( param1, param2 );
                    },

                    whoAmI      : function whoAmI ()
                    {
                        var s = this._super();

                        return 'CarTruck | ' + s;
                    },

                    methodPublic : function methodPublic ()
                    {
                        return 'public';
                    }
                }
            );

            Vehicle(
                'Vehicle.Car2',
                {
                    car : 'car'
                },
                {
                    plane : 'plane'
                }
            );

            it( 'Extending first level', function ()
            {
                expect( window.Vehicle.Car ).toBeDefined();
                expect( typeof window.Vehicle.Car ).toBe( 'function' );

                var v = new Vehicle.Car( 'hola', 23 );
                expect( v.get( 'param3' ) ).toEqual( 'hola' );
                expect( v.get( 'param4' ) ).toEqual( 23 );

                var v = new Vehicle.Car( 'adios', 123 );
                expect( v.get( 'param1' ) ).toEqual( 'adios' );
                expect( v.get( 'param2' ) ).toEqual( 123 );
            } );

            it( 'Extending second level', function ()
            {
                expect( window.Car.Truck ).toBeDefined();
                expect( typeof window.Car.Truck ).toBe( 'function' );

                var v = new Car.Truck( 'hola', 23 );
                expect( v.get( 'param3' ) ).toEqual( 'hola' );
                expect( v.get( 'param4' ) ).toEqual( 23 );

                var v = new Car.Truck( 'adios', 123 );
                expect( v.get( 'param1' ) ).toEqual( 'adios' );
                expect( v.get( 'param2' ) ).toEqual( 123 );

            } );

            it( 'Call _super in public method', function ()
            {
                var v = new Car.Truck( 'hola', 23 );

                expect( v.whoAmI() ).toEqual( 'CarTruck | vehicleNew' );
            } );

            it( 'Call _super in private method', function ()
            {
                var v = new Car.Truck( 'hola', 23 );

                expect( v.self.mounted() ).toEqual( 2 );
            } );

            it( 'Call _super in init method', function ()
            {
                var v = new Car.Truck( 'adios', 123 );
                expect( v.get( 'param1' ) ).toEqual( 'adios' );
                expect( v.get( 'param2' ) ).toEqual( 123 );
            } );

            it( 'Call _super in setup method', function ()
            {
                var v = new Car.Truck( 'hola', 23 );
                expect( v.get( 'param3' ) ).toEqual( 'hola' );
                expect( v.get( 'param4' ) ).toEqual( 23 );
            } );

            it( 'Add method to public', function ()
            {
                var v = new Car.Truck();
                expect( v.methodPublic( 'param3' ) ).toEqual( 'public' );
            } );

            it( 'Add method to private', function ()
            {
                var v = new Car.Truck();
                expect( v.self.methodPriv() ).toEqual( 'private' );
            } );

            it( 'Add attribute', function ()
            {
                var v2 = new Car.Truck();
                expect( v2.get( 'vehicle' ) ).toEqual( 'truck' );
            } );

            it( 'Overwrite an attribute', function ()
            {
                var v2 = new Car.Truck();
                expect( v2.get( 'wheels2' ) ).toEqual( 100 );
            } );

            it( 'If pass only two parameters, name and object, the object is public', function ()
            {
                var v = new Vehicle.Car();
                expect( v.car ).toEqual( 'car' );
            } );

            it( 'Pass three parameters, name and 2 object, first object is attribute, second public',
                function ()
                {
                    var v = new Vehicle.Car2();
                    console.log(v);
                    expect( v.attr.car ).toEqual( 'car' );
                    expect( v.plane ).toEqual( 'plane' );
                }
            );
        } );
    }
);
