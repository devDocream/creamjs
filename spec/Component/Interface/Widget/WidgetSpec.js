define( [
        'cream',
        'vendor/creamjs/spec/Component/Interface/Widget/view/widgetspec.twig!'
],
function widgetSpec ( cream, view )
{

    var items = [
            {
                title   : 'hola',
                value   : 'hola'
            },
            {
                title   : 'adios',
                value   : 'adios'
            }
        ],
        sb = cream.widget(
                'Hola.Prueba',
                {
                    view        : view,
                    selectors   : {
                                 checks: '.item-check'
                    }
                },
                {
                    pro: 'private'
                },
                {
                    init    : function( selector, options, alias )
                    {
                        if ( selector === 'body' && options.items !== undefined &&
                             alias === 'dummy' )
                        {
                            checkInit = true;
                        }
                    },

                    setup    : function( selector, options, alias )
                    {
                            if ( selector === 'body' && options.items !== undefined &&
                            alias === 'dummy' )
                            {
                                checkSetup = true;
                            }

                        return this._super.apply( this, arguments );
                    }
                },
                {
                    '.hola .item-check click' : function itemClick( ){
                        console.log( arguments, 'click' )
                    },

                    '.item-title.selected click' : function itemClick( ){
                        console.log( arguments, 'click2' )
                    }

                },
                {
                    Model : {

                        'USERS:CHANGED' : function usersChanged ( options )
                        {

                        }
                    },
                    'Widget:alias' : {

                        'REMOVED' : function aliasRemoved ( options )
                        {

                        },
                        'USERS:CHANGED' : function usersChanged ( options )
                        {

                        }
                    }
                }
            );

    var wig = new sb( 'body',{ items :  items }, 'dummy' );
    describe( 'Attributes', function instantiation ()
    {


    } );

    describe( 'Init-setup', function instantiation ()
    {

        it( 'Constructor parameters are passed to init', function ()
        {
            expect( checkInit ).toEqual( true );

        } );

        it( 'Constructor parameters are passed to setup', function ()
        {
            expect( checkSetup ).toEqual( true );

        } );

    } );

    describe( 'Controls', function instantiation ()
    {

        it( 'Controls are bound', function ()
        {
            expect( wig.controls ).toBeDefined();
            expect( cream.keysLength( wig.controls ) ).toBeDefined( 5 );

        } );
    } );

    describe( 'Services', function instantiation ()
    {

        it( 'Service events are bound', function ()
        {
                expect( wig.services ).toBeDefined();
            expect( cream.keysLength( wig.services ) ).toBeDefined( 2 );
        } );
    } );

    describe( 'Life cycle', function instantiation ()
    {

        var items = [
                {
                    title   : 'hola',
                    value   : 'hola'
                },
                {
                    title   : 'adios',
                    value   : 'adios'
                },
                {
                    title   : 'que',
                    value   : 'tal'
                }
            ];

        wig.set( 'items' , items );
        wig.update( );
        it( 'Widget is updated', function ()
        {
            var selectors = wig.selectors( 'checks' );

            expect( selectors.length ).toEqual( 3 );

        } );


    } );

    describe( 'Functionality', function instantiation ()
    {

        it( 'Get precached element', function ()
        {
            var checks = wig.element.find( '.item-check' ).is( wig.selectors( 'checks' ) );

            expect( checks ).toEqual( true );

        } );

        it( 'Get service', function ()
        {
            var srv = wig.service( 'view' );

            expect( srv._type ).toEqual( 'cream-service' );
            expect( srv._shortName ).toEqual( 'View' );
        } );

        it( 'Element is defined', function ()
        {
            var result = $( '.hola-prueba' ).is( wig.element );

            expect( result ).toEqual( true );

        } );


        it( 'Alias is defined', function ()
        {
            expect( wig.alias ).toEqual( 'dummy' );

        } );

    } );

} );
