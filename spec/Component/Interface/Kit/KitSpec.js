define( [
    'cream'
],
function KitSpec ( cream )
{

    var items = [
            {
                title   : 'hola',
                value   : 'hola'
            },
            {
                title   : 'adios',
                value   : 'adios'
            }
        ],
        kit =  cream.kit
        (
            'Foo.Kit.Bar',

            //Attr
            {
                refresh:
                {
                    FooWidget :
                    {
                        'REFRESHED' : ['/NATIONS/', '/FOO/BAR/', '/FOO/BAR/2/']
                    },

                    'BarWidget:foo':
                    {
                        'REFRESHED' : '/NATIONS/'
                    }
                }
            },

            //Private
            {

            },

            //Public
            {

            },

            //Controls
            {

            },

            //Events
            {
                FooWidget :                                                     //Widget name
                {                                                               //Plugin name

                    'REFRESHED' : function refreshed ( options ) //Event subscriber
                    {

                    }
                },

                'BarWidget:foo':                               //WidgetName:WidgetAlias
                {                                               //PluginName:PluginAlias

                    'REFRESHED' : function refreshedWithAlias ( options ) //Event subscriber
                    {

                    }
                }
            },
            //Services
            {

            },

            //Collections
            {
                '/NATIONS/' :   function nationsCollection ( options )         //Collection name
                {
                    return new Date().getTime();
                },

                '/FOO/BAR/' :   function fooBarCollection ( options )         //Collection name
                {
                    if ( cream.notUndefined( options ) )
                    {
                        return options.msg;
                    }


                    return 'FOOBAR' + new Date().getTime();
                },

                '/FOO/BAR/2/' :   function fooBarCollection ( options )         //Collection name
                {
                    console.log( 'actualiza foo bar 2', options)
                    if ( cream.notUndefined( options ) )
                    {
                        return options.msg;
                    }

                    return 'FOOBAR' + new Date().getTime();
                }
        } );

    var instance = new kit( 'body', {}, 'fooBar' ),
        ifz      = cream.get( 'interface' );

    describe( 'COLLECTIONS', function instantiation ()
    {
        it( 'At init, precache all collections', function ()
        {
            expect( System.Cream.Container.attr.service.Interface.attr.collections[ '/FOO/BAR/' ] )
                .toBeDefined();
            expect( System.Cream.Container.attr.service.Interface.attr.collections[ '/NATIONS/' ] )
                .toBeDefined();
        } );

        it( 'At init, generate all events subscribers for auto-refresh collections', function ()
        {
            expect( instance.collectionSubscribers[ 'BarWidget:foo'].REFRESHED[ '/NATIONS/' ] )
                .toBeDefined();
            expect( typeof instance.collectionSubscribers[ 'BarWidget:foo'].REFRESHED[ '/NATIONS/'].fn )
                .toEqual( 'function' );
            expect( instance.collectionSubscribers[ 'FooWidget'].REFRESHED[ '/FOO/BAR/' ] )
                .toBeDefined();
            expect( typeof instance.collectionSubscribers[ 'FooWidget'].REFRESHED[ '/FOO/BAR/'].fn )
                .toEqual( 'function' );
            expect( instance.collectionSubscribers[ 'FooWidget'].REFRESHED[ '/NATIONS/'  ] )
                .toBeDefined();
            expect( typeof instance.collectionSubscribers[ 'FooWidget'].REFRESHED[ '/NATIONS/'].fn )
                .toEqual( 'function' );
        } );

        it( 'Get precached collection result', function ()
        {
            expect(  ifz.collection( instance, '/FOO/BAR/' ) )
                .toEqual(  System.Cream.Container.attr.service.Interface.attr.collections[ '/FOO/BAR/' ] );

            expect(  ifz.collection( instance, '/NATIONS/' ) )
                .toEqual(  System.Cream.Container.attr.service.Interface.attr.collections[ '/NATIONS/' ] );

        } );

        it( 'Get precached collection result forcing refresh', function ()
        {
            var prev = System.Cream.Container.attr.service.Interface.attr.collections[ '/FOO/BAR/'],
                prev2 = System.Cream.Container.attr.service.Interface.attr.collections[ '/NATIONS/' ];

            expect( ifz.collection( instance, '/FOO/BAR/', true ) )
                .not.toEqual( prev );

            expect( ifz.collection( instance, '/NATIONS/', true ) )
                .not.toEqual( prev2 );

            expect(  ifz.collection( instance, '/FOO/BAR/' ) )
                .toEqual(  System.Cream.Container.attr.service.Interface.attr.collections[ '/FOO/BAR/' ] );

            expect(  ifz.collection( instance, '/NATIONS/' ) )
                .toEqual(  System.Cream.Container.attr.service.Interface.attr.collections[ '/NATIONS/' ] );

        } );

        it( 'Manual refresh collection', function ()
        {
            var prev = System.Cream.Container.attr.service.Interface.attr.collections[ '/FOO/BAR/'],
                prev2 = System.Cream.Container.attr.service.Interface.attr.collections[ '/NATIONS/' ];

            expect( ifz.refreshCollection( instance, '/FOO/BAR/' ) )
                .not.toEqual( prev );

            expect( ifz.refreshCollection( instance, '/NATIONS/' ) )
                .not.toEqual( prev2 );

            expect(  ifz.collection( instance, '/FOO/BAR/' ) )
                .toEqual(  System.Cream.Container.attr.service.Interface.attr.collections[ '/FOO/BAR/' ] );

            expect(  ifz.collection( instance, '/NATIONS/' ) )
                .toEqual(  System.Cream.Container.attr.service.Interface.attr.collections[ '/NATIONS/' ] );

        } );

        it( 'Pass options to manual refresh collection', function ()
        {
            var prev3 = System.Cream.Container.attr.service.Interface.attr.collections[ '/FOO/BAR/2/'];

            expect( ifz.refreshCollection( instance, '/FOO/BAR/2/', { msg: 'hola'} ) )
                .not.toEqual( prev3 );

            expect(  ifz.collection( instance, '/FOO/BAR/2/' ) )
                .toEqual(  System.Cream.Container.attr.service.Interface.attr.collections[ '/FOO/BAR/2/' ] );

            expect(  ifz.collection( instance, '/FOO/BAR/2/' ) )
                .toEqual( 'hola' );
        } );

       var  widget = {
            attr: {

            },
            _type       : 'cream-widget',
            _shortName : 'FooWidget'

        }, plugin = {
                attr : {},

                alias      : 'foo',
                _type      : 'cream-plugin',
                _shortName : 'BarWidget'
            };

        it( 'Auto refresh collection by trigger event', function ()
        {
            var prev = System.Cream.Container.attr.service.Interface.attr.collections[ '/FOO/BAR/2/'];

            ifz.publish( widget, 'REFRESHED', { msg : 'adios' } );

            expect(  ifz.collection( instance, '/FOO/BAR/2/' ) )
                .toEqual(  System.Cream.Container.attr.service.Interface.attr.collections[ '/FOO/BAR/2/' ] );

            expect(  ifz.collection( instance, '/FOO/BAR/2/' ) )
                .not.toEqual(  prev );

            expect(  ifz.collection( instance, '/FOO/BAR/2/' ) )
                .toEqual( 'adios' );
        } );


    } );

} );
