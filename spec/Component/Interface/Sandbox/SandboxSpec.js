define( [
        'cream'
    ],
function sandboxSpec ( cream )
{
    var sb = cream.sandbox(
        'Foo.Sandbox.Bar',
        {
            attr: 'attr'
        },
        {
            pro: 'private'
        },
        {
            attr: 'public'
        },
        {
            attr: 'events'
        },
        {
            attr: 'services'
        },
        {
            attr: 'collections'
        }
    );

   var instance = new sb( { attrFoo: 'FOO' } );

    describe( 'SANDBOX', function instantiation ()
    {
        it( 'Auto define body like element', function ()
        {
            expect( instance.selector ).toEqual( 'body' );
            expect( $( document.body ).hasClass( 'cream-sandbox' ) ).toEqual( true );
        } );

        it( 'Set alias to sandbox', function ()
        {
            expect( instance.alias ).toEqual( 'sandbox' );
        } );

        it( 'Extend attrs with options', function ()
        {
            expect( instance.attr.attrFoo ).toBeDefined();
            expect( instance.attr.attrFoo ).toEqual( 'FOO' );
        } );

        it( 'Extend fine the parts ', function ()
        {
            expect( instance.services.attr ).toEqual( 'services' );
            expect( instance.collections.attr ).toEqual( 'collections' );
            expect( instance.events.attr ).toEqual( 'events' );
        } );


    } );
} );
