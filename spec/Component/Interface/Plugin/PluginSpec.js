define( [
    'cream'
],
function PluginSpec ( cream )
{

    var items = [
            {
                title   : 'hola',
                value   : 'hola'
            },
            {
                title   : 'adios',
                value   : 'adios'
            }
        ],
        plugin = cream.plugin(
            'Hola.Plugin',
            {

            },
            {

            },
            {
                init: function()
                {
                    console.log( this.addWidget( 'prueba', 'body', {}, 'dummy2' ) );
                }
            },
            {


            },
            {
                'MODEL':
                {
                    triggered: true
                }
            },
            {

            }
        ),
        sb = cream.widget(
            'Hola.Prueba',
            {

                selectors   : {
                    checks: '.item-check'
                }
            },
            {
                pro: 'private'
            },
            {
                init    : function( selector, options, alias )
                {
                    if ( selector === 'body' && options.items !== undefined &&
                         alias === 'dummy' )
                    {
                        checkInit = true;
                    }
                },

                setup    : function( selector, options, alias )
                {
                    if ( selector === 'body' && options.items !== undefined &&
                         alias === 'dummy' )
                    {
                        checkSetup = true;
                    }

                    return this._super.apply( this, arguments );
                }
            },
            {
                '.hola .item-check click' : function itemClick( ){
                    console.log( arguments, 'click' )
                },

                '.item-title.selected click' : function itemClick( ){
                    console.log( arguments, 'click2' )
                }

            },
            {
                Model : {

                    'USERS:CHANGED' : function usersChanged ( options )
                    {

                    }
                },
                'Widget:alias' : {

                    'REMOVED' : function aliasRemoved ( options )
                    {

                    },
                    'USERS:CHANGED' : function usersChanged ( options )
                    {

                    }
                }
            }
        );

    var wig = new sb( 'body',{ items :  items }, 'dummy' );

    var p = new plugin( 'body', { items :  items }, 'dummyPlugin' );

    describe( 'ADD WIDGET', function instantiation ()
    {
        it( 'Trigger events without alias', function ()
        {

        } );

    } );

    describe( 'FIND  WIDGET', function instantiation ()
    {
        it( 'Trigger events without alias', function ()
        {


        } );

    } );

    describe( 'FINANDREMOVE  WIDGET', function instantiation ()
    {
        it( 'Trigger events without alias', function ()
        {

        } );

    } );

} );
