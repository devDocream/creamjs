define( [
    'framework/Routing/Routing'
],
function routingSpec ( Routing )
{
    var routes =
    {
            '/vendor/creamjs/spec/Framework/Routing/RoutingSpec.html': {
                development   : 'DoCream/DemoBunch/Sandbox/DemoSandbox',
                production    : 'dist/bundles/DemoSandbox',
                load          : {
                    url      : '/load/cream',
                    callback : function callback ( data )
                    {
                    }
                }
            },
            '/cream/aux2' : {
                development : 'DoCream/DemoBunch/Sandbox/AuxiliarSandbox',
                production  : 'dist/bundles/AuxiliarSandbox',
                load        : {
                    url : '/load/auxcream'
                }
            },
            '/cream/{id}' : {
                development : 'DoCream/DemoBunch/Sandbox/AuxiliarSandbox',
                production  : 'dist/bundles/AuxiliarSandbox',
                load        : {
                    url : '/load/auxcream'
                }
            },
            '/cream/{id}/hola' : {
                development : 'DoCream/DemoBunch/Sandbox/AuxiliarSandbox',
                production  : 'dist/bundles/AuxiliarSandbox',
                load        : {
                    url : '/load/auxcream'
                }
            },
            '/{name}/cream/hola' : {
                development : 'DoCream/DemoBunch/Sandbox/AuxiliarSandbox',
                production  : 'dist/bundles/AuxiliarSandbox',
                load        : {
                    url : '/load/auxcream'
                }
            },
            '/cream/aux' : {
                development : 'DoCream/DemoBunch/Sandbox/AuxiliarSandbox',
                production  : 'dist/bundles/AuxiliarSandbox',
                load        : {
                    url : '/load/auxcream'
                }
            },
            '/cream/{name}/adios/{id}/aux' : {
                development : 'DoCream/DemoBunch/Sandbox/AuxiliarSandbox',
                production  : 'dist/bundles/AuxiliarSandbox',
                load        : {
                    url : '/load/auxcream'
                }
            }

    };

    describe( 'Routing', function instantiation ()
    {

        it( 'Not existing route', function ()
        {
            var route = Routing
                .setPath( Routing.realPath( System.baseURL + 'aux' ) )
                .setRoutes( routes )
                .match();


            expect( route.route ).toBeUndefined();
            expect( route.path ).toBeUndefined();
            expect( route.vars ).toBeUndefined();
            expect( route.params ).toBeUndefined();
            expect( route.hash ).toBeUndefined();
        } );

        it( 'With html file', function ()
        {
            var route = Routing
                .setPath( Routing.realPath( ) )
                .setRoutes( routes )
                .match();

            expect( route.path ).toEqual( '/vendor/creamjs/spec/Framework/Routing/RoutingSpec.html' );
            expect( route.route ).toEqual( '/vendor/creamjs/spec/Framework/Routing/RoutingSpec.html' );
        } );


        it( 'Final variable', function ()
        {
            var route = Routing
                .setPath( Routing.realPath( System.baseURL + 'cream/1' ) )
                .setRoutes( routes )
                .match();


            expect( route.route ).toEqual( '/cream/{id}' );
            expect( route.path ).toEqual( '/cream/1' );
            expect( route.vars.id ).toEqual( '1' );
            expect( route.params ).toBeUndefined();
            expect( route.hash ).toBeUndefined();

        } );

        it( 'Intermediary variable', function ()
        {


            var route = Routing
                .setPath( Routing.realPath( System.baseURL + 'cream/1/hola' ) )
                .setRoutes( routes )
                .match();


            expect( route.route ).toEqual( '/cream/{id}/hola' );
            expect( route.path ).toEqual( '/cream/1/hola' );
            expect( route.vars.id ).toEqual( '1' );
            expect( route.params ).toBeUndefined();
            expect( route.hash ).toBeUndefined();

        } );

        it( 'Initial variable', function ()
        {

            var route = Routing
                .setPath( Routing.realPath( System.baseURL + 'fernando/cream/hola' ) )
                .setRoutes( routes )
                .match();


            expect( route.route ).toEqual( '/{name}/cream/hola' );
            expect( route.path ).toEqual( '/fernando/cream/hola' );
            expect( route.vars.name ).toEqual( 'fernando' );
            expect( route.params ).toBeUndefined();
            expect( route.hash ).toBeUndefined();

        } );

        it( 'Two similar routes, one with variable and other with implicit value', function ()
        {
            var route = Routing
                .setPath( Routing.realPath( System.baseURL + 'cream/aux' ) )
                .setRoutes( routes )
                .match();


            expect( route.route ).toEqual( '/cream/{id}' );
            expect( route.path ).toEqual( '/cream/aux' );
            expect( route.vars.id ).toEqual( 'aux' );
            expect( route.params ).toBeUndefined();
            expect( route.hash ).toBeUndefined();

            route = Routing
                .setPath( Routing.realPath( System.baseURL + 'cream/aux2' ) )
                .setRoutes( routes )
                .match();


            expect( route.route ).toEqual( '/cream/aux2' );
            expect( route.path ).toEqual( '/cream/aux2' );
            expect( route.vars.id ).toBeUndefined();
            expect( route.params ).toBeUndefined();
            expect( route.hash ).toBeUndefined();

        } );

        it( 'Multiple vars', function ()
        {
            var route = Routing
                .setPath( Routing.realPath( System.baseURL + 'cream/fernando/adios/10/aux' ) )
                .setRoutes( routes )
                .match();


            expect( route.route ).toEqual( '/cream/{name}/adios/{id}/aux' );
            expect( route.path ).toEqual( '/cream/fernando/adios/10/aux' );
            expect( route.vars.id ).toEqual( '10' );
            expect( route.vars.name ).toEqual( 'fernando' );
            expect( route.params ).toBeUndefined();
            expect( route.hash ).toBeUndefined();

        } );

        it( 'Get Hash', function ()
        {
            var route = Routing
                .setPath( Routing.realPath( System.baseURL + 'cream/fernando/adios/10/aux#/hash1' ) )
                .setRoutes( routes )
                .match();


            expect( route.route ).toEqual( '/cream/{name}/adios/{id}/aux' );
            expect( route.path ).toEqual( '/cream/fernando/adios/10/aux#/hash1' );
            expect( route.vars.id ).toEqual( '10' );
            expect( route.vars.name ).toEqual( 'fernando' );
            expect( route.params ).toBeUndefined();
            expect( route.hash ).toBeDefined();
            expect( route.hash ).toEqual( '/hash1' );

        } );

        it( 'Get Query params', function ()
        {
            var route = Routing
                .setPath( Routing.realPath( System.baseURL + 'cream/fernando/adios/10/aux?hola=1&adios=0' ) )
                .setRoutes( routes )
                .match();


            expect( route.route ).toEqual( '/cream/{name}/adios/{id}/aux' );
            expect( route.path ).toEqual( '/cream/fernando/adios/10/aux?hola=1&adios=0' );
            expect( route.vars.id ).toEqual( '10' );
            expect( route.vars.name ).toEqual( 'fernando' );
            expect( route.hash ).toBeUndefined();
            expect( route.params ).toBeDefined();
            expect( route.params ).toEqual( 'hola=1&adios=0' );

        } );

        it( 'Query params and hash together', function ()
        {


            var route = Routing
                .setPath( Routing.realPath( System.baseURL + 'cream/fernando/adios/10/aux#/adios?hola=1&adios=0' ) )
                .setRoutes( routes )
                .match();


            expect( route.route ).toEqual( '/cream/{name}/adios/{id}/aux' );
            expect( route.path ).toEqual( '/cream/fernando/adios/10/aux#/adios?hola=1&adios=0' );
            expect( route.vars.id ).toEqual( '10' );
            expect( route.vars.name ).toEqual( 'fernando' );
            expect( route.hash ).toBeDefined();
            expect( route.hash ).toEqual( '/adios' );
            expect( route.params ).toBeDefined();
            expect( route.params ).toEqual( 'hola=1&adios=0' );

        } );


    } );

}
);
