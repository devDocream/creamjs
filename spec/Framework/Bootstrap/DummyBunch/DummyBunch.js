define(
    function DemoBunch ()
    {
        return  {

            Cream   :
            {
                services : {
                    View      : {
                        helpers: {
                            'capsTu' : 'str.capitalize'
                        }
                    }
                },
                sandbox:{
                    data: {
                    }
                },

                utils :
                {
                    utilsBunch : 'utilsBunch',
                    overwrite   : 'bunch',
                    overwrite2   : 'bunch'
                }
            },
            Demo    :
            {
                services    :
                {
                    View      : {
                        helpers: {
                            'capsTu' : 'str.capitalize2'
                        }
                    },
                    String : {
                        path : 'DoCream/DemoBunch/Service/StringService'
                    }
                },

                sandbox     :
                {
                    data :
                    {
                        hola : 'Sandbox:hola.foo'
                    }
                },

                utils       :
                {
                    utilsSandbox : 'utilsSandbox',
                    overwrite   : 'sandbox'
                }
            }
        };

    } );
