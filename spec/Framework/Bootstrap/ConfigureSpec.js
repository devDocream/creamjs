define( [
        'framework/Bootstrap/Configure'
    ],
function routingSpec ( )
{

    describe( 'Configure', function instantiation ()
    {

        it( 'UTILS', function ()
        {
            expect( System.Cream.utils.utilsSandbox ).toBeDefined()
            expect( System.Cream.utils.utilsVendor ).toBeDefined();
            expect( System.Cream.utils.utilsBunch ).toBeDefined();
            expect( System.Cream.utils.overwrite ).toEqual( 'sandbox' );
            expect( System.Cream.utils.overwrite2 ).toEqual( 'bunch' );
            expect( System.Cream.utils.overwrite3 ).toEqual( 'vendor' );

        } );


        it( 'Data Dependency', function ()
        {
            expect( System.Cream.DD.collection[ '/elements/' ] ).toEqual( 'users' );
            expect( System.Cream.DD.kit.left.deps.stations ).toEqual( 'workStations' );
            expect( System.Cream.DD.repository.user.deps.users ).toEqual( 'users' );
            expect( System.Cream.DD.service.str.deps.init ).toEqual( 'users' );
            expect( System.Cream.DD.widget.label.label0.title ).toEqual( 'users' );
            expect( System.Cream.DD.widget.label.deps ).toBeDefined( 'users' );
            expect( System.Cream.DD.sandbox.hola.deps.foo ).toBeDefined( 'hola' );
        } );

        it( 'Load data', function ()
        {
            expect( System.Cream.data.users[ 0 ] ).toEqual( 'fernando' );
            expect( System.Cream.data.workStations[0] ).toEqual( 'administration' );
        } );


    } );


}
);
