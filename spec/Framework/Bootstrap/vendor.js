define(
function DoCream ()
{
    return  {

        Cream: {
            services:{
                View      : {
                    helpers: {
                        'capsTu' : 'str.capitalize'
                    }
                }
            },

            sandbox:{
                data: {
                    'users'         : ['Repository:User.users', 'Service:Str.init', 'Collection:/ELEMENTS/', 'widget:label:label0.title'],
                    'workStations'  : ['Kit:left.stations']
                }
            },
            utils: {
                utilsVendor : 'utilsVendor',
                overwrite   : 'vendor',
                overwrite2   : 'vendor',
                overwrite3   : 'vendor'
            }
        }
    };


} );
