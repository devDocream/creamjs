define( [
        'service/Interface/InterfaceService',
        'vendor/creamjs/spec/Service/Interface/dummy.twig!'
],
function interfaceServiceSpec ( srv, View )
{

    var ifz = cream.get( 'interface' ),
        dummy = {
                   attr: {
                            view    : '',
                            place   : {}
                   },
                    _cls        : 'dummy-spec-hola',
                    alias   : 'dummy1',
                    _type       : 'cream-interface',
                    _shortName : 'Hola',
                    _name      : 'Dummy.Spec.Hola',
                    get     : function( name )
                    {
                        return this.attr[ name ];
                    },
                    set     : function( name, value  )
                    {
                        return this.attr[ name ] = value;
                    }

        },
        dummy2 = {
            attr: {
                view    : '',
                place   : {}
            },
            _cls        : 'dummy-spec-adios',
            _type       : 'cream-auxiliar',
            _shortName : 'Adios',
            _name      : 'Dummy.Spec.Adios',
            controls:{
                        'click':  function goClick(){ window.eventService2 =  'hola foobar'; }
            },
            get     : function( name )
            {
                return this.attr[ name ];
            },
            set     : function( name, value  )
            {
                return this.attr[ name ] = value;
            }

        },
        dummy3 = {
            attr: {
                view    : '',
                place   : {}
            },
            _cls        : 'dummy-spec-quetal',
            alias       : 'dummy3',
            _type       : 'cream-interface',
            _shortName : 'QueTal',
            _name      : 'Dummy.Spec.QueTal',
            get     : function( name )
            {
                return this.attr[ name ];
            },
            set     : function( name, value  )
            {
                return this.attr[ name ] = value;
            }

        },
        dummy4 = {
            attr: {
                view    : '',
                place   : {}
            },
            _cls        : 'dummy-spec-bueno',
            _type       : 'cream-auxiliar',
            _shortName : 'Bueno',
            _name      : 'Dummy.Spec.Bueno',
            controls:{
                'click':  function goClick(){ window.eventService22 =  'hola foobar'; }
            },
            get     : function( name )
            {
                return this.attr[ name ];
            },
            set     : function( name, value  )
            {
                return this.attr[ name ] = value;
            }

        },
        dummy5 = {
            attr: {
                view    : '',
                place   : {}
            },
            _cls        : 'dummy-spec-quetal',
            alias       : 'dummy3',
            _type       : 'cream-interface',
            _shortName : 'QueTal',
            _name      : 'Dummy.Spec.QueTal',
            get     : function( name )
            {
                return this.attr[ name ];
            },
            set     : function( name, value  )
            {
                return this.attr[ name ] = value;
            }

        },
        dummy6 = {
            attr: {
                view    : '',
                place   : {}
            },
            _cls        : 'dummy-spec-bueno',
            _type       : 'cream-auxiliar',
            _shortName : 'Bueno',
            _name      : 'Dummy.Spec.Bueno',
            controls:{
                'click':  function goClick(){ window.eventService22 =  'hola foobar'; }
            },
            get     : function( name )
            {
                return this.attr[ name ];
            },
            set     : function( name, value  )
            {
                return this.attr[ name ] = value;
            }

        };


    describe( 'CREATE', function instantiation ()
    {

        var newElement = ifz.create( dummy, '.foo', 'interface' ),
            newElement2 = ifz.create( dummy2, '.foo4', 'auxiliar' );

        it ( 'Create element over the selector if it have not view', function ()
        {
            var result = $( '.foo' ).is( newElement.element ),
                result2 = $( '.foo4' ).is( newElement2.element );

            expect( result ).toEqual( true );
            expect( result2 ).toEqual( true );
        } );

        it ( 'Add proper classes', function ()
        {
            expect( newElement.element.hasClass( 'dummy-spec-hola' ) ).toEqual( true );
            expect( newElement.element.hasClass( 'cream-interface' )  ).toEqual( true );
            expect( newElement.element.hasClass( 'cream-interface-dummy1' )  ).toEqual( true );
            expect( newElement.element.hasClass( 'dummy-spec-hola-dummy1' ) ).toEqual( true );

            expect( newElement2.element.hasClass( 'dummy-spec-adios' ) ).toEqual( true );
            expect( newElement2.element.hasClass( 'cream-auxiliar' )  ).toEqual( true );

        } );

        it ( 'The new element is added to the container', function ()
        {
            var aux     = System.Cream.Container.attr.interface.Hola.dummy1,
                aux2    = System.Cream.Container.attr.auxiliar.Adios[ newElement2.id ];
            expect( aux ).toBeDefined( );
            expect( aux.id ).toEqual( newElement.id );
            expect( aux2 ).toBeDefined( );
            expect( aux2.id ).toEqual( newElement2.id );

        } );

         it ( 'Create element, render view, and locate it in the correct place', function ()
        {
            dummy2.attr.view = View;
            dummy2.controls[ '.bar click'] = function goClick2(){ window.eventService3 =  'AUXILIAR'; }

            var newElement3 = ifz.create( dummy2, '.foo4', 'auxiliar' );

            expect( $('.foo4' ).find( '.auxiliar' ).length ).toEqual( 1 );

            dummy2.alias = 'dummy-spec';
            var newElement4 = ifz.create( dummy2, 'body', 'auxiliar' ),
                elements = $('body' ).children();

            expect( $(elements[elements.length - 1]).hasClass( 'dummy-spec-adios-dummy-spec') ).toEqual( true );

            dummy2.attr.place.type = 'prepend';
            var newElement5 = ifz.create( dummy2, 'body', 'auxiliar' ),
                elements = $('body' ).children();

            expect( $(elements[0]).hasClass( 'dummy-spec-adios-dummy-spec') ).toEqual( true );

            dummy2.attr.place.type = 'after';
            dummy2.attr.place.value = '.foo6';
            var newElement5 = ifz.create( dummy2, 'body', 'auxiliar' ),
                elements    = $('.foo6').next();

            expect( elements.hasClass('dummy-spec-adios-dummy-spec' ) ).toEqual( true );

            dummy2.attr.place.type = 'before';
            dummy2.attr.place.value = '.foo6';
            var newElement5 = ifz.create( dummy2, 'body', 'auxiliar' ),
                elements    = $('.foo6').prev();

            expect( elements.hasClass('dummy-spec-adios-dummy-spec' ) ).toEqual( true );

            dummy2.attr.place.type = 'over';

            var newElement5 = ifz.create( dummy2, '.foo6', 'auxiliar' );

            expect( $( '.foo6' ).hasClass( 'dummy-spec-adios-dummy-spec' ) ).toEqual( true );




        } );

        it ( 'Bind controls events', function ()
        {

            $( '.foo4' ).click();
            expect( window.eventService2 ).toEqual( 'hola foobar' );

            $('.foo4' ).find( '.bar' ).click();
            expect(  window.eventService3 ).toEqual( 'AUXILIAR' );
        } );



    } );

    describe( 'DESTROY', function instantiation ()
    {
        dummy4.attr.view = View;
        dummy4.controls[ '.bar click'] = function goClick2(){ window.eventService33 =  'AUXILIAR'; }

        var newElement3 = ifz.create( dummy3, '.foo5', 'interface' ),
            newElement4 = ifz.create( dummy4, '.foo7', 'auxiliar' );

        ifz.destroy( newElement3, 'interface' );
        ifz.destroy( newElement4, 'auxiliar' );

        it ( 'The instance is removed from container', function ()
         {
             var aux     = System.Cream.Container.attr.interface.QueTal.dummy3,
             aux2    = System.Cream.Container.attr.auxiliar.Bueno[ newElement4.id ];
             expect( aux ).toBeUndefined( );
             expect( aux2 ).toBeUndefined( );

         } );

        it ( 'Unbind controls events', function ()
        {
            window.eventService22 = false;
            $( '.foo5' ).click();
            expect( window.eventService22 ).toEqual( false );
            window.eventService33 = false;
            $('.foo7' ).find( '.bar' ).click();
            expect(  window.eventService33 ).toEqual( false );
        } );

        it ( 'Removed interface element classes', function ()
        {
            expect( newElement3.element.hasClass( 'dummy-spec-quetal') ).toEqual( false );
            expect( newElement3.element.hasClass( 'cream-interface')  ).toEqual( false );
            expect( newElement3.element.hasClass( 'cream-interface-dummy3')  ).toEqual( false );
            expect( newElement3.element.hasClass( 'dummy-spec-quetal-dummy3') ).toEqual( false );

            expect( newElement4.element.hasClass( 'dummy-spec-bueno') ).toEqual( false );
            expect( newElement4.element.hasClass( 'cream-auxiliar')  ).toEqual( false );
        } );


    } );

    describe( 'REMOVE', function instantiation ()
    {
        dummy6.attr.view = View;
        dummy6.controls[ '.bar click'] = function goClick2(){ window.eventService33 =  'AUXILIAR'; }

        var newElement5 = ifz.create( dummy5, '.foo9', 'interface' ),
            newElement6 = ifz.create( dummy6, '.foo2', 'auxiliar' );

        ifz.remove( newElement5, 'interface' );
        ifz.remove( newElement6, 'auxiliar' );


        it ( 'Removed html interface representation from DOM', function ()
        {
            expect( $('.foo9' ).length ).toEqual( 0 );
            expect( $('.foo2' ).find( '.auxiliar' ).length ).toEqual( 0 );
        } );

    } );

    describe( 'UPDATE', function instantiation ()
    {
        dummy6.attr.view = View;
        dummy6.controls[ '.bar click'] = function goClick2(){ window.eventService33 =  'AUXILIAR'; }

        var newElement5 = ifz.create( dummy5, '.foo9', 'interface' ),
            newElement6 = ifz.create( dummy6, '.foo2', 'auxiliar' );

        ifz.remove( newElement5, 'interface' );
        ifz.remove( newElement6, 'auxiliar' );


        it ( 'Removed html interface representation from DOM', function ()
        {
            expect( $('.foo9' ).length ).toEqual( 0 );
            expect( $('.foo2' ).find( '.auxiliar' ).length ).toEqual( 0 );
        } );

    } );
    var events  = cream.get( 'event' ),
        service = {
            _type       : 'cream-service',
            id          : '198',
            _shortName  : 'Model'
        },

        widget  =
        {
            _type    : 'cream-widget',
            id       : '19888',
            _shortName : 'Poo',
            services : {
                Model :
                {
                    'FOO:BAR' : function fooBar ()
                    {
                        this.result = 'OK';
                    }
                },

                'Model:users' :
                {
                    'FOO:BAR2' : function fooBar2 ()
                    {

                    },
                    'POST:CALLBACK' : function fooBar3 ()
                    {

                    }
                },


                'Ajax:upload' :
                {
                    'POST:CALLBACK' : function fooBar3 ()
                    {

                    }
                }
            }
        },
        widgetWithAlias  =
        {
            _type    : 'cream-widget',
            id       : '199',
            _shortName : 'Poo2',
            alias      : 'car',
            services : {
                Model :
                {
                    'FOO:BAR' : function fooBar ()
                    {
                        this.result = 'OK';
                    }
                },

                'Model:users' :
                {
                    'FOO:BAR2' : function fooBar2 ()
                    {

                    },
                    'POST:CALLBACK' : function fooBar3 ()
                    {

                    }
                },


                'Ajax:upload' :
                {
                    'POST:CALLBACK' : function fooBar3 ()
                    {

                    }
                }
            }
        },

        plugin  =
        {
            _type    : 'cream-plugin',
            id       : '19889',
            _shortName: 'Poo2',
            events   : {
                Poo :
                {
                    'POO:BAR' : function fooBar ()
                    {
                        this.result = 'OK';
                    }
                },

                'Poo:car' :
                {
                    'POO:BAR2' : function fooBar2 ()
                    {

                    },
                    'FOO:BAR:BAR' : function fooBar3 ()
                    {

                    }
                },


                'Foo:bar' :
                {
                    'FOO:BAR:BAR' : function fooBar3 ()
                    {

                    }
                }
            }
        },
        kit  =
        {
            _type       : 'cream-kit',
            id          : '19890',
            _shortName  : 'FooKit',
            result      : 'BAD',
            result2     : 'BAD',
            events   : {
                Poo2 :
                {
                    'POO2:BAR' : function fooBar ( options )
                    {
                        this.result2 = 'OK';
                        this.withAlias = options;
                    }
                },

                'Poo2:car' :
                {
                    'POO2:BAR2' : function fooBar2 ()
                    {
                        this.withAlias = 'OK';
                    },
                    'FOO2:BAR:BAR' : function fooBar3 ()
                    {

                    }
                },


                'Foo2:bar' :
                {
                    'FOO2:BAR:BAR' : function fooBar3 ()
                    {

                    }
                },
                Poo :
                {
                    'POO:BAR' : function fooBar ()
                    {
                        this.result = 'OK';
                    }
                }
            }
        },
        sandbox  =
        {
            _type    : 'cream-sandbox',
            id       : '19891',
            events   : {
                Poo2 :
                {
                    'POO2:BAR' : function fooBar ()
                    {

                    }
                },

                'Poo2:car' :
                {
                    'POO2:BAR2' : function fooBar2 ()
                    {

                    },
                    'FOO2:BAR:BAR' : function fooBar3 ()
                    {

                    }
                },


                'Foo2:bar' :
                {
                    'FOO2:BAR:BAR' : function fooBar3 ()
                    {

                    }
                },

                'FooKit' : {
                    'FOO:EVENT': function fooEvent( options )
                    {

                        this.result = 'OK';
                        this.options = options;
                    }
                }

            }
        };

    describe( 'SUBSCRIBE', function instantiation ()
    {

        it( 'Subscribe from plugin to widget events', function ()
        {
            var subs;

            ifz.self.subUnsub( plugin, 'subscribe' );
            subs = events.get( 'subscribers' );

            expect( subs.widget['POO:BAR'].Poo.subscribers['19889'] ).toBeDefined( );
            expect( subs.widget['POO:BAR'].Poo.subscribers['19889'].instance ).toBeDefined( );
            expect( subs.widget['POO:BAR'].Poo.subscribers['19889'].fn ).toBeDefined( );

            expect( subs.widget['POO:BAR2'].Poo.subscribers ).toBeDefined( );
            expect( subs.widget['POO:BAR2'].Poo.car['19889'] ).toBeDefined( );
            expect( subs.widget['POO:BAR2'].Poo.car['19889'].instance ).toBeDefined( );
            expect( subs.widget['POO:BAR2'].Poo.car['19889'].instance.id ).toEqual( '19889' );
            expect( subs.widget['POO:BAR2'].Poo.car['19889'].fn ).toBeDefined( );
            expect( typeof subs.widget['POO:BAR2'].Poo.car['19889'].fn ).toEqual( 'function' );

            expect( subs.widget['FOO:BAR:BAR'].Poo.subscribers ).toBeDefined( );
            expect( subs.widget['FOO:BAR:BAR'].Poo.car['19889'] ).toBeDefined( );
            expect( subs.widget['FOO:BAR:BAR'].Poo.car['19889'].instance ).toBeDefined( );
            expect( subs.widget['FOO:BAR:BAR'].Poo.car['19889'].instance.id ).toEqual( '19889' );
            expect( subs.widget['FOO:BAR:BAR'].Poo.car['19889'].fn ).toBeDefined( );

            expect( subs.widget['FOO:BAR:BAR'].Foo.subscribers ).toBeDefined( );
            expect( subs.widget['FOO:BAR:BAR'].Foo.bar['19889'] ).toBeDefined( );
            expect( subs.widget['FOO:BAR:BAR'].Foo.bar['19889'].instance ).toBeDefined( );
            expect( subs.widget['FOO:BAR:BAR'].Foo.bar['19889'].instance.id ).toEqual( '19889' );
            expect( subs.widget['FOO:BAR:BAR'].Foo.bar['19889'].fn ).toBeDefined( );
            expect( typeof subs.widget['FOO:BAR:BAR'].Foo.bar['19889'].fn ).toEqual( 'function' );

        } );

        it( 'Subscribe from kit to plugin events', function ()
        {
            var subs;

            ifz.self.subUnsub( kit, 'subscribe' );
            subs = events.get( 'subscribers' );

            expect( subs.plugin['POO2:BAR'].Poo2.subscribers['19890'] ).toBeDefined( );
            expect( subs.plugin['POO2:BAR'].Poo2.subscribers['19890'].instance ).toBeDefined( );
            expect( subs.plugin['POO2:BAR'].Poo2.subscribers['19890'].fn ).toBeDefined( );

            expect( subs.plugin['POO2:BAR2'].Poo2.subscribers ).toBeDefined( );
            expect( subs.plugin['POO2:BAR2'].Poo2.car['19890'] ).toBeDefined( );
            expect( subs.plugin['POO2:BAR2'].Poo2.car['19890'].instance ).toBeDefined( );
            expect( subs.plugin['POO2:BAR2'].Poo2.car['19890'].instance.id ).toEqual( '19890' );
            expect( subs.plugin['POO2:BAR2'].Poo2.car['19890'].fn ).toBeDefined( );
            expect( typeof subs.plugin['POO2:BAR2'].Poo2.car['19890'].fn ).toEqual( 'function' );

            expect( subs.plugin['FOO2:BAR:BAR'].Poo2.subscribers ).toBeDefined( );
            expect( subs.plugin['FOO2:BAR:BAR'].Poo2.car['19890'] ).toBeDefined( );
            expect( subs.plugin['FOO2:BAR:BAR'].Poo2.car['19890'].instance ).toBeDefined( );
            expect( subs.plugin['FOO2:BAR:BAR'].Poo2.car['19890'].instance.id ).toEqual( '19890' );
            expect( subs.plugin['FOO2:BAR:BAR'].Poo2.car['19890'].fn ).toBeDefined( );

            expect( subs.plugin['FOO2:BAR:BAR'].Foo2.subscribers ).toBeDefined( );
            expect( subs.plugin['FOO2:BAR:BAR'].Foo2.bar['19890'] ).toBeDefined( );
            expect( subs.plugin['FOO2:BAR:BAR'].Foo2.bar['19890'].instance ).toBeDefined( );
            expect( subs.plugin['FOO2:BAR:BAR'].Foo2.bar['19890'].instance.id ).toEqual( '19890' );
            expect( subs.plugin['FOO2:BAR:BAR'].Foo2.bar['19890'].fn ).toBeDefined( );
            expect( typeof subs.plugin['FOO2:BAR:BAR'].Foo2.bar['19890'].fn ).toEqual( 'function' );


        } );

        it( 'Subscribe from kit to widget events', function ()
        {

            var subs;

            subs = events.get( 'subscribers' );

            expect( subs.widget['POO2:BAR'].Poo2.subscribers['19890'] ).toBeDefined( );
            expect( subs.widget['POO2:BAR'].Poo2.subscribers['19890'].instance ).toBeDefined( );
            expect( subs.widget['POO2:BAR'].Poo2.subscribers['19890'].fn ).toBeDefined( );

            expect( subs.widget['POO2:BAR2'].Poo2.subscribers ).toBeDefined( );
            expect( subs.widget['POO2:BAR2'].Poo2.car['19890'] ).toBeDefined( );
            expect( subs.widget['POO2:BAR2'].Poo2.car['19890'].instance ).toBeDefined( );
            expect( subs.widget['POO2:BAR2'].Poo2.car['19890'].instance.id ).toEqual( '19890' );
            expect( subs.widget['POO2:BAR2'].Poo2.car['19890'].fn ).toBeDefined( );
            expect( typeof subs.widget['POO2:BAR2'].Poo2.car['19890'].fn ).toEqual( 'function' );

            expect( subs.widget['FOO2:BAR:BAR'].Poo2.subscribers ).toBeDefined( );
            expect( subs.widget['FOO2:BAR:BAR'].Poo2.car['19890'] ).toBeDefined( );
            expect( subs.widget['FOO2:BAR:BAR'].Poo2.car['19890'].instance ).toBeDefined( );
            expect( subs.widget['FOO2:BAR:BAR'].Poo2.car['19890'].instance.id ).toEqual( '19890' );;
            expect( subs.widget['FOO2:BAR:BAR'].Poo2.car['19890'].fn ).toBeDefined( );

            expect( subs.widget['FOO2:BAR:BAR'].Foo2.subscribers ).toBeDefined( );
            expect( subs.widget['FOO2:BAR:BAR'].Foo2.bar['19890'] ).toBeDefined( );
            expect( subs.widget['FOO2:BAR:BAR'].Foo2.bar['19890'].instance ).toBeDefined( );
            expect( subs.widget['FOO2:BAR:BAR'].Foo2.bar['19890'].instance.id ).toEqual( '19890' );
            expect( subs.widget['FOO2:BAR:BAR'].Foo2.bar['19890'].fn ).toBeDefined( );
            expect( typeof subs.widget['FOO2:BAR:BAR'].Foo2.bar['19890'].fn ).toEqual( 'function' );

        } );

        it( 'Subscribe from sandbox to kit events', function ()
        {

            var subs;
            ifz.self.subUnsub(  sandbox, 'subscribe' );
            subs = events.get( 'subscribers' );

            expect( subs.kit['POO2:BAR'].Poo2.subscribers['19891'] ).toBeDefined( );
            expect( subs.kit['POO2:BAR'].Poo2.subscribers['19891'].instance ).toBeDefined( );
            expect( subs.kit['POO2:BAR'].Poo2.subscribers['19891'].fn ).toBeDefined( );

            expect( subs.kit['POO2:BAR2'].Poo2.subscribers ).toBeDefined( );
            expect( subs.kit['POO2:BAR2'].Poo2.car['19891'] ).toBeDefined( );
            expect( subs.kit['POO2:BAR2'].Poo2.car['19891'].instance ).toBeDefined( );
            expect( subs.kit['POO2:BAR2'].Poo2.car['19891'].instance.id ).toEqual( '19891' );
            expect( subs.kit['POO2:BAR2'].Poo2.car['19891'].fn ).toBeDefined( );
            expect( typeof subs.kit['POO2:BAR2'].Poo2.car['19891'].fn ).toEqual( 'function' );

            expect( subs.kit['FOO2:BAR:BAR'].Poo2.subscribers ).toBeDefined( );
            expect( subs.kit['FOO2:BAR:BAR'].Poo2.car['19891'] ).toBeDefined( );
            expect( subs.kit['FOO2:BAR:BAR'].Poo2.car['19891'].instance ).toBeDefined( );
            expect( subs.kit['FOO2:BAR:BAR'].Poo2.car['19891'].instance.id ).toEqual( '19891' );
            expect( subs.kit['FOO2:BAR:BAR'].Poo2.car['19891'].fn ).toBeDefined( );

            expect( subs.kit['FOO2:BAR:BAR'].Foo2.subscribers ).toBeDefined( );
            expect( subs.kit['FOO2:BAR:BAR'].Foo2.bar['19891'] ).toBeDefined( );
            expect( subs.kit['FOO2:BAR:BAR'].Foo2.bar['19891'].instance ).toBeDefined( );
            expect( subs.kit['FOO2:BAR:BAR'].Foo2.bar['19891'].instance.id ).toEqual( '19891' );
            expect( subs.kit['FOO2:BAR:BAR'].Foo2.bar['19891'].fn ).toBeDefined( );
            expect( typeof subs.kit['FOO2:BAR:BAR'].Foo2.bar['19891'].fn ).toEqual( 'function' );
        } );

        it( 'Subscribe from sandbox, kit, plugin, widget to service events', function ()
        {
            var subs;

            ifz.self.subUnsub( widget, 'subscribe' );
            subs = events.get( 'subscribers' );

            expect( subs.service['FOO:BAR'].Model.subscribers['19888'] ).toBeDefined( );
            expect( subs.service['FOO:BAR'].Model.subscribers['19888'].instance ).toBeDefined( );
            expect( subs.service['FOO:BAR'].Model.subscribers['19888'].fn ).toBeDefined( );

            expect( subs.service['FOO:BAR2'].Model.subscribers ).toBeDefined( );
            expect( subs.service['FOO:BAR2'].Model.users['19888'] ).toBeDefined( );
            expect( subs.service['FOO:BAR2'].Model.users['19888'].instance ).toBeDefined( );
            expect( subs.service['FOO:BAR2'].Model.users['19888'].instance.id ).toEqual( '19888' );
            expect( subs.service['FOO:BAR2'].Model.users['19888'].fn ).toBeDefined( );
            expect( typeof subs.service['FOO:BAR2'].Model.users['19888'].fn ).toEqual( 'function' );

            expect( subs.service['POST:CALLBACK'].Model.subscribers ).toBeDefined( );
            expect( subs.service['POST:CALLBACK'].Model.users['19888'] ).toBeDefined( );
            expect( subs.service['POST:CALLBACK'].Model.users['19888'].instance ).toBeDefined( );
            expect( subs.service['POST:CALLBACK'].Model.users['19888'].instance.id ).toEqual( '19888' );
            expect( subs.service['POST:CALLBACK'].Model.users['19888'].fn ).toBeDefined( );

            expect( subs.service['POST:CALLBACK'].Ajax.subscribers ).toBeDefined( );
            expect( subs.service['POST:CALLBACK'].Ajax.upload['19888'] ).toBeDefined( );
            expect( subs.service['POST:CALLBACK'].Ajax.upload['19888'].instance ).toBeDefined( );
            expect( subs.service['POST:CALLBACK'].Ajax.upload['19888'].instance.id ).toEqual( '19888' );
            expect( subs.service['POST:CALLBACK'].Ajax.upload['19888'].fn ).toBeDefined( );
            expect( typeof subs.service['POST:CALLBACK'].Ajax.upload['19888'].fn ).toEqual( 'function' );
        } );

    } );
    describe( 'PUBLISH', function instantiation ()
    {

        it( 'Publish event from widget', function ()
        {

            ifz.publish( widget, 'POO:BAR', { result : 'hola' } );
            expect( kit.result ).toEqual( 'OK' );
            expect( plugin.result ).toEqual( 'OK' );

        } );

        it( 'Publish event from plugin', function ()
        {

            ifz.publish( plugin, 'POO2:BAR', { result : 'hola' } );
            expect( kit.result2 ).toEqual( 'OK' );

        } );

        it( 'Publish event from kit', function ()
        {

            ifz.publish( kit, 'FOO:EVENT', { result : 'hola' } );
            expect( sandbox.result ).toEqual( 'OK' );

        } );

        it( 'Publish event from service', function ()
        {
            ifz.publish( service, 'FOO:BAR'  );
            expect( widget.result ).toEqual( 'OK' );

        } );

        it( 'Passing options', function ()
        {
            expect( sandbox.options.result ).toEqual( 'hola' );
        } );

        it( 'Publish event with alias', function ()
        {
            ifz.publish( widgetWithAlias, 'POO2:BAR2' );
            expect( kit.withAlias ).toEqual( 'OK' );
        } );

    } );
    describe( 'PUBLISHALL', function instantiation ()
    {
        it( 'Trigger events without alias', function ()
        {
            ifz.publish( widgetWithAlias, 'POO2:BAR', { result: 'whithout'} );
            expect( kit.withAlias.result ).toEqual( 'whithout' );
        } );

    } );
    describe( 'UNSUBSCRIBE', function instantiation ()
    {

        it( 'Unsubscribe plugin from widget events', function ()
        {
            var subs;

            ifz.self.subUnsub( plugin, 'unsubscribe' );

            subs = events.get( 'subscribers' );

            expect( subs.widget['POO:BAR'].Poo.subscribers['19889'] ).toBeUndefined( );

            expect( subs.widget['POO:BAR2'].Poo.subscribers ).toBeDefined( );
            expect( subs.widget['POO:BAR2'].Poo.car['19889'] ).toBeUndefined( );

            expect( subs.widget['FOO:BAR:BAR'].Poo.subscribers ).toBeDefined( );
            expect( subs.widget['FOO:BAR:BAR'].Poo.car['19889'] ).toBeUndefined( );

            expect( subs.widget['FOO:BAR:BAR'].Foo.subscribers ).toBeDefined( );
            expect( subs.widget['FOO:BAR:BAR'].Foo.bar['19889'] ).toBeUndefined( );


        } );


        it( 'Unsubscribe kit from widget and plugin events', function ()
        {

            var subs;

            ifz.self.subUnsub( kit, 'unsubscribe' );

            subs = events.get( 'subscribers' );

            expect( subs.widget['POO2:BAR'].Poo2.subscribers['19890'] ).toBeUndefined( );

            expect( subs.widget['POO2:BAR2'].Poo2.subscribers ).toBeDefined( );
            expect( subs.widget['POO2:BAR2'].Poo2.car['19890'] ).toBeUndefined( );

            expect( subs.widget['FOO2:BAR:BAR'].Poo2.subscribers ).toBeDefined( );
            expect( subs.widget['FOO2:BAR:BAR'].Poo2.car['19890'] ).toBeUndefined( );

            expect( subs.widget['FOO2:BAR:BAR'].Foo2.subscribers ).toBeDefined( );
            expect( subs.widget['FOO2:BAR:BAR'].Foo2.bar['19890'] ).toBeUndefined( );

            expect( subs.plugin['POO2:BAR'].Poo2.subscribers['19890'] ).toBeUndefined( );

            expect( subs.plugin['POO2:BAR2'].Poo2.subscribers ).toBeDefined( );
            expect( subs.plugin['POO2:BAR2'].Poo2.car['19890'] ).toBeUndefined( );

            expect( subs.plugin['FOO2:BAR:BAR'].Poo2.subscribers ).toBeDefined( );
            expect( subs.plugin['FOO2:BAR:BAR'].Poo2.car['19890'] ).toBeUndefined( );

            expect( subs.plugin['FOO2:BAR:BAR'].Foo2.subscribers ).toBeDefined( );
            expect( subs.plugin['FOO2:BAR:BAR'].Foo2.bar['19890'] ).toBeUndefined( );




        } );

        it( 'Unsubscribe sandbox from kit events', function ()
        {
            var subs;

            ifz.self.subUnsub( sandbox, 'unsubscribe' );

            subs = events.get( 'subscribers' );

            expect( subs.kit['POO2:BAR'].Poo2.subscribers['19891'] ).toBeUndefined( );

            expect( subs.kit['POO2:BAR2'].Poo2.subscribers ).toBeDefined( );
            expect( subs.kit['POO2:BAR2'].Poo2.car['19891'] ).toBeUndefined( );

            expect( subs.kit['FOO2:BAR:BAR'].Poo2.subscribers ).toBeDefined( );
            expect( subs.kit['FOO2:BAR:BAR'].Poo2.car['19891'] ).toBeUndefined( );

            expect( subs.kit['FOO2:BAR:BAR'].Foo2.subscribers ).toBeDefined( );
            expect( subs.kit['FOO2:BAR:BAR'].Foo2.bar['19891'] ).toBeUndefined( );


        } );

        it( 'Unsubscribe sandbox, kit, plugin, widget from service events', function ()
        {
            var subs;

            ifz.self.subUnsub( widget, 'unsubscribe' );

            subs = events.get( 'subscribers' );

            expect( subs.service['FOO:BAR'].Model.subscribers['19888'] ).toBeUndefined( );

            expect( subs.service['FOO:BAR2'].Model.subscribers ).toBeDefined( );
            expect( subs.service['FOO:BAR2'].Model.users['19888'] ).toBeUndefined( );

            expect( subs.service['POST:CALLBACK'].Model.subscribers ).toBeDefined( );
            expect( subs.service['POST:CALLBACK'].Model.users['19888'] ).toBeUndefined( );

            expect( subs.service['POST:CALLBACK'].Ajax.subscribers ).toBeDefined( );
            expect( subs.service['POST:CALLBACK'].Ajax.upload['19888'] ).toBeUndefined( );
        } );

    } );

    var items = [
            {
                title   : 'hola',
                value   : 'hola'
            },
            {
                title   : 'adios',
                value   : 'adios'
            }
        ],
        pluginAux = cream.plugin(
            'Test.Plugin.Test1',
            {

            },
            {

            },
            {

            },
            {


            },
            {

            },
            {

            }
        ),
        sbAux = cream.widget(
            'Test.Widget.Test2',
            {

            },
            {

            },
            {

            },
            {


            },
            {

            }
        );

    //ar wig = new sb( 'body',{ items :  items }, 'dummy' );

    var p = new pluginAux( 'body', { items :  items }, 'dummyPlugin' );
    ifz.add ( 'test2', 'body', {}, 'widget', 'test' );
    ifz.add ( 'test2', 'body', { parent: p }, 'widget', 'test2' );
    ifz.add ( 'test2', '.foo5', { parent: p }, 'widget', 'test4' );
    ifz.add ( 'test1', 'body',{}, 'plugin', 'test3' );

    var aux     = System.Cream.Container.attr.widget.Test2.test,
        aux2    = System.Cream.Container.attr.plugin.Test1.test3,
        aux3    = System.Cream.Container.attr.widget.Test2.test2,
        aux5    = System.Cream.Container.attr.widget.Test2.test4,
        aux4    = p.attr.widget.Test2.test4;

    describe( 'ADD', function instantiation ()
    {


        it( 'Interface elements will be created', function ()
        {
            expect( aux ).toBeDefined( );
            expect( aux2 ).toBeDefined( );

        } );
        it( 'Parent create the element, and selector is not inside parent, do not initialize', function ()
        {
            expect( aux3 ).toBeUndefined( );
        } );
        it( 'If parent create the element, a copy of widget resides inside it', function ()
        {
            expect( aux5 ).toBeDefined( );
            expect( aux5.id ).toEqual( aux4.id );
        } );

    } );

    describe( 'FIND', function instantiation ()
    {
        var found = ifz.find( p.attr.widget , 'test2', 'test4' ),
            found2 = ifz.find( p.attr.widget , 'test2' );

        it( 'Find the interface copy that resides inside parent', function ()
        {
            expect( found ).toBeDefined( );
            expect( found.id ).toEqual( aux5.id );
            expect( found2.test4.id ).toEqual( aux5.id );

        } );

    } );

    describe( 'FINANDREMOVE', function instantiation ()
    {
            ifz.add ( 'test2', '.foo5', { parent: p }, 'widget', 'test5' );

            var found = ifz.findAndRemove( p.attr.widget , 'test2', 'test5' );


            it( 'Find the interface copy that resides inside parent and remove it, globally', function ()
            {
                expect( System.Cream.Container.attr.widget.Test2.test5 ).toBeUndefined();
                expect( p.attr.widget.Test2.test5 ).toBeUndefined();

            } );

    } );

    describe( 'INIT INTERFACES WITH LOADED DATA', function instantiation ()
    {


    } );

    describe( 'INIT COLLECTION WITH LOADED DATA', function instantiation ()
    {


    } );
}
);
