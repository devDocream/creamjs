define( [
        'service/View/ViewService',
        'vendor/creamjs/spec/Service/View/dummy.twig!'
],
function viewSpec (v, view )
{

    describe( 'View Service', function instantiation ()
    {
        it( 'Extend filters', function ()
        {
            expect( Twig.functions.bar( { msg: 'FooBar'} ) ).toEqual( 'FooBar' );
        } );

        it( 'Extend helpers', function ()
        {
            expect( Twig.filters.foo( { msg: 'BarFoo'} ) ).toEqual( 'BarFoo' );
        } );

        it( 'Render precompiled', function ()
        {
            expect( cream.get( 'view' ).renderCompiled( view, { msg: 'FooBar'} ) )
                .toEqual( '<div class="auxiliar"><div class="bar">FooBar</div></div>' );
        } );

        it( 'Get compiled view by name', function ()
        {
            var viewCompiled = cream.get( 'view' )
                .getCompiledView( 'vendor/creamjs/spec/Service/View/dummy.twig' );
            expect( cream.get( 'view' ).renderCompiled( viewCompiled, { msg: 'BarFoo'} ) )
                .toEqual( '<div class="auxiliar"><div class="bar">BarFoo</div></div>' );
        } );

        it( 'Render by file name', function ()
        {
            expect( cream.get( 'view' ).render( 'vendor/creamjs/spec/Service/View/dummy.twig', { msg: 'Foooo'} ) )
                .toEqual( '<div class="auxiliar"><div class="bar">Foooo</div></div>' );
        } );

    } );
} );
