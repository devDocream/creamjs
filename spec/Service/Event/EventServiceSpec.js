define( [
        'service/Event/EventService'
],
function eventServiceSpec ( event )
{

    event = cream.get( 'event' );

    describe( 'BIND', function instantiation ()
    {


        it( 'Bind event to selector and with multiple levels', function ()
        {
            event.bind(
                '.foo click',
                function goClick(){ window.eventService =  'hola foo'; },
                { attr: { foo: 'bar'}, element: $( 'body' ) }
            );

            $( '.foo' ).click();

            expect( window.eventService ).toEqual( 'hola foo' );

            event.bind(
                '.foo2 .bar input change',
                function goClick(){ window.eventService2 =  'hola foobar'; },
                { attr: { foo: 'bar'}, element: $( 'body' ) }
            );

            $( '.foo2 .bar input' ).click();

            expect( window.eventService2 ).toEqual( 'hola foobar' );

        } );

        it( 'Bind event to selector only inside instance.element', function ()
        {
            cont = 0;

            event.bind(
                '.bar input change',
                function goClick(){ cont = cont + 1; window.eventService3 =  cont; },
                { attr: { foo: 'bar'}, element: $( '.foo2' ) }
            );

            $( '.bar input' ).click();

            expect( window.eventService3 ).toEqual( 1 );

        } );


        it( 'Bind event to instance.element directly', function ()
        {
            event.bind(
                'click',
                function(){ console.log('lanza');window.eventService4 =  4; },
                { attr: { foo: 'bar'}, element: $( '.foo4' ) }
            );

            $( '.foo4' ).click();

            expect( window.eventService4 ).toEqual( 4 );

        } );

        it( 'Bind event to variable in attr', function ()
        {
            event.bind(
                '{foo} click',
                function goClick(){ window.eventService5 =  5; },
                { attr: { foo: $('.foo5')}, element: $( 'body' ) }
            );

            $( '.foo5' ).click();

            expect( window.eventService5 ).toEqual( 5 );

        } );

        it( 'Bind event to variable in window', function ()
        {
            window.foo = $('.foo6');
            event.bind(
                '{foo} click',
                function goClick(){ window.eventService6 =  6; },
                { attr: { foo7: $('.foo6')}, element: $( 'body' ) }
            );

            $( '.foo6' ).click();

            expect( window.eventService6 ).toEqual( 6 );

        } );

        it( 'Autobind event to new equal elements', function ()
        {
            cont2 = 0;

            event.bind(
                '.foo7 click',
                function goClick(){ cont2 = cont2 + 1; window.eventService7 =  cont2; },
                { attr: { foo: 'bar'}, element: $( 'body' ) }
            );

            $( '.foo7' ).click();

            $("body" ).append( $( '<div/>' ).addClass( 'foo7' ) );

            $( '.foo7' ).click();

            expect( window.eventService7 ).toEqual( 3 );

        } );

        it( 'Bind event to variable with multiple levels', function ()
        {

            event.bind(
                '{foo7.foo9} click',
                function goClick(){ window.eventService9 =  9; },
                { attr: { foo7: {  foo9: $('.foo9') } }, element: $( 'body' ) }
            );

            $( '.foo9' ).click();

            expect( window.eventService9 ).toEqual( 9 );

        } );


    } );

    describe( 'unbind', function instantiation ()
    {

        it( 'Bind event to selector and with multiple levels', function ()
        {
            window.eventService = 11;

            event.unbind(
                '.foo click',
                function goClick(){ window.eventService =  'hola foo'; },
                { attr: { foo: 'bar'}, element: $( 'body' ) }
            );

            $( '.foo' ).click();

            expect( window.eventService ).toEqual( 11 );

            window.eventService2 = 12;
            event.unbind(
                '.foo2 .bar input change',
                function goClick(){ window.eventService2 =  'hola foobar'; },
                { attr: { foo: 'bar'}, element: $( 'body' ) }
            );

            $( '.foo2 .bar input' ).click();

            expect( window.eventService2 ).toEqual( 12 );

        } );

        it( 'Bind event to selector only inside instance.element', function ()
        {
            cont = 0;
            window.eventService3 = 11;
            event.unbind(
                '.bar input change',
                function goClick(){ cont = cont + 1; window.eventService3 =  cont; },
                { attr: { foo: 'bar'}, element: $( '.foo2' ) }
            );

            $( '.bar input' ).click();

            expect( window.eventService3 ).toEqual( 11 );

        } );


        it( 'Bind event to instance.element directly', function ()
        {
            window.eventService4 = 44;
            event.unbind(
                'click',
                function(){ console.log('lanza');window.eventService4 =  4; },
                { attr: { foo: 'bar'}, element: $( '.foo4' ) }
            );
            $( '.foo4' ).click();

            expect( window.eventService4 ).toEqual( 44 );

        } );

        it( 'Bind event to variable in attr', function ()
        {
            window.eventService5 = 55;
            event.unbind(
                '{foo} click',
                function goClick(){ window.eventService5 =  5; },
                { attr: { foo: $('.foo5')}, element: $( 'body' ) }
            );

            $( '.foo5' ).click();

            expect( window.eventService5 ).toEqual( 55 );

        } );

        it( 'Bind event to variable in window', function ()
        {
            window.foo = $('.foo6');
            window.eventService6 = 7;
            event.unbind(
                '{foo} click',
                function goClick(){ window.eventService6 =  6; },
                { attr: { foo7: $('.foo6')}, element: $( 'body' ) }
            );

            $( '.foo6' ).click();

            expect( window.eventService6 ).toEqual( 7 );

        } );

        it( 'Autobind event to new equal elements', function ()
        {
            cont2 = 0;
            window.eventService7 = 7;
            event.unbind(
                '.foo7 click',
                function goClick(){ cont2 = cont2 + 1; window.eventService7 =  cont2; },
                { attr: { foo: 'bar'}, element: $( 'body' ) }
            );

            $( '.foo7' ).click();

            $("body" ).append( $( '<div/>' ).addClass( 'foo7' ) );

            $( '.foo7' ).click();

            expect( window.eventService7 ).toEqual( 7 );

        } );

        it( 'Bind event to variable with multiple levels', function ()
        {
            window.eventService9 = 99;
            event.unbind(
                '{foo7.foo9} click',
                function goClick(){ window.eventService9 =  9; },
                { attr: { foo7: {  foo9: $('.foo9') } }, element: $( 'body' ) }
            );

            $( '.foo9' ).click();

            expect( window.eventService9 ).toEqual( 99 );

        } );

    } );

}
);
