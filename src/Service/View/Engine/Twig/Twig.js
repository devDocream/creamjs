define( [
    'framework',
    'twig',
    'class'
],
function twigEngine ( cream, Twig, Class )
{

    var twigEng = Class(
        'Cream.Services.Twig',
        {},
        {},
        {
            _wrapper      : function _wrapper ( toWrap )
            {
                var fn = function wrapped ( )
                {
                    var parts = toWrap.split( '.' ),
                        srv   = parts.length === 1 ? false : true,
                        element;

                    if ( srv === true )
                    {
                        element = cream.get( parts[0] );
                        if ( element !== undefined )
                        {
                            if ( typeof element[ parts[1] ] === 'function'  )
                            {
                                return element[ parts[1] ].apply( element, arguments );
                            }
                        }
                    }
                    else if ( typeof window.cream[ parts[0] ] === 'function'  )
                    {
                        return window.cream[ parts[ 0 ] ].apply( window.cream, arguments );
                    }

                    return '';
                };

                return fn;

            },

            extendHelpers : function functions( fns )
            {
                var keys    = cream.keys( fns ),
                    length  = cream.keysLength( fns ),
                    i;

                for ( i = 0; i < length; i++ )
                {
                    Twig.extendFunction( keys[ i ], this._wrapper( fns[ keys[ i ] ] ) );
                }

            },

            extendFilters   : function filters ( filters )
            {
                var keys    = cream.keys( filters ),
                    length  = cream.keysLength( filters ),
                    i;

                for ( i = 0; i < length; i++ )
                {
                    Twig.extendFilter( keys[ i ], this._wrapper( filters[ keys[ i ] ] ) );
                }
            },

            render          : function render ( template, opts )
            {
                var key     = template,
                    tmp     = this.getCompiledView( key );

                if ( cream.notUndefined( tmp ) )
                {
                    return tmp
                        .render( opts );
                }

                if ( cream.notUndefined( this.getView( key ) ) )
                {
                    return Twig.twig( { ref : key } )
                        .render( opts );
                }

                Twig.twig(
                    {
                        id       : key,
                        href     : key,
                        async    : false
                    } );

                this.addView( key, key );

                return  Twig.twig( { ref : key } )
                            .render( opts );
            },

            renderCompiled      : function renderCompiled( view, opts )
            {
                if ( cream.notUndefined( view ) )
                {
                    return view
                        .render( opts );
                }
            }
        } );

    return new twigEng();
} );
