define( [
    'framework',
    'service/View/Engine/cream.engine!'
],
function viewServ ( cream, engine )
{
    var View;

    /**
     * @class       Cream.Services.View
     * @classdesc   View service
     *
     * @author      Fernando Coca fernando@docream.com
    */
    View = cream.service(
        'Cream.Services.View',
        {
            views       : [],
            helpers     : [],
            functions   : [],
            filters     : []
        },
        {

        },
        {
            init            : function init ( )
            {
                var services    = System.Cream.services,
                    viewService = services !== undefined ? System.Cream.services.View : '',
                    helpers, filters;

                if ( viewService !== '' )
                {
                    helpers = viewService.helpers;
                    filters = viewService.filters;

                    if ( cream.notUndefined( helpers ) )
                    {
                        if ( cream.notUndefined( helpers ) &&
                             cream.isFunction( this.extendHelpers ) )
                        {
                            this.attr.functions = helpers;
                            this.extendHelpers( helpers );
                        }


                    }
                    if ( cream.notUndefined( filters ) &&
                          cream.isFunction( this.extendFilters ) )
                    {
                        this.attr.filters = filters;
                        this.extendFilters( filters );
                    }
                }


            },

            addView         : function addView ( name, value  )
            {
                if ( cream.hasValue( name ) && cream.notUndefined( value ) )
                {
                    this.attr.views[ name ] = value;

                    return value;
                }
            },

            getView         : function getView ( name )
            {
                return this.get( 'views' )[ name ];
            },

            /**
             * @method                              Cream.Services.View.getCompiledView
             * @description                         Returns a View object, it search by template name,
             *                                      should be preloaded with steal.
             *
             * @param   {string} view               Template name
             *
             * @returns {(View|undefined)}
             *
            */
            getCompiledView : function compiled ( view )
            {
                var f       = '/',
                    f       = f.replace( /[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&" ),
                    name    = 'template:'
                               + view.replace( System.baseURL, '' )
                                .replace( new RegExp( f , 'g' ), '_' )
                                .replace( '.twig', '' )
                                .toLowerCase();

                if ( cream.notUndefined( name ) && cream.notUndefined( System.Cream.templates[ name ] ) )
                {
                    return  System.Cream.templates[ name ];
                }

                return undefined;
            },

            /**
             * @method                              Cream.Services.View.render
             * @description                         Render a template by name and with options.
             *                                      The template should be loaded before with steal.
             *
             * @param   {string}    template        Template name
             * @param   {object}    opts            Template parameters
             *
             * @returns {string}
             *
            */
            render          : engine.render,

            /**
             * @method                              Cream.Services.View.renderCompiled
             * @description                         Render a preloaded template object with options.
             *                                      The view object is returned by steal when we load
             *                                      the template or we can get this object
             *                                      with getCompiledView function.
             *
             * @param   {View}      view            View object
             * @param   {object}    opts            View parameters
             *
             * @returns {string}
             *
             */
            renderCompiled  : engine.renderCompiled,
            extendFilters   : engine.extendFilters,
            extendHelpers   : engine.extendHelpers,
            _wrapper        : engine._wrapper
        }

    );

    return View;
}
);
