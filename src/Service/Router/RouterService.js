define( [
    'framework',
    'adapter/Router/RouterSF'
], function RouterService ( cream )
{
    /**
     * @class               Cream.Services.Router
     * @parent              Cream.Services
     * @augments            Cream.Service
     * @classdesc           Router service
     *
     * @author              Fernando Coca <fernando@docream.com>
     *
    */
    return cream.service(

        'Cream.Services.Router',

        {
        },
        {

        },
        {
            init        : function init ( )
            {
                var services       = System.Cream.services,
                    routerService  = services !== undefined ? System.Cream.services.Router : '',
                    prop           = routerService !== '' ? routerService.property : '',
                    data           = System.Cream.data !== undefined && prop !== '' &&
                                     System.Cream.data[ prop ] !== undefined
                        ? System.Cream.data[ prop ]
                        : {};


                this.fromJSON( data );

            },

            fromJSON    : window.fos.Router.setData,

            /**
             * @method                                       Cream.Services.Router.generate
             * @description                                  Generate URL from route name
             *
             * @param   {string}           route_name        Route name
             * @param   {object}           params            Object with route options
             *
             * @returns {string}
             *
            */
            generate    : window.Routing.generate

        } );

} );
