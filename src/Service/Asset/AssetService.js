define( [
    'framework'
], function AssetService ( cream )
{
    /**
     * @class               Cream.Services.Asset
     * @parent              Cream.Services
     * @augments            Cream.Service
     * @classdesc           Asset service
     *
     * @author              Fernando Coca <fernando@docream.com>
     *
    */
    return cream.service(

        'Cream.Services.Asset',

        {
            base : ''
        },
        {

        },
        {
            init    : function init ( )
            {

                var services        = System.Cream.services,
                    assetService    = services !== undefined ? System.Cream.services.Asset : '',
                    path            = assetService !== '' ? assetService.base : 'auto',
                    exists          = path !== undefined && path !== '' && path !== 'auto';

                this.set( 'base', exists ? path : System.baseURL );

            },


            /**
             * @method                                       Cream.Services.Asset.asset
             * @description                                  Generate absolute path from src param
             *
             * @param   {string}           src               Path to asset file
             *
             * @returns {string}
             *
            */
            asset   : function asset ( src )
            {
                return this.get( 'base' ) + src  ;
            }
        } );

} );
