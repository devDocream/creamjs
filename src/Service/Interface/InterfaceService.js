define( [
    'framework',
    'service/View/ViewService',
    'service/Event/EventService'
],
function interfaceService ()
{

    /**
     * @class       Cream.Services.Interface
     * @classdesc   Interface Service. Manage life cycle of interface components (widgets, plugins,)
     *
     * @author      Fernando Coca fernando@docream.com
    */
    cream.service(
        'Cream.Services.Interface',
        {
            sandbox     : {},
            widget      : {},
            kit         : {},
            plugin      : {},
            collections : {}
        },
        {

            beforeAfter     : function beforeAfter ( selected, selector, type, tmp )
            {
                var t = selected.find( selector );

                if ( t.length > 0 && cream.notUndefined( selector )
                     && cream.notBlank( selector ) )
                {
                    if ( type === 'after' )
                    {
                        return tmp.insertAfter( t );
                    }

                    return tmp.insertBefore( t );
                }

                return tmp.appendTo( t );
            },

            locate          : function locate ( rendered, selector, place )
            {
                var selected = cream.$( selector ),
                    compiled = cream.$( rendered ),
                    element;

                switch ( place.type )
                {
                    case 'prepend' :

                        element = compiled.prependTo( selected );

                        break;

                    case 'before' :

                        element = this.beforeAfter( selected, place.value, 'before', compiled );

                        break;

                    case 'after' :

                        element = this.beforeAfter( selected, place.value, 'after', compiled );

                        break;

                    default :

                        element = compiled.appendTo( selected );

                        break;
                }

                return element;
            },

            render          : function render ( view, options )
            {
                var rendered;

                if ( cream.notNull( view ) && typeof view.render === 'function' )
                {
                    rendered = cream.get( 'view' )
                        .renderCompiled( view, options );
                    return rendered;
                }

                return '';
            },

            bindUnbind      : function bindUnbind ( instance, type )
            {
                var events      = cream.get( 'event' ),
                    controls    = instance.controls,
                    keys        = cream.keys( controls ),
                    length      = cream.keysLength( controls ),
                    i, key;

                if ( cream.notUndefined( controls ) )
                {

                    for ( i = 0; i < length; i++ )
                    {
                        key = keys [ i ];
                        if ( key !== 'name' )
                        {
                            if ( type === 'bind' )
                            {
                                events.bind( key, controls[ key ], instance );
                            }
                            else
                            {
                                events.unbind( key, controls[ key ], instance );
                            }

                        }
                    }
                }
            },

            addClasses      : function addClasses ( instance )
            {
                var classNameAlias, classNameType;

                instance.element
                    .addClass( instance._cls )
                    .addClass( instance._type );

                if ( cream.notBlank( instance.alias ) )
                {
                    classNameAlias = instance._cls + '-' + instance.alias;
                    classNameType  = instance._type + '-' + instance.alias;

                    instance.element
                        .addClass( classNameAlias )
                        .addClass( classNameType );
                }

                return instance;
            },

            removeClasses   : function removeClasses ( instance )
            {
                var classNameAlias, classNameType;

                instance.element
                    .removeClass( instance._cls )
                    .removeClass( instance._type );

                if ( cream.notBlank( instance.alias ) )
                {
                    classNameAlias = instance._cls + '-' + instance.alias;
                    classNameType  = instance._type + '-' + instance.alias;

                    instance.element
                        .removeClass( classNameAlias )
                        .removeClass( classNameType );
                }

                return instance;
            },

            generateId      : function generateId ( instance )
            {
                var date = new Date();

                instance.id = date.getTime();

                return instance;
            },

            cacheElements   : function cacheElements ( instance )
            {
                var selectors   = instance.get( 'selectors' ) ,
                    keys        = cream.keys( selectors ),
                    length      = cream.keysLength( selectors ),
                    i, key;

                if ( length > 0 )
                {
                    instance.set( 'elements', {} );

                    for ( i = 0; i < length; i++ )
                    {
                        key = keys[i];

                        instance.attr.elements[ key ]  = instance.element.find( selectors[ key ] );
                    }
                }
                return instance;
            },

            _subUnsub       : function _subUnsub ( subscriptions, instance, type, action )
            {
                var events      = cream.get( 'event' ),
                    keys        = cream.keys( subscriptions ),
                    length      = cream.keysLength( subscriptions ),
                    i, key, keys2, key2, length2, j;

                for ( i = 0; i < length; i++ )
                {
                    key = keys [ i ];
                    if ( key !== 'name' )
                    {
                        keys2       = cream.keys( subscriptions[ key ] );
                        length2     = cream.keysLength( subscriptions[ key ] );

                        for ( j = 0; j < length2; j++ )
                        {
                            key2 = keys2 [ j ];
                            if ( action === 'subscribe' )
                            {
                                events.subscribe( key, key2, subscriptions[ key ][ key2 ], instance, type );
                            }
                            else
                            if ( action === 'unsubscribe')
                            {
                                events.unsubscribe( key, key2, instance, type );
                            }

                        }
                    }
                }
            },

            subUnsub        : function subeUnsub  ( instance, action )
            {
                this._subUnsub( instance.services, instance, 'service', action );
                this._subUnsub( instance.events, instance, 'interface', action );

            },

            subscribeCollections : function subscribeCollections( instance )
            {
                var collections = instance.collections,
                    toRefresh   = instance.attr.refresh,
                    _this       = this,
                    result      = {},
                    keys, length, key, i, j,
                    keys2, key2, length2, aux, length3, k, fnCollection;

                if ( cream.notUndefined( collections ) && cream.notUndefined( toRefresh ) )
                {
                    keys = cream.keys( toRefresh );
                    length = cream.keysLength( toRefresh );

                    for ( i = 0; i < length; i++ )
                    {
                        key = keys [i];

                        if ( key !== 'name' )
                        {
                            if ( cream.isUndefined( result[key] ) )
                            {
                                result[key] = {};
                            }

                            keys2 = cream.keys( toRefresh[key] );
                            length2 = cream.keysLength( toRefresh[key] );

                            for ( j = 0; j < length2; j++ )
                            {
                                key2    = keys2 [j];
                                aux     = toRefresh[ key ][ key2 ];
                                aux     = cream.isArray( aux ) ? aux : [ aux ];
                                length3 = aux.length;

                                if ( cream.isUndefined( result[key][key2] ) )
                                {
                                    result[key][ key2 ] = {};
                                }

                                for ( k = 0; k < length3; k++ )
                                {
                                   if (  cream.isFunction( collections[ aux[ k ] ] ) )
                                   {
                                       if ( cream.isUndefined( instance.events[ key ] ) )
                                       {
                                           instance.events[ key ] = {};
                                       }
                                       if ( cream.isUndefined( instance.events[ key ][ key2 ] ) )
                                       {
                                           instance.events[ key ][ key2 ] = function(){};
                                       }

                                       fnCollection =  collections[ aux[ k ] ];
                                       result[key][key2][ aux[ k ] ] =
                                       {
                                            fn : fnCollection,
                                            instance: instance,
                                            collection : aux[ k ]
                                       };
                                   }

                                }

                            }
                        }
                    }
                }

                return result;
            },

            cacheCollections : function cacheCollection ( instance )
            {
                var collections = instance.collections,
                    datas       = System.Cream.data,
                    dd          = System.Cream.DD,
                    keys, length, key, i;

                dd = dd !== undefined && dd.collection ? dd.collection : {};
                datas = datas !== undefined ? datas : {};

                if ( cream.notUndefined( collections ) )
                {
                    keys        = cream.keys( collections );
                    length      = cream.keysLength( collections );

                    for ( i = 0; i < length; i++ )
                    {
                       key = keys[ i ];
                       if ( key !== 'name' )
                       {

                           if ( dd !== undefined && dd[ key ] !== undefined && datas !== undefined &&
                               datas[ dd[ key ] ] !== undefined )
                           {
                               this.attr.collections[ key ] = datas[ dd[ key ] ];
                           }
                           else if( cream.isFunction( collections[ key ] ) )
                           {
                               this.attr.collections[ key ] = collections[ key ].apply( instance, [] );
                           }
                       }

                    }
                }
            },

            initCollections : function initCollections ( instance )
            {
                var result = this.subscribeCollections( instance );
                this.cacheCollections( instance );

                instance.collectionSubscribers = result;

                return instance;
            },

            dataDependencies : function dataDependencies ( nam, typ, alia )
            {
                var dd      = System.Cream.DD,
                    type    = dd !== undefined ? dd[ typ ] : '',
                    name    = type !== undefined && type !== '' ? type[ nam ] : '',
                    alias   = name !== undefined && name !== '' && alia !== undefined && alia !== ''
                                ? name[ alia ]
                                : '',
                    datas   = System.Cream.data !== undefined ? System.Cream.data : {},
                    deps, key, keys, length, i, result;

                result = {};

                if ( name !== '' )
                {
                    deps = name.deps;
                    keys = cream.keys( deps );
                    length = cream.keysLength( deps );

                    for ( i = 0; i < length; i++ )
                    {
                        key = keys [ i ];
                        result[ key ] = datas[ deps[ key ] ];
                    }
                }
                if ( alias !== '' )
                {
                    deps = alias;
                    keys = cream.keys( deps );
                    length = cream.keysLength( deps );

                    for ( i = 0; i < length; i++ )
                    {
                        key = keys [ i ];
                        result[ key ] = datas[ deps[ key ] ];
                    }
                }
                return result;
            }


        },
        {
            init            : function init ( )
            {

            },

            /**
             * @method                              Cream.Services.Interface.update
             * @description                         Reinitialize the interface instance with actual
             *                                      parameters
             *
             * @param   {(Cream.Widget|Cream.Plugin|Cream.Kit|Cream.Sandbox)} instance
             *
            */
            update          : function update ( instance )
            {
                var type = instance._type.replace( 'cream-', '' );

                this.remove( instance );
                this.create( instance, instance.selector, type );
                instance.init.apply( instance, [ instance.selector, instance.attr, instance.alias ] );
            },

            /**
             * @method                              Cream.Services.Interface.destroy
             * @description                         Destroy a interface instance:
             *                                      - Remove classes from dom element
             *                                      - Unbind events
             *                                      - Remove instance to cream container
             *
             * @param   {(Cream.Widget|Cream.Plugin|Cream.Kit|Cream.Sandbox)} instance
             * @param   {string}                type         Interface type: widget, plugin,..
             *
             * @returns
             *
            */
            destroy         : function destroy ( instance )
            {
                this.self.bindUnbind( instance, 'unbind' );
                this.self.subUnsub( instance, 'unsubscribe' );

                instance    = this.self.removeClasses( instance );

                cream.remove( instance );
            },

            /**
             * @method                              Cream.Services.Interface.remove
             * @description                         Destroy a interface instance and dom element:
             *                                      - Remove classes from dom element
             *                                      - Unbind events
             *                                      - Remove instance to cream container
             *                                      - Remove interface dom element from the DOM
             *
             * @param   {(Cream.Widget|Cream.Plugin|Cream.Kit|Cream.Sandbox)} instance
             *
            */
            remove          : function remove ( instance )
            {
                this.destroy( instance );

                instance.element.remove();
            },

            /**
             * @method                              Cream.Services.Interface.create
             * @description                         Initialize interface instance:
             *                                      - Add classes to dom element
             *                                      - Bind ui events
             *                                      - Subscribe classes events
             *                                      - Render and append a predefined view
             *                                      - Add instance to cream container
             *                                      - Pre-cache elements from selectors attr
             *
             * @param   {(Cream.Widget|Cream.Plugin|Cream.Kit|Cream.Sandbox)} instance
             * @param   {(string|$Object)}      selector     Container or element target to init
             *                                               the interface
             * @param   {string}                type         Interface type: widget, plugin,..
             *
             * @returns {(Cream.Widget|Cream.Plugin|Cream.Kit|Cream.Sandbox)}
             *
            */
            create          : function create( instance, selector, type )
            {
                var view            = instance.get( 'view' ),
                    place           = instance.get( 'place' ),
                    element         = cream.$( selector ),
                    elementOver     = element,
                    rendered;

                if ( element.length === 0 )
                {
                    return instance;
                }

                instance.selector = selector;

                if ( cream.notNull( view ) && cream.notUndefined( view ) && view !== '' )
                {
                    rendered = this.self.render( view, instance.attr );
                    element  = this.self.locate( rendered, selector, place );
                }

                if ( cream.isUndefined( instance.id ) )
                {
                    instance         = this.self.generateId( instance );
                }

                instance.element = cream.notNull( view ) && place.type === 'over' && view !== ''
                                    ? elementOver
                                    : element;

                instance         = this.self.addClasses( instance );
                instance         = this.self.cacheElements( instance );
                instance         = this.self.initCollections( instance );

                this.self.bindUnbind( instance, 'bind' );
                this.self.subUnsub( instance, 'subscribe' );

                cream.add( instance, type );

                return instance;
            },

            /**
             * @method                                  Cream.Services.Interface.publish
             * @description                             Trigger the event  only over the actual
             *                                          widget instance.
             *                                          If alias widget is defined, this function
             *                                              find and trigger 'Widget:alias' event
             *                                          Else find and trigger 'Widget' event
             *
             * @param {(Cream.Widget|Cream.Plugin|Cream.Kit|Cream.Sandbox)} instance   Publisher class
             * @param {string}              event         Event name
             * @param {object}              [opts]      Event options
             *
             *
             */
            publish         : function publish ( instance, event, opts )
            {
                var events      = cream.get( 'event' ),
                    type        = instance._type.replace( 'cream-', '' ),
                    name        = instance._shortName,
                    alias       = instance.alias,
                    nameAlias   = alias !== '' && cream.notUndefined( alias )
                                    ? name + ':' + alias
                                    : name;

                events.publish( nameAlias, event, opts, type );
            },

            /**
             * @method                                  Cream.Services.Interface.publishAll
             * @description                             Trigger the event over all instances, does
             *                                          not consider instance alias.
             *
             * @param {(Cream.Widget|Cream.Plugin|Cream.Kit|Cream.Sandbox)} instance Publisher class
             * @param {string}              event         Event name
             * @param {(object|undefined)}  [opts]      Event options
             *
             *
            */
            publishAll      : function publishAll ( instance, event, opts )
            {
                var events      = cream.get( 'event' ),
                    type        = instance._type.replace( 'cream-', '' ),
                    name        = instance._shortName;

                events.publish( name, event, opts, type );
            },

            /**
             * @method                                  Cream.Services.Interface.publishAll
             * @description                             Create Interface Classes instance by name.
             *                                          The classes has to be preloaded.
             *                                          (widgets, plugins, kits,..)
             *
             * @param {string}              name         Interface short name
             * @param {string}              selector     CSS style selector where init the interface
             * @param {object}              options      Interface options
             * @param {string}              type         Interface type
             * @param {string}              alias        Interface alias to identify it
             *
             *
             * @returns {(Cream.Widget|Cream.Plugin|Cream.Kit|Cream.Sandbox|object)}
            */
            add             : function add ( name, selector, options, type, alias )
            {
                var constructor = cream.get( name, type, 'classes' ),
                    selectorParent = selector,
                    instance, dd;


                if ( cream.notUndefined( options.parent ) )
                {
                    selectorParent = options.parent.element.find( selector );

                    if ( selectorParent.length === 0 )
                    {
                        return {};
                    }
                }

                dd      = this.self.dataDependencies( name, type, alias );
                options = cream.extend( cream.extend( true, {}, dd ), options );

                instance = new constructor( selectorParent, options, alias );

                return instance;
            },

            /**
             * @method                                  Cream.Services.Interface.find
             * @description                             Find Interface instance/s from object
             *
             * @param {object}              target      Object where to find
             * @param {string}              name        Interface name
             * @param {string}              alias       Interface alias
             *
             *
             * @returns {(Cream.Widget|Cream.Plugin|Cream.Kit|Cream.Sandbox|array)}
            */
            find            : function find ( target, name, alias )
            {
                var objects = target[ cream.capitalize( name ) ];

                return cream.notUndefined( alias ) && cream.notUndefined( objects )
                    ? objects[ alias ]
                    : objects;
            },

            /**
             * @method                                  Cream.Services.Interface.findAndRemove
             * @description                             Find Interface instance/s and remove it/them
             *
             * @param {object}              target      Object where to find
             * @param {string}              name        Interface name
             * @param {string}              alias       Interface alias
             *
            */
            findAndRemove   : function findAndRemove ( target, name, alias )
            {
                var objects     = this.find( target, name, alias ),
                    keys        = cream.isUndefined( objects.id )
                                    ? cream.keys( objects )
                                    : [],
                    length      = cream.keysLength( objects ),
                    i, key;

                if ( cream.notUndefined( objects.id ) )
                {
                    return this.remove( objects );
                }

                for ( i = 0; i < length; i++ )
                {
                    key = keys[ i ];

                    this.remove( objects[ key ] );
                }

                return true;
            },

            /**
             * @method                                  Cream.Services.Interface.collection
             * @description                             Returns cached result of a collection
             *
             *
             * @param {(Cream.Sandbox|Cream.Kit)}       instance
             * @param {string}          name            Collection name
             * @param {boolean}         [refresh=false] If true, force refresh collection before return
             * @param {object}          [options]       Object with options to pass collection function

             *
             * @returns {*}
             *
            */
            collection      : function collection ( instance, name, refresh, options )
            {
                if ( cream.isUndefined( refresh ) || cream.isFalse( refresh ) )
                {
                    return this.attr.collections[ name ];
                }

                return this.refreshCollection( instance, name, options );

            },

            /**
             * @method                                  Cream.Services.Interface.refreshCollection
             * @description                             Refresh a collection and returns the value
             *
             * @param {(Cream.Sandbox|Cream.Kit)}       instance
             * @param {string}          name            Collection name
             * @param {object}          [options]       Object with options to pass collection function

             *
             * @returns {*}
             *
             */
            refreshCollection : function refreshCollection( instance, name, options )
            {
                var fnCollection = instance.collections[ name ];

                if ( cream.isFunction( fnCollection ) )
                {
                    this.attr.collections[ name ] = fnCollection.apply( instance, [ options ] );
                }

                return this.attr.collections[ name ];
            }

        }
    );

} );
