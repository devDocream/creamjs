define( [
    'framework',
    'adapter/Translator/TranslatorSF'
], function TranslatorService ( cream )
{

    var trans = window.Translator( document );

    /**
     * @class               Cream.Services.Translator
     * @parent              Cream.Services
     * @augments            Cream.Service
     * @classdesc           Router service
     *
     * @author              Fernando Coca <fernando@docream.com>
     *
    */
    return cream.service(

        'Cream.Services.Translator',

        {
        },
        {

        },
        {
            init    : function init ( )
            {

                var services       = System.Cream.services,
                    transService   = services !== undefined ? System.Cream.services.Translator : '',
                    prop           = transService !== '' ? transService.property : '',
                    data           = System.Cream.data !== undefined && prop !== '' &&
                                        System.Cream.data[ prop ] !== undefined
                                     ? System.Cream.data[ prop ]
                                     : {};


                this.fromJSON( data );

            },

            fromJSON    : trans.fromJSON,

            /**
             * @method                                       Cream.Services.Translator.transChoice
             * @description                                  Translate a translation named string
             *                                               to the configured language by locale,
             *                                               or to the fallback language.
             *                                               For pluralized translations, indicating
             *                                               the number of elements.
             *
             * @param   {string}           id                Translation key
             * @param   {integer}          number            Choice number
             * @param   {object}           params            Object with translation options
             * @param   {string}           domain            Translations domain name
             * @param   {string}           [locale]          Locale
             *
             * @returns {string}
             *
             */
            transChoice : trans.transChoice,

            /**
             * @method                                       Cream.Services.Router.generate
             * @description                                  Translate a translation named string
             *                                               to the configured language by locale,
             *                                               or to the fallback language.
             *
             * @param   {string}           id                Translation key
             * @param   {object}           params            Object with translation options
             * @param   {string}           domain            Translations domain name
             * @param   {string}           [locale]          Locale
             *
             * @returns {string}
             *
            */
            trans       : trans.trans,

            reset       : trans.rest,

            add         : trans.add


        } );

} );
