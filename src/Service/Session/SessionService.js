define( [
    'framework'
], function SessionService( cream )
{


    /**
     * @class               Cream.Services.Session
     * @parent              Cream.Services
     * @augments            Cream.Service
     * @classdesc           Session service, use the window.sessionStorage to store data
     *
     * @author              Fernando Coca <fernando@docream.com>
     *
     */
    return cream.service(

        'Cream.Services.Session',
        {

            driver : 'session'
        },
        {
            _set        : function _set( key, value, storage )
            {
                var item;

                if ( value !== undefined && key !== undefined && key !== '' &&
                     storage !== undefined )
                {
                    item = typeof value === 'object'
                        ? JSON.stringify( value )
                        : value;
                    try
                    {
                        storage.setItem( key, item );
                    }
                    catch ( e )
                    {
                        return false;
                    }

                    return true;
                }

            },

            _get        : function _get ( key, storage )
            {
                var isJSON = false, item;

                if ( cream.hasValue( key ) && storage !== undefined )
                {
                    try
                    {
                        item = storage.getItem( key );
                    }
                    catch( e )
                    {
                        return undefined;
                    }

                    isJSON = cream.isJSON( item );

                    return isJSON
                        ? JSON.parse( item )
                        : item ;
                }

                return undefined;
            }
        },
        {
            init        : function init ( options )
            {
                var driver;

                if ( cream.hasValue( options.driver ) )
                {
                    this.set( 'driver', options.driver );
                }

                driver = this.get( 'driver' );

                this.set( 'driver',
                    driver === 'session'
                        ? window.sessionStorage
                        : window.localStorage
                );
            },

            /**
             * @method                                          Cream.Services.Session.save
             * @description                                     Save the key-value
             *
             * @param   {string}                        key     Key name
             * @param   {(array|object|string|number)}  value   Value
             *
             * @returns {string}
             *
             */
            save        : function save ( key, value )
            {
                this.self._set( key, value, this.get( 'driver' ) );
            },

            /**
             * @method                                       Cream.Services.Session.fetch
             * @description                                  Retrieve the value for key
             *
             * @param   {string}    key                      Key name
             *
             * @returns {(array|object|string|number)}
             *
             */
            fetch       : function fetch ( key )
            {
                return this.self._get( key,  this.get( 'driver' ) );
            },

            /**
             * @method                                       Cream.Services.Session.remove
             * @description                                  Remove the value from the storage
             *
             * @param   {string}    key                      Key name
             *
             * @returns {boolean}
             *
             */
            remove      : function remove ( key )
            {
                var storage = this.get( 'driver' );

                if ( cream.hasValue( key ) )
                {
                    try
                    {
                        storage.removeItem( key );
                    }
                    catch( e )
                    {
                        return false;
                    }

                    return true;
                }

                return false;
            },

            /**
             * @method                                       Cream.Services.Session.clear
             * @description                                  Remove all the values from the storage
             *
             * @returns {boolean}
             *
             */
            clear       : function clear ( )
            {
                var storage = this.get( 'storage' );

                try
                {
                    storage.clear();
                }
                catch ( e )
                {
                    return false;
                }

                return true ;
            }
        }
    );

} );
