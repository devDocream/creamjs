define( [
    'framework'
],
function AjaxService ( cream )
{

    /**
     * @class               Cream.Services.Ajax
     * @parent              Cream.Services
     * @augments            Cream.Service
     * @classdesc           Ajax service
     *
     * @author              Fernando Coca <fernando@docream.com>
     *
    */
    return cream.service(

        'Cream.Services.Ajax',

        {

        },

        {
            __responseType      : function __responseType ( type )
            {
                var resType = '';

                switch ( type )
                {
                    case 'json':
                        resType = 'responseJSON';
                        break;
                    case 'html':
                        resType = 'responseHTML';
                        break;
                    case 'text':
                        resType = 'responseText';
                        break;
                    case 'script':
                        resType = 'responseScript';
                        break;
                    default:
                        resType = 'responseText';
                        break;
                }

                return resType;
            },

            __configureQuery    : function __configureQuery ( extra )
            {
                var query = {};

                if ( extra.type !== undefined )
                {
                    query.type = extra.type;
                }
                if ( extra.async !== undefined )
                {
                    query.async = extra.async;
                }
                if ( extra.dataType !== undefined )
                {
                    query.dataType = extra.dataType;
                }
                if ( extra.username !== undefined )
                {
                    query.username = extra.username;
                }
                if ( extra.password !== undefined )
                {
                    query.password = extra.password;
                }
                if ( extra.cache !== undefined )
                {
                    query.cache = extra.cache;
                }
                if ( extra.processData !== undefined )
                {
                    query.processData = extra.processData;
                }
                if ( extra.crossDomain !== undefined )
                {
                    query.crossDomain = extra.crossDomain;
                }

                return query;
            },

            _ajax               : function _ajax ( url, data, extra )
            {
                var resType     = '',
                    query       = this.__configureQuery( extra ),
                    response    = {};

                query.url = url;
                query.data = data;

                if ( query.async !== undefined && query.async === false )
                {
                    resType = this.__responseType( query.dataType );

                    response = cream.$.ajax( query );

                    if ( response[ resType ] !== undefined )
                    {
                        return response[ resType ];
                    }
                    return { code : response.status, msg : response.statusText };

                }

                return cream.$.ajax( query );
            }
        },

        {

            init                : function init ()
            {

            },

            /**
             * @method                                       Cream.Services.Ajax.get
             * @description                                  Request type GET
             *
             * @param   {string}           url               URL to make the request
             * @param   {object}           data              Object params to send with the request
             * @param   {object}           extra             Request configuration, lik jquery $.ajax
             *
             * @returns {*}
             *
            */
            get                 : function get ( url, data, extra )
            {
                var query;
                extra = extra !== undefined ? extra : {};
                query = cream.extend( cream.extend( true, {}, extra ), { type : 'GET'} );

                return this.self._ajax( url, data , query );
            },

            /**
             * @method                                       Cream.Services.Ajax.post
             * @description                                  Request type POST
             *
             * @param   {string}           url               URL to make the request
             * @param   {object}           data              Object params to send with the request
             * @param   {object}           extra             Request configuration, lik jquery $.ajax
             *
             * @returns {*}
             *
            */
            post                : function post ( url, data, extra )
            {
                var query;
                extra = extra !== undefined ? extra : {};
                query = cream.extend( cream.extend( true, {}, extra ), { type : 'POST'} );

                return this.self._ajax( url, data , query );
            },
            /**
             * @method                                       Cream.Services.Ajax.put
             * @description                                  Request type PUT
             *
             * @param   {string}           url               URL to make the request
             * @param   {object}           data              Object params to send with the request
             * @param   {object}           extra             Request configuration, lik jquery $.ajax
             *
             * @returns {*}
             *
            */
            put                 : function put( url, data, extra )
            {
                var query;
                extra = extra !== undefined ? extra : {};
                query = cream.extend( cream.extend( true, {}, extra ), { type : 'PUT'} );

                return this.self._ajax( url, data , query );
            },

            /**
             * @method                                       Cream.Services.Ajax.remove
             * @description                                  Request type DELETE
             *
             * @param   {string}           url               URL to make the request
             * @param   {object}           data              Object params to send with the request
             * @param   {object}           extra             Request configuration, lik jquery $.ajax
             *
             * @returns {*}
             *
            */
            remove              : function remove ( url, data, extra )
            {
                var query;
                extra = extra !== undefined ? extra : {};
                query = cream.extend( cream.extend( true, {}, extra ), { type : 'DELETE'} );

                return this.self._ajax( url, data , query );
            },

            /**
             * @method                                       Cream.Services.Ajax.getJson
             * @description                                  Request type GET to obtain a json
             *
             * @param   {string}           url               URL to make the request
             * @param   {object}           data              Object params to send with the request
             * @param   {object}           extra             Request configuration, lik jquery $.ajax
             *
             * @returns {*}
             *
            */
            getJson             : function getJson ( url, data, extra )
            {
                var query;
                extra = extra !== undefined ? extra : {};
                query = cream.extend( cream.extend( true, {}, extra ), { type : 'GET', dataType :'json'} );

                return this.self._ajax( url, data , query );
            },

            /**
             * @method                                       Cream.Services.Ajax.getText
             * @description                                  Request type GET to obtain a text
             *
             * @param   {string}           url               URL to make the request
             * @param   {object}           data              Object params to send with the request
             * @param   {object}           extra             Request configuration, lik jquery $.ajax
             *
             * @returns {*}
             *
            */
            getText             : function getText( url, data, extra )
            {
                var query;
                extra = extra !== undefined ? extra : {};
                query = cream.extend( cream.extend( true, {}, extra ), { type : 'GET', dataType :'text'} );

                return this.self._ajax( url, data , query );
            },

            /**
             * @method                                       Cream.Services.Ajax.getHtml
             * @description                                  Request type GET to obtain a html
             *
             * @param   {string}           url               URL to make the request
             * @param   {object}           data              Object params to send with the request
             * @param   {object}           extra             Request configuration, lik jquery $.ajax
             *
             * @returns {*}
             *
            */
            getHtml             : function getHtml ( url, data , extra )
            {
                var query;
                extra = extra !== undefined ? extra : {};
                query = cream.extend( cream.extend( true, {}, extra ), { type : 'GET', dataType :'html' } );

                return this.self._ajax( url, data , query );
            },

            /**
             * @method                                       Cream.Services.Ajax.getScript
             * @description                                  Request type GET to obtain a script
             *
             * @param   {string}           url               URL to make the request
             * @param   {object}           data              Object params to send with the request
             * @param   {object}           extra             Request configuration, lik jquery $.ajax
             *
             * @returns {*}
             *
            */
            getScript           : function getScript ( url, data, extra )
            {
                var query;
                extra = extra !== undefined ? extra : {};
                query = cream.extend( cream.extend( true, {}, extra ), { type : 'GET', dataType : 'script' } );

                return this.self._ajax( url, data , query );
            },

            /**
             * @method                              Cream.Services.Ajax.download
             * @description                         Download a file via ajax
             *
             * @param   {string} url                Input to check
             * @param   {object} data               Request parameters
             * @param   {string} [method]           Http method
             *
             * @returns {(response|Error)}
             *
            */
            download            : function download ( url, data, method )
            {
                var input, form;

                if ( url && data )
                {
                    form = cream.$( '<form />', { action: url, method: method || 'get ' } );
                    cream.$.each( data, function data ( key, value )
                    {
                        input = cream.$( '<input />',
                            {
                                type    : 'hidden',
                                name    : value.name,
                                value   : value.value
                            } ).appendTo( form );
                    } );
                    return form.appendTo( 'body' ).submit().remove();
                }

                throw new Error( 'File.download( url, data ) - url or data invalid' );
            },

            /**
             *  *** NOT TESTED ***
             *
             * @method                                       Cream.Services.Ajax.download
             * @description                                  Upload file/s via ajax
             *
             * @param   {(string|object)}  selector          Input type file selector or object
             * @param   {string}           url               URL to upload the files
             * @param   {object}           [params]          Options -> params.callback
             *
             * @returns {boolean}
             *
            */
            upload              : function upload ( selector, url, params )
            {

                if ( selector && url )
                {
                    var input       = cream.$( selector ),
                        files       = input.length > 0 ? input[0].files : [],
                        fd          = new FormData(),
                        file,
                        xhr,
                        i;

                    for ( i = 0; i < files.length; i++ )
                    {
                        file = files[i];
                        fd.append( 'files[]', file, file.name );
                    }

                    xhr = new XMLHttpRequest();
                    xhr.open( 'POST', url, true );

                    xhr.onload = function onloadXHR ( )
                    {
                        if ( xhr.readyState === 4 && xhr.status === 200 )
                        {
                            params.callback.apply( this, [ input, JSON.parse( xhr.responseText ) ] );
                        }
                        else if ( xhr.status !== 200 )
                        {
                            throw new Error( "Error: upload file - " + JSON.parse( xhr.responseText ) );
                        }
                    };

                    xhr.send( fd );

                    return false;
                }
            }

        }
    );
} );
