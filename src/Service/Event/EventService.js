define( [
    'cream'
],
function eventService ()
{

    /**
     * @class       Cream.Services.Event
     * @classdesc   Event Service
     *
     * @author      Fernando Coca fernando@docream.com
    */
    cream.service(
        'Cream.Services.Event',
        {
            subscribers :
            {
                sandbox : {},
                kit     : {},
                plugin  : {},
                widget  : {},
                service : {}
            }
        },

        {
            find                : function find( target, vars )
            {
                var found   = false,
                    l       = vars.length,
                    result  = undefined,
                    a       = null,
                    i;

                for ( i = 0; i < l && cream.isFalse( found ); i++ )
                {

                    a = cream.isNull( a )
                        ? target[ vars[ i ] ]
                        : a[ vars[ i ] ];

                    if ( cream.isUndefined( a ) )
                    {
                        found = true;
                    }
                    else
                    {
                        result = a;
                    }
                }

                return {
                        result  : result,
                        found   : found
                };
            },

            findAttr            : function findVar( vars, instance )
            {
                var response;

                response = this.find( instance.attr, vars );

                if ( cream.isFalse( response.found ) )
                {
                    return {
                        target  : response.result,
                        obj     : 'instance'
                    };
                }

                response = this.find( window, vars );

                return {
                    target : response.result,
                    obj    : 'window'
                };
            },

            shifter             : function shifter ( event )
            {
                this.fn.apply( this.obj, [ this.obj, cream.$( event.currentTarget ), event ] );
            },

            bindUnbind          : function bindUnbind ( control, fn, instance, type )
            {
                var parts       = control.split( ' ' ),
                    pL          = cream.isArray( parts ) ? parts.length : 0,
                    selector    = parts.slice( 0, pL - 1 ).join( ' ' ),
                    obj         = {},
                    vars        = pL > 0 && parts [ 0 ].indexOf( '{' ) > -1
                        ?  ( ( cream.trim( parts[0] ) )
                        .replace( '{', '' ) )
                                      .replace( '}', '' )
                        : undefined,
                    callback    = { fn: fn, obj: instance},
                    proxied     = cream.proxy( this.shifter, callback ),
                    partsVars, targetContainer, auxSelector;

                /* Use attr or window var to bind the event*/
                if ( cream.notUndefined( vars ) )
                {
                    partsVars   = vars.split( '.' );
                    obj         = this.findAttr( partsVars, instance );

                    if ( cream.notUndefined( obj.target ) )
                    {
                        auxSelector     = cream.isString( obj.target )
                            ? obj.target
                            : obj.target.selector;

                        if ( obj.obj === 'instance' )
                        {
                            targetContainer = instance.element;

                            return type === 'bind'
                                    ? targetContainer.on( parts[pL - 1], auxSelector, proxied )
                                    : targetContainer.off( parts[pL - 1], auxSelector );
                        }

                        targetContainer = cream.$( document.body );

                        return type === 'bind'
                                ? targetContainer.on( parts[pL - 1], auxSelector, proxied )
                                : targetContainer.off( parts[pL - 1], auxSelector );
                    }

                    return false;
                }

                if ( cream.notBlank( selector ) )
                {
                    targetContainer = instance.element;
                    return type === 'bind'
                        ? targetContainer.on( parts[pL - 1], selector, proxied )
                        : targetContainer.off( parts[pL - 1], selector );
                }

                /* Don't have selector for control, use instance element */
                targetContainer = cream.$( document.body );
                auxSelector     = cream.isString( instance.element )
                    ? instance.element
                    : instance.element.selector;

                return type === 'bind'
                    ? targetContainer.on( parts[ pL - 1 ], auxSelector,  proxied )
                    : targetContainer.off(  parts[ pL - 1 ], auxSelector );
            },

            subscriber              : function subscriber ( instance, fn )
            {
                return {
                    fn          : fn,
                    instance    : instance
                };
            },

            subscriptionType         : function subcriptionType ( instanceType, type )
            {
                var creamType = instanceType.replace( 'cream-', '' );

                if ( type === 'service' )
                {
                    return type;
                }

                switch ( creamType )
                {
                    case 'sandbox'  :
                        return 'kit';

                    case 'kit'      :
                        return [ 'widget', 'plugin' ];

                    case 'plugin'   :
                        return 'widget';

                    case 'widget'   :
                        return 'service';
                }
            },

            unsubscribe             : function unsubscribe ( element, evt, instance, type )
            {
                var parts       = element.split( ':' ),
                    pL          = cream.isArray( parts ) ? parts.length : 0,
                    name        = pL > 0 ? parts[ 0 ] : element,
                    alias       = pL > 0 ? parts[ 1 ] : '',
                    subType     = this.subscriptionType( instance._type, type ),
                    su          = this.get( 'subscribers' ),
                    subs        = cream.isArray( subType ) ? su[ subType [0] ] : su[ subType ],
                    subs2       = cream.isArray( subType ) ? su[ subType [1] ] : undefined,
                    toUnsub;

                if ( cream.notUndefined( subs[ evt ] ) && cream.notUndefined( subs[ evt ][ name ] ) )
                {
                    toUnsub = cream.notBlank( alias ) &&
                              cream.notUndefined( subs[ evt ][ name ][ alias ] )
                        ? 'alias'
                        : 'id';

                    if ( cream.notUndefined( toUnsub ) )
                    {
                        if ( toUnsub === 'alias' )
                        {
                            delete subs[ evt ][ name ][ alias ][ instance.id ];
                            if ( cream.notUndefined( subs2 ) )
                            {
                                delete subs2[ evt ][ name ][ alias ][ instance.id ];
                            }
                        }
                        else
                        if ( toUnsub === 'id' )
                        {
                            delete subs[ evt ][ name].subscribers[ instance.id ];
                            if ( cream.notUndefined( subs2 ) )
                            {
                                delete subs2[ evt ][ name].subscribers[ instance.id ];
                            }
                        }
                    }

                }
            },

            _initSubscribers       : function _initSubscribers  ( subs, subs2, evt, name, alias )
            {
                if ( cream.isUndefined( subs[ evt ] ) )
                {
                    subs[ evt ] = {};
                }

                if ( cream.notUndefined( subs2 ) && cream.isUndefined( subs2[ evt ] ) )
                {
                    subs2[ evt ] = {};
                }

                if ( cream.isUndefined( subs[ evt ][ name ] ) )
                {
                    subs[ evt ][ name ]              = {};
                    subs[ evt ][ name ].subscribers  = {};
                }
                if ( cream.notUndefined( subs2 ) && cream.isUndefined( subs2[ evt ][ name ] ) )
                {
                    subs2[ evt ][ name ]              = {};
                    subs2[ evt ][ name ].subscribers  = {};
                }

                if ( cream.notBlank( alias ) )
                {
                    if ( cream.isUndefined( subs[ evt ][ name ][ alias ] ) )
                    {
                        subs[ evt ][ name ][ alias ]  = {};
                    }
                    if ( cream.notUndefined( subs2 ) &&
                         cream.isUndefined( subs2[ evt ][ name ][ alias ] ) )
                    {
                        subs2[ evt ][ name ][ alias ]  = {};
                    }
                }
            },

            subscribe               : function subscribe ( element, evt, instance, fn, type )
            {
                var parts       = element.split( ':' ),
                    pL          = cream.isArray( parts ) ? parts.length : 0,
                    name        = pL > 0 ? parts[ 0 ] : element,
                    alias       = pL > 0 ? parts[ 1 ] : '',
                    subType     = this.subscriptionType( instance._type, type ),
                    su          = this.get( 'subscribers' ),
                    subs        = cream.isArray( subType ) ? su[ subType [0] ] : su[ subType ],
                    subs2       = cream.isArray( subType ) ? su[ subType [1] ] : undefined,
                    subscriber;

                if ( cream.notBlank( evt ) && cream.notBlank( element ) && cream.isFunction( fn )  )
                {
                    this._initSubscribers( subs, subs2, evt, name, alias );

                    subscriber = this.subscriber( instance, fn );

                    if ( cream.notBlank( alias ) )
                    {
                        subs[ evt ][ name ][ alias ][ instance.id ]  =  subscriber;

                        if ( cream.notUndefined( subs2 ) )
                        {
                            subs2[ evt ][ name ][ alias ][ instance.id ]  =  subscriber;
                        }
                    }
                    else
                    {
                        subs[ evt ][ name].subscribers[ instance.id ] = subscriber;

                        if ( cream.notUndefined( subs2 ) )
                        {
                            subs2[ evt ][ name].subscribers[ instance.id ] = subscriber;
                        }
                    }

                }

            },

            _getCollections         : function ( instance, name, evt, alias )
            {
                var collections = {};
                if ( cream.notUndefined( instance.collectionSubscribers ) )
                {
                    if ( cream.notBlank( alias ) &&
                         cream.notUndefined( instance.collectionSubscribers[ name + ':' + alias] ) &&
                         cream.notUndefined( instance.collectionSubscribers[ name + ':' + alias][ evt ]  ) )
                    {
                        return  instance.collectionSubscribers[ name + ':' + alias][ evt ];
                    }

                    if ( cream.notUndefined( instance.collectionSubscribers[ name ] ) &&
                         cream.notUndefined( instance.collectionSubscribers[ name ][ evt ] ) )
                    {
                        return instance.collectionSubscribers[ name ][ evt ];
                    }

                }

                return collections;
            },

            refreshCollections      : function  refreshCollections( instance, name, evt, alias, opts )
            {
                var collections     = this._getCollections( instance, name, evt, alias ),
                    keys            = cream.keys( collections ),
                    length          = cream.keysLength( collections ),
                    ifz             = cream.get( 'interface' ),
                    key, i;

                if ( length > 0 )
                {
                    for ( i = 0; i < length; i++ )
                    {
                        key = keys[ i ];
                        if ( cream.isFunction( collections[ key ].fn ) )
                        {
                            ifz.get( 'collections' )[ collections[ key ].collection ] =
                                collections[ key].fn.apply( instance, [ opts ] );
                        }


                    }
                }
            },

            publish                 : function publish ( element, evt, options, type )
            {
                var parts       = element.split( ':' ),
                    pL          = cream.isArray( parts ) ? parts.length : 0,
                    name        = pL > 0 ? parts[ 0 ] : element,
                    alias       = pL > 0 ? parts[ 1 ] : '',
                    subs        = this.get( 'subscribers' )[ type ],
                    toTrigger, length, i, keys, key, instance;

                if ( cream.notBlank( name ) && cream.notUndefined( subs[ evt ] ) &&
                     cream.notUndefined( subs[ evt ][ name ] ) )
                {

                    toTrigger = cream.notBlank( alias ) &&
                         cream.notUndefined( subs[ evt ][ name ][ alias ] )
                        ? subs[ evt ][ name ][ alias ]
                        : subs[ evt ][ name].subscribers;

                    if ( Object.keys( toTrigger ) )
                    {
                        keys    = cream.keys( toTrigger );
                        length  = cream.keysLength( toTrigger );

                        for ( i = 0; i < length; i++ )
                        {
                            key         = keys[ i ];
                            instance    = toTrigger[ key ].instance;
                            this.refreshCollections( instance, name, evt, alias, options );

                            toTrigger[ key ].fn.apply( instance, [ options ] );
                        }
                    }

                }
            }
        },

        {
            /**
             * @method                              Cream.Services.Event.bind
             * @description                         Bind a event to the selector with a callback.
             *                                      - if selector is '.class1 click', we will find
             *                                      children with class named .class1 inside
             *                                      instance.element, and the event will be bound
             *                                      to it.
             *                                      - If the selector is 'click', the event will
             *                                      be bound to instance.element directly.
             *                                      - If we have '{foo} click', the event will be
             *                                      bound to instance.attr.foo (jquery object), if
             *                                      it does not exist, it will be bound to window.foo.
             *                                          - we can bind to chained var : '{foo.bar} click'
             *
             * @param   {string}   control           Selector and event
             * @param   {function} fn                Callback function
             * @param   {object}   instance          A object with attr and element properties
             *
             * @returns {$Object}
             *
            */
            bind        : function bind ( control, fn, instance )
            {
                return this.self.bindUnbind( control, fn, instance, 'bind' );
            },

            /**
             * @method                              Cream.Services.Event.unbind
             * @description                         Unbind a event from the selector with a callback
             *
             * @param   {string}   control           Selector and event => '.class1 click'
             * @param   {function} fn                Callback function
             * @param   {object}   instance          A object with attr and element properties
             *
             * @returns {$Object}
             *
            */
            unbind      : function unbind ( control, fn, instance )
            {
                return this.self.bindUnbind( control, fn, instance, 'unbind' );
            },

            /**
             * @method                              Cream.Services.Event.subscribe
             * @description                         Subscribe to a class event
             *
             *
             * @param   {string}   element           Publisher class name
             * @param   {string}   event             Event name
             * @param   {function} fn                Callback function
             * @param   {object}   instance          Subscriber class
             * @param   {string}   type              service|interface
             *
             *
            */
            subscribe   : function subscribe ( element, event, fn, instance, type )
            {
                return this.self.subscribe( element, event, instance, fn, type );
            },

            /**
             * @method                               Cream.Services.Event.unsubscribe
             * @description                          Unsubscribe from a class event
             *
             * @param   {string}   element           Publisher class name
             * @param   {string}   event             Event name
             * @param   {object}   instance          Subscriber class
             * @param   {string}   type              service|interface
             *
             *
            */
            unsubscribe : function unsubscribe ( element, event,  instance, type )
            {
                return this.self.unsubscribe( element, event, instance, type );
            },

            /**
             * @method                              Cream.Services.Event.publish
             * @description                         Trigger a event from publisher class to subscriber
             *                                      instances
             *
             * @param   {string}   element           Publisher class name
             * @param   {string}   event             Event name
             * @param   {object}   options           Event options
             * @param   {string}   type              service|widget|plugin|sandbox|kit
             *
             *
            */
            publish     : function publish ( element, event, options, type )
            {
                return this.self.publish( element, event, options, type );
            }
        }
    );
} );
