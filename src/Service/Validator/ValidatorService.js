define( [
    'framework'
],
function ValidatorService  ( cream )
{
    /**
     * @class               Cream.Services.Validator
     * @parent              Cream.Services
     * @augments            Cream.Service
     * @classdesc           Validator service
     *
     * @author              Fernando Coca <fernando@docream.com>
     *
     */
    return cream.service(

        'Cream.Services.Validator',

        {
            asserts:
            {
                blank :
                {
                    fn      : 'isBlank',
                    message : 'The value should be blank'
                },

                notBlank :
                {
                    fn      : 'notBlank',
                    message : 'The value should not be blank'
                },

                null :
                {
                    fn      : 'isNull',
                    message : 'The value should be null'
                },

                notNull :
                {
                    fn      : 'notNull',
                    message : 'The value should not be null'
                },

                boolean:
                {
                    fn      : 'isBoolean',
                    message : 'The value {{value}} is not a valid boolean: [false, true]'
                },
                booleanText:
                {
                    fn      : 'isBooleanText',
                    message : 'The value "{{value}}" is not a valid boolean text: ["false","true"]'
                },
                array:
                {
                    fn      : 'isArray',
                    message : 'The value is not a valid array'
                },
                string:
                {
                    fn      : 'isString',
                    message : 'The value is not a valid string'
                },
                integer:
                {
                    fn      : 'isInteger',
                    message : 'The value "{{value}}" is not a valid integer'
                },
                float:
                {
                    fn      : 'isFloat',
                    message : 'The value "{{value}}" is not a valid float'
                },
                decimal :
                {
                    fn      : 'isDecimal',
                    message : 'The value "{{value}}" is not a valid decimal'
                },
                number :
                {
                    fn      : 'isNumber',
                    message : 'The value "{{value}}" is not a valid number'
                },
                date :
                {
                    fn      : 'isDate',
                    message : 'The value is not a date object'
                },
                validDate:
                {
                    fn      : 'isValidDate',
                    message : 'The value is not a valid date object'
                },
                timeText:
                {
                    fn      : 'isTimeText',
                    message : 'The value "{{value}}" is not a time in text format'
                },
                validTimeText:
                {
                    fn      : 'isValidTimeText',
                    message : 'The value "{{value}}" is not a valid time in text format]'
                },
                validDateText:
                {
                    fn      : 'isValidDateText',
                    message : 'The value "{{value}}" is not a valid date in text format'
                },
                validDateTimeText:
                {
                    fn      : 'isValidDateTimeText',
                    message : 'The value "{{value}}" is not a valid a dateTime in text format'
                },
                dateTimeText:
                {
                    fn      : 'isDateTimeText',
                    message : 'The value "{{value}}" is not a dateTime in text format'
                },
                dateText:
                {
                    fn      : 'isDateText',
                    message : 'The value "{{value}}" is not a date in text format'
                },
                true:
                {
                    fn      : 'isTrue',
                    message : 'The value "{{value}}" should be true'
                },
                false:
                {
                    fn      : 'isFalse',
                    message : 'The value "{{value}}" should be false'
                },
                object:
                {
                    fn      : 'isObject',
                    message : 'The value is not a valid plain object'
                },
                undefined:
                {
                    fn      : 'isUndefined',
                    message : 'The value should be undefined'
                },
                notUndefined:
                {
                    fn      : 'notUndefined',
                    message : 'The value should not be undefined'
                },
                lower:
                {
                    fn      : 'isLower',
                    message : 'The value should be in lower case'
                },
                upper:
                {
                    fn      : 'isUpper',
                    message : 'The value should be in upper case'
                },
                email:
                {
                    fn      : 'isEmail',
                    message : 'The value is not a valid email'
                },
                length:
                {
                    fn          : 'inLength',
                    exactMessage: 'This value should have exactly {{min}} characters',
                    minMessage  : 'This value is too short. It should have {{min}} characters or more',
                    maxMessage  : 'This value is too long. It should have {{max}} characters or less'
                },
                url:
                {
                    fn      : 'isURL',
                    message : 'The value is not a valid URL'
                },
                ip:
                {
                    fn      : 'isIp',
                    message : 'The value is not a valid IP'
                },
                regex:
                {
                    fn      : 'testRegex',
                    message : 'The value is not valid'
                },
                range:
                {
                    fn              : 'inRange',
                    invalidMessage  : 'The value should be a integer, float or date',
                    minMessage      : 'This value should be {{min}} or more',
                    maxMessage      : 'This value should be {{max}} or less'
                },
                equalTo:
                {
                    fn      : 'equalTo',
                    message : 'This value should be equal to {{compare}}'
                },
                identicalTo:
                {
                    fn      : 'identicalTo',
                    message : 'This value should be identical to {{compare}}'
                },
                lessThan:
                {
                    fn      : 'lt',
                    message : 'This value should be less than {{compare}}'
                },
                greaterThan:
                {
                    fn      : 'gt',
                    message : 'This value should be greater than {{compare}}'
                },
                lessThanOrEqual:
                {
                    fn      : 'lte',
                    message : 'This value should be less or equal than {{compare}}'
                },
                greaterThanOrEqual:
                {
                    fn      : 'gte',
                    message : 'This value should be greater or equal than {{compare}}'
                },
                type:
                {
                    fn      : 'is',
                    message : 'The value {{value}} is not a valid {{type}}'
                },
                count:
                {
                    fn      : 'inCount',
                    exactMessage: 'The collection should contain exactly {{min}} elements',
                    minMessage  : 'The collection should contain {{min}} elements or more',
                    maxMessage  : 'The collection should contain {{max}} elements or less'
                },
                choice:
                {
                    fn          : 'inChoices',
                    message     : 'The value you selected is not a valid choice',
                    minMessage  : 'You must select at least {{min}} choices',
                    maxMessage  : 'You must select at most {{max}} choices',
                    multipleMessage:'One or more of the given values is invalid'
                }

            },

            types:
            {
                options : {
                    messages: [ 'min', 'max'],
                    params: function( config, value )
                    {
                        return [ value, config.min, config.max, true ];
                    },
                    elements: ['length', 'count', 'choice', 'range']
                },
                regex  : {
                    messages : [],
                    params   : function ( config, value )
                    {
                        return [ value, config.pattern, true ];
                    },
                    elements : ['regex']
                },
                compare:
                {
                    messages : [ 'compare'],
                    params   : function ( config, value )
                    {
                        return [ value, config.compare, true ];
                    },
                    elements:  ['equalTo', 'identicalTo', 'lessThanOrEqual',
                        'greaterThanOrEqual', 'greaterThan', 'lessThan']
                },
                type :
                {
                    messages: ['type'],
                    params   : function ( config, value )
                    {
                        return [ value, config.type, true ];
                    },
                    elements:  ['type']
                }
            }


        },
        {
            _getMessage     : function _getMessage ( msgResult, config, name, value )
            {
                var asserts     = this.get( 'asserts' ),
                    types       = this.get( 'types' ),
                    msg         = config[ msgResult + 'Message' ] !== undefined
                                    ? config[ msgResult + 'Message' ]
                                    : asserts[ name ][ msgResult + 'Message'] !== undefined
                                      ? asserts[ name ][ msgResult + 'Message' ]
                                      : asserts[ name ].message,
                    keys        = cream.keys( types ),
                    length      = cream.keysLength( types ),
                    keys2, length2,
                    prop, i, prop2, j;


                for ( i = 0; i < length; i++ )
                {
                    prop = keys[ i ];
                    if ( types[ prop ].elements.indexOf( name ) > -1 )
                    {
                        keys2   = cream.keys( types[prop].messages );
                        length2 = cream.keysLength( types[prop].messages );

                        for ( j = 0; j < length2; j++ )
                        {
                            prop2 = keys2[ j ];
                            msg = cream.replace(
                                                msg,
                                                '{{' + types[ prop ].messages[ prop2 ] + '}}',
                                                config[ types[ prop ].messages[ prop2 ] ]
                                    );
                        }

                        msg = cream.replace( msg, '{{value}}', value );

                        return msg;
                    }
                }

                return msg;
            },

            _getParams      : function _getParams ( assert, config, value )
            {
                var types    = this.get( 'types' ),
                    keys     = cream.keys( types ),
                    length   = cream.keysLength( keys ),
                    i, prop;

                for ( i = 0; i < length; i++ )
                {
                    prop = keys[ i ];

                    if ( types[ prop ].elements.indexOf( assert ) > - 1 )
                    {
                        return types[prop].params( config, value );
                    }
                }

                return [ value ];
            },

            _getFn          : function _getFn( name )
            {

                var asserts = this.get( 'asserts' ),
                    nameFn  =  asserts[ name ].fn,
                    parts   = nameFn.split( '.' ),
                    serv    = parts !== undefined && parts.length > 1
                                ? cream.get( parts[0] )
                                : parts[0],
                    fn      = parts !== undefined && parts.length > 1
                                ? serv[ parts[1] ]
                                : cream[ parts[0] ];

                return fn;

            },

            _validate       : function _validate ( value, assert, nameAssert )
            {
                var fn, result;

                if ( cream.isString( assert ) )
                {
                    fn = this._getFn( assert );

                    result = fn.apply( Cream, this._getParams( nameAssert , assert, value ) );

                    return result === false
                            ? this._getMessage( result , assert, nameAssert, value )
                            : result;
                }

                fn = this._getFn( nameAssert );

                result = fn.apply( cream, this._getParams( nameAssert , assert, value ) );

                return result !== true
                        ? this._getMessage( result , assert , nameAssert, value )
                        : result;
            }
         },
        {
            init        : function init ()
            {
                var services         = System.Cream.services,
                    validatorService = services !== undefined ? System.Cream.services.Validator : '',
                    options;

               if ( validatorService !== '' )
               {
                   options = validatorService;

                   if ( options.asserts !== undefined )
                   {
                       this.attr.asserts = this.self.asserts =
                       cream.extend( cream.extend( true, {}, this.attr.asserts ), options.asserts );
                   }

                   if ( options.types !== undefined )
                   {
                       this.attr.types = this.self.types =
                       cream.extend( cream.extend( true, {}, this.attr.types ), options.types );
                   }
               }
            },

            /**
             * @method                                      Cream.Services.Validator.validate
             * @description                                 Validate a object against his assertions
             *
             * @param   {object}        properties          Properties to validate
             * @param   {object}        asserts             Properties assertions
             *
             * @returns {boolean}
             *
             *
             * @example
             *
             * asserts: {
             *      'id'        :
             *      {
             *          notBlank: {
             *                     message: 'hola'
             *
             *         }
             *     },
             *     'name'      : {
             *          notBlank:{
             *                     message: 'adios'
             *         },
             *         length : {
             *                     min: 5,
             *                     max : 10
             *        }
             *     },
             *     'lastname'  : ['notBlank'],
             *     'sex'       : [],
             *    'active'    : 'boolean'
             * }
            */
            validate    : function validate ( properties, asserts )
            {
                var validations = [],
                    keys        = cream.keys( asserts ),
                    length      = cream.keysLength( asserts ),
                    i, prop,
                    property, assert, validated, arrObj,
                    keys2, prop2, length2, j,
                    nameAssert, element;

                if ( length === 0 )
                {
                    return true;
                }


                for ( i = 0; i < length; i++ )
                {

                    prop        = keys [ i ];
                    property    = properties[prop];
                    assert      = asserts[prop];
                    validated   = true;
                    assert      = cream.isString( assert )
                                ? [assert]
                                : cream.isArray( assert ) || cream.isObject( assert )
                                           ? assert
                                           : [];

                    arrObj      = cream.isString( assert )
                                ?  assert !== 'nullable'
                                : typeof assert.indexOf === 'function'
                                             ? assert.indexOf( 'nullable' ) === -1
                                             : assert['nullable'] === undefined;

                    keys2       = cream.keys( assert );
                    length2     = cream.keysLength( assert );

                    for ( j = 0; j < length2; j++ )
                    {
                        prop2           = keys2[ j ];

                        element         = assert[prop2];
                        nameAssert      = cream.isArray( assert ) ? element : prop2;

                        if ( nameAssert !== 'nullable' )
                        {
                            validated = this.self._validate( property, element, nameAssert );

                            if ( arrObj === false && validated !== true )
                            {
                                if ( cream.notUndefined( property ) &&  cream.notNull( property ) )
                                {
                                    validations.push( {
                                        property : prop,
                                        value    : property,
                                        msg      : validated
                                    } );
                                }

                            }
                            else
                            {
                                if ( arrObj &&
                                     ( cream.isUndefined( property ) || cream.isNull( property ) ) )
                                {
                                    validations.push( {
                                        property : prop,
                                        value    : property,
                                        msg      : this.get( 'asserts' ).notNull.message
                                    } );
                                }
                                else if ( validated !== true )
                                {
                                    validations.push( {
                                        property : prop,
                                        value    : property,
                                        msg      : validated
                                    } );
                                }
                            }
                        }
                    }
                }
                return validations.length === 0 ?  true :  validations ;
            }
        } );

} );
