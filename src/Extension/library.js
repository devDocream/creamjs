require( 'vendor/creamjs/src/Framework/Bootstrap/Configure' );

var compiler = {
    compile : function compile ( name )
    {
        var lib = System.Cream.lib;

        return 'define("' + name + '",["vendor/creamjs/src/Adapter/Library/'
               + lib
               + '"], function ( adapter ) {\n\t'
               + 'return adapter;\n});';
    }

};

if ( steal.config( 'env' ) === 'production' )
{
    exports.fetch = function fetch ( load )
    {
        //System.import(load.name);
        return '';
    };
}
else
{
    exports.instantiate = function inst ( load )
    {
        load.metadata.deps = [];
        load.metadata.execute = function execute ()
        {
            return System.newModule( {} );
        };

        load.source = compiler.compile( load.name );
        load.metadata.format = "amd";
    };
}
exports.buildType = "js";
exports.includeInBuild = true;
