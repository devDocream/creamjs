require( 'vendor/creamjs/src/Adapter/Library/cream.library!' );

var compiler = {

    compile : function compiler ( name )
    {
        var utils       = System.Cream.utils,
            keys        = Object.keys( utils ),
            length      = keys.length,
            paths       = [],
            utilCream   = -1,
            i;

        for ( i = 0; i < length; i++ )
        {
            if ( utils[keys[i]] !== false )
            {
                utilCream = utils[keys[i]].indexOf( '/' );

                if ( utilCream === -1 )
                {
                    paths.push( "'util/" + utils[keys[i]] + "'" );
                }
                else
                {
                    paths.push( "'" + utils[keys[i]] + "'" );
                }
            }
        }

        return 'define("' + name + '",[' + paths.join( ',' ) + '], function () {\n\t'
                + 'var length = arguments.length;\n\t'
                + 'for (var i = 0; i < length; i++ )\n\t'
                + '{\n\t'
                + 'window.cream.extend( window.cream, arguments[i])\n\t'
                + '}\n\t'
                + 'return window.cream;\n});';
    }

};

if ( steal.config( 'env' ) === 'production' )
{
    exports.fetch = function fetch ()
    {
        return '';
    };
}
else
{
    exports.instantiate = function inst ( load )
    {
        load.metadata.deps = [];
        load.metadata.execute = function execute ()
        {
            return System.newModule( {} );
        };

        load.source = compiler.compile( load.name );
        load.metadata.format = "amd";
    };
}
exports.buildType = "js";
exports.includeInBuild = true;
