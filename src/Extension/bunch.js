var compiler =
{

    vendorBunch  : function vendorBunch ( path )
    {
        var bunch       = [],
            vendor      = [],
            parts       = path.split( '/' ),
            length      = parts.length,
            sbName      = parts[ parts.length-1 ].replace( 'Sandbox','' ),
            vendorName  = parts[0],
            bunchName   = '',
            found       = false,
            i           = 0;

        while ( i < length && !found )
        {
            if ( parts[ i ].indexOf( 'Bunch' ) > -1 )
            {
                bunch.push( parts[ i ] );
                bunchName = parts[ i].replace( 'Bunch', '' );
                found = true;
            }
            else
            {
                bunch.push( parts[ i ] );
                vendor.push( parts[ i ] );
            }

            i++;
        }

        return {
            vendor      : vendor.join( '/' ) + '/' + vendorName,
            bunch       : bunch.join( '/' ) + '/' + bunchName + 'Bunch',
            sbName      : sbName
        };
    },

    compile : function compile ( name )
    {
        var route   = System.Cream.route,
            sandbox = route !== undefined ? route.value.development : '',
            bunch, vendor, path;

        if ( sandbox !== '' )
        {

            path     = this.vendorBunch( route.value.development );
            bunch    = path.bunch;
            vendor   = path.vendor;

            return 'define("' + name + '",["'
                   + bunch
                   + '", "'
                   + vendor
                   + '"], function ( b , v ) {\n\t'
                   + 'System.Cream.bunch = b;\n\t'
                   + 'System.Cream.vendor = v;\n\t'
                   + 'System.Cream.bunch.name = "' + path.sbName + '";\n\t'
                   + 'return {};\n});';
        }

        return 'define("' + name + '",[], function () {\n\t'
               + 'return {};\n});';
    }

};

if ( steal.config( 'env' ) === 'production' )
{
    exports.fetch = function fetch ( load )
    {
        return '';
    };
}
else
{
    exports.instantiate = function inst ( load )
    {
        load.metadata.deps = [];
        load.metadata.execute = function execute ()
        {
            return System.newModule( {} );
        };

        load.source = compiler.compile( load.name );
        load.metadata.format = "amd";
    };
}
exports.buildType = "js";
exports.includeInBuild = true;