var compiler = {

    compile     : function compile ( template, options )
    {
        var tokens  = JSON.stringify( template.tokens ),
            id      = template.id,
            output;
        if ( options.module )
        {
            output = this.module( id, tokens, options.twig, options.name );
        }

        return output;
    },

    module      : function module ( id, tokens, pathToTwig, name )
    {
        return 'define("' + name + '",["' + pathToTwig
               + '"], function (Twig) {\n\tvar twig, templates;\ntwig = Twig.twig;\ntemplates = '
               + this.wrap( id, tokens ) + '\n\t window.System.Cream.templates["'
               + id + '"] = templates; return templates;\n});';
    },

    wrap        : function wrap ( id, tokens )
    {
        return 'twig({id:"'
                + id.replace( '"', '\\"' )
                + '", data:'
                + tokens
                + ', precompiled: true}); \n';
    }

}

if ( steal.config( 'env' ) === 'production' )
{
    exports.fetch = function fetch ( )
    {
        return '';
    };
}
else
{
    exports.instantiate = function instantiate ( load )
    {
        var f   = '/',
            f2  = f.replace( /[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&" ),
            id  = 'template:'
                    + load.address.replace( System.baseURL,'' )
                    .replace( new RegExp( f2 ,'g' ), '_' )
                    .replace( '.twig','' ).replace( 'src_', '' )
                    .toLowerCase(),
            template,
            options;

        load.metadata.deps = [];
        load.metadata.execute = function exec ()
        {
            return System.newModule( {} );
        };

        template = { id : id, tokens : load.source };
        options  = { module : 'amd', twig : 'twig', name : load.name};

        load.source = compiler.compile( template, options );
        load.metadata.format = "amd";
    };
}
exports.buildType = "js";
exports.includeInBuild = true;
