var compiler = {
    compile : function compile ( name )
    {
        var engine = System.Cream.services.View.engine;

        if ( engine === undefined )
        {
            engine = 'twig';
        }
        return 'define("' + name + '",["service/View/Engine/'
               + engine + '/'
               + engine
               + '"], function ( adapter ) {\n\t'
               + 'return adapter;\n});';
    }

};

if ( steal.config( 'env' ) === 'production' )
{
    exports.fetch = function fetch ( load )
    {
        return '';
    };
}
else
{
    exports.instantiate = function inst ( load )
    {
        load.metadata.deps = [];
        load.metadata.execute = function execute ()
        {
            return System.newModule( {} );
        };

        load.source = compiler.compile( load.name );
        load.metadata.format = "amd";
    };
}
exports.buildType = "js";
exports.includeInBuild = true;
