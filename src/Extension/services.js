/**
 * @namespace Cream.Services
 * @description Creamjs services
 * @global
 *
 * @author Fernando Coca <fernando@docream.com>
 *
 */
require( 'framework' );
require( 'component/Component' );

var compiler = {

    compile : function compiler ( name )
    {
        var servs           = System.Cream.services,
            keys            = Object.keys( servs ),
            length          = keys.length,
            paths           = [],
            i;

        for ( i = 0; i < length; i++ )
        {
            if ( servs[  keys[i] ] !== false )
            {
                if ( servs[ keys[ i ] ].path === undefined )
                {
                    paths.push( "'service/" + keys[i] + '/' + keys[i] + "Service'" );
                }
                else
                {
                    paths.push( "'" + servs[ keys[ i ] ].path + "'" );
                }
            }
        }

        return 'define("' + name + '",[' + paths.join( ',' ) + '], function ( ) {\n\t'
                + 'return window.cream \n});';
    }

};

if ( steal.config( 'env' ) === 'production' )
{
    exports.fetch = function fetch ()
    {
        return '';
    };
}
else
{
    exports.instantiate = function inst ( load )
    {
        load.metadata.deps = [];
        load.metadata.execute = function execute ()
        {
            return System.newModule( {} );
        };

        load.source = compiler.compile( load.name );
        load.metadata.format = "amd";
    };
}
exports.buildType = "js";
exports.includeInBuild = true;
