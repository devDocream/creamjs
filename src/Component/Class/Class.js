define( [
    'framework'
],
function creamClass ( cream )
{

    // Inspired by Simple JavaScript Inheritance By John Resig http://ejohn.org/
    // and $.Class of JavascriptMVC by Justin Meyer

    var initializing    = false,
        fnTest          = /xyz/.test( function ()
        {
            xyz;
        } ) ? /\b_super\b/ : /.*/;

    /**
     * @class       Cream.Class
     * @parent      cream
     * @classdesc   Creamjs Class constructor
     *
     * @author      Fernando Coca fernando@docream.com
     *
    */
    Cream.Class = function cls ()
    {
        if ( cream.isUndefined( arguments.length ) )
        {
            return new Error( 'At least one Class argument is required' );
        }

        return Cream.Class.extend.apply( Cream.Class, arguments );
    };

    // Create a new Class that inherits from this class
    cream.extend(
    Cream.Class,
    {

        _new            : function _new ( nw )
        {
            var inst;
            initializing    = true;
            inst            = new this();
            initializing    = false;

            if ( nw === true )
            {
                if ( !cream.isUndefined( inst.setup ) )
                {
                    return inst.setup( arguments );
                }
            }

            return inst;
        },

        _newClass       : function _newClass ( cls, scls )
        {
            var keys    = cream.keys( scls ),
                i       = 0,
                length  = keys.length,
                name;

            for ( i = 0; i < length; i++ )
            {
                name = keys[ i ];
                // Check if we're overwriting an existing function
                cls[name] = scls[name];
            }

            return cls;
        },

        /**
         * @method                          Cream.Class._supers
         * @description                     Introduce _super function inside child function
         *                                  to call parent function
         * @private
         *
         * @param {object} properties       Child properties
         * @param {object} super            Parent properties
         * @param {string} name             Property name to extend
         * @returns {object}
        */
        _supers         : function supers ( prop, _super, toExtend, nameToCheck )
        {
            // Check if we're overwriting an existing function
            var superfn = function supers ( name, fn )
                {
                    return function superAux ()
                    {
                        var tmp = this._super,
                            ret;

                        // Add a new ._super() method that is the same method
                        // but on the sup0er-class
                        this._super = _super[name];

                        // The method only need to be bound temporarily, so we
                        // remove it when we're done executing
                        ret = fn.apply( this, arguments );
                        this._super = tmp;

                        return ret;
                    };
                },
                keys    = cream.keys( prop ),
                i       = 0,
                length  = keys.length,
                name;

            for ( i = 0; i < length; i++ )
            {
                name = keys[ i ];
                toExtend[name] = cream.isFunction( prop[name] ) &&
                                 cream.isFunction( _super[name] ) &&
                                 fnTest.test( prop[name] ) &&
                                 name !== nameToCheck
                    ? superfn( name, prop[name] )
                    : prop[name];
            }

            return toExtend;
        },

        _prototype      : function _prototype ( prototype, prop, _super )
        {
            return this._supers( prop, _super, prototype );
        },

        _other          : function _other ( prop, _super )
        {
            _super =  !cream.isUndefined( _super )
                        ? _super
                        : {};

            return this._supers( prop, _super, _super );
        },

        _private        : function _private ( priv, prop, _super )
        {
            return this._supers( prop, _super, priv, 'self' );
        },

        _applySetupInit : function setini ()
        {
            // Get a raw instance object (`init` is not called).
            var inst    = this._new(),
                arg     = arguments,
                length  = arg.length;


            // Call `setup` if there is a `setup`
            if ( inst.setup  && !cream.isUndefined( inst._type ) )
            {
                return inst.setup.apply( inst, arg[ length - 1 ] );
            }

            return inst;
/*
            // Call `init` if there is an `init`
            if ( inst.init && !cream.isUndefined( inst._type ) )
            {
                inst.init.apply( inst, arg[ length - 1 ] );
            }
*/
        },

        _generateParts : function parts ( nameSpace, priv, proto, attr )
        {
            if ( !cream.isString( nameSpace ) )
            {
                proto       = nameSpace;
                nameSpace   = 'Class';
                attr        = {};
                priv        = {};
            }

            if ( cream.isUndefined( proto ) )
            {
                if ( cream.isUndefined( priv ) )
                {
                    proto   = cream.isUndefined( attr )
                        ? {}
                        : attr;
                    priv    = {};
                    attr    = {};
                }
                else
                {
                    proto   = priv;
                    priv    = {};
                }
            }

            return { proto: proto, attr: attr, priv: priv, nameSpace: nameSpace };
        },

        setup           : function setup ()
        {
            this.self.attr = this.attr;

        },

        extend          : function extend ()
        {

            var args        = [],
                nameSpace   = arguments[0],
                attr        = arguments[1],
                priv        = arguments[2],
                proto       = arguments[3],
                args2       = [],
                parts,
                prop;

            if ( cream.isUndefined( nameSpace ) || !cream.isString( nameSpace ) )
            {
                return  'First argument for Class constructor must be a string' ;
            }

            if ( ( cream.keys( arguments ) ).length > 4 )
            {
                for ( prop in arguments )
                {
                    args2.push( arguments[prop] );
                }
                args = args2.slice( 4, args2.length );
            }

            this.originalArgs = [];

            if ( !cream.isUndefined( args[0] ) )
            {
                this.originalArgs = args;
            }

            parts       = this._generateParts( nameSpace, priv, proto, attr );
            attr        = parts.attr;
            priv        = parts.priv;
            proto       = parts.proto;
            nameSpace   = parts.nameSpace;

            var _super_class    = this,
                _super          = this.prototype,
                attrs           = cream.extend( cream.extend( true, {}, this.prototype.attr ), attr ),
                privs           = {},
                partName,
                prototype;

            prototype = this._new();

            // Copy the properties over onto the new prototype
            prototype = this._prototype( prototype, proto, _super );

            // The dummy class constructor
            function Class ()
            {
                var args = arguments,
                    at;

                // All construction is actually done in the init method
                if ( !initializing )
                {
                    if ( args.length && cream.isUndefined( this._originalArgs ) &&
                            cream.isString( args[0] ) )
                    {
                        return Class.extend.apply( Class, args );
                    }

                    at = !cream.isUndefined( args ) && args.length > 0
                            ? this._originalArgs.concat( args )
                            : this._originalArgs;

                    return Class._applySetupInit.apply( Class, at );
                }
            }

            // Populate our constructed prototype object
            var Class = this._newClass( Class, _super_class );

            // Enforce the constructor to be what we expect
            if ( cream.isString( nameSpace ) )
            {
                var cls     = Class,
                    global  = cream.getGlobal( nameSpace, cls );

                global = cls;
            }

            privs       = this._private( privs, prototype.self, _super.self );
            privs       = this._prototype( privs, priv, this.prototype.self );
            partName    = nameSpace.split( '.' );

            // And make this class extendable
            cream.extend( Class, {
                constructor : Class,
                prototype   : prototype,
                name        : nameSpace

            } );

            // Make sure our prototype looks nice.
            Class.prototype.constructor = Class;
            Class.prototype.constructor.attr = attrs;
            Class.prototype.constructor.self = priv;
            Class.prototype._originalArgs = args;
            Class.prototype.self = privs;
            Class.prototype.attr = attrs;
            Class.prototype.self.attr = attrs;

            /**
             * @name            Cream.Class._name
             * @type            {string}
             * @description     Class complete name
             *
             */
            Class.prototype._name = nameSpace;

            /**
             * @name            Cream.Class._shortName
             * @type            {string}
             * @description     Class short name
             *
             */
            Class.prototype._shortName = partName[partName.length - 1];

            /**
             * @name            Cream.Class._type
             * @type            {string}
             * @description     Class type
             *
             */

            Class.prototype._type = cream.getClassName( this.prototype._name );
            /**
             * @name            Cream.Class._cls
             * @type            {string}
             * @description     CSS style class name
             *
             */
            Class.prototype._cls = cream.getClassName( nameSpace );

            for ( var i  in this.prototype._originalArgs )
            {
                var name = this.prototype._originalArgs[i].name;

                if ( cream.notUndefined( name ) )
                {
                    Class.prototype[name] = this._other( args[i], this.prototype._originalArgs[i] );
                }
            }
            return Class;
        }

} );
    /**
     * Method called before the class is created
     *
     * @method          Cream.Class.setup
     * @description     Called before the class is created
     * @returns         {Cream.Class}
     *
    */
    Cream.Class.prototype.setup = function setup ()
    {
       if ( typeof this.init === 'function' )
        {
            this.init.apply( this, arguments );
        }

        return this;
    };

    /**
     * Method called after the class is created
     *
     * @method          Cream.Class.init
     * @description     Called after setup finishes
     * @returns         {}
     *
    */
    Cream.Class.prototype.init = function init ()
    {};

    /**
     * The methods defined in the private object
     *
     * @method          Cream.Class.self
     * @description     Private methods
     *
    */
    Cream.Class.prototype.self = {};
    Cream.Class.prototype.self.attr = {};

    /**
     * @method          Cream.Class.get
     * @description     Getter for Class attributes
     * @param           {string} name - Attribute's name
     *
     * @returns         {Object|string|function|null}
     *
    */
    Cream.Class.prototype.get = Cream.Class.prototype.self.get = function get ( name )
    {
        if ( !cream.isUndefined( this.attr[name] ) )
        {
            return this.attr[name];
        }

        return null;
    };

    /**
     * @method          Cream.Class.attr
     * @description     Class attributes
     *
    */
    Cream.Class.prototype.attr = {};

    /**
     * @method          Cream.Class.set
     * @description     Setter for Class attributes
     * @param           {string}                 Name   - Attribute's name
     * @param           {string|function|Object} Value  - Attribute's value
     *
     * @returns         {Cream.Class}
     *
    */
    Cream.Class.prototype.set = Cream.Class.prototype.self.set = function set ( name, value )
    {
        if ( !cream.isUndefined( name )  && name !== '' )
        {
            this.attr[name] = value;
            if ( !cream.isUndefined( this.self ) )
            {
                this.self.attr[name] = value;
            }
        }

        return this;
    };

    Cream.Class.prototype._name = 'Class';

    return Cream.Class;

}
);
