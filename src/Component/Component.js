define( [
    'framework/Container/Container',
    'service',
    'widget',
    'plugin',
    'kit',
    'sandbox',
    'repository'
],
function component (
    container,
    Service,
    Widget,
    Plugin,
    Kit,
    Sandbox,
    Repository
)
{

    var dataDependencies = function dataDependencies( nam, typ )
    {
        var dd      = System.Cream.DD,
            type    = dd !== undefined ? dd[ typ ] : '',
            name    = type !== undefined && type !== '' ? type[ nam ] : '',
            datas   = System.Cream.data !== undefined ? System.Cream.data : {},
            deps, key, keys, length, i, result;

        result = {};

        if ( name !== '' )
        {
            deps = name.deps;
            keys = cream.keys( deps );
            length = cream.keysLength( deps );

            for ( i = 0; i < length; i++ )
            {
                key = keys [ i ];
                result[ key ] = datas[ deps[ key ] ];
            }
        }
        return result;
    };

    cream.extend(
        cream,
        {

            /**
             * @method                              cream.service
             * @description                         Service Factory
             *                                      (review Service constructor  docs)
             *
             *
             * @returns {Cream.Service}
             *
            */
            service     : function service ()
            {
                var serv        = Service.apply( this, arguments ),
                    options     = {},
                    nameParts   = arguments[0].split( '.' ),
                    name        = nameParts[ nameParts.length - 1].toLowerCase(),
                    instance,
                    dd;

                dd       = dataDependencies( name, 'service' );
                options  = cream.extend( cream.extend( true, {}, options ), dd );

                instance = new serv( options );

                container.service( instance );

                return instance;
            },

            /**
             * @method                              cream.repository
             * @description                         Repository Factory
             *                                      (review Repository constructor  docs)
             *
             *
             * @returns {Cream.Repository}
             *
             */
            repository  : function repository ()
            {
                var repo        = Repository.apply( this, arguments ),
                    options     = {},
                    nameParts   = arguments[0].split( '.' ),
                    name        = nameParts[ nameParts.length - 1].toLowerCase(),
                    instance,
                    dd;

                dd       = dataDependencies( name, 'repository' );
                options  = cream.extend( cream.extend( true, {}, options ), dd );

                instance = new repo( options );

                container.repository( instance );

                return instance;
            },
            /**
             * @method                              cream.widget
             * @description                         Widget Factory
             *                                      (review Widget constructor docs)
             *
             *
             * @returns {Cream.Widget}
             *
            */
            widget      : function widget ()
            {
                var wdgt = Widget.apply( this, arguments );

                container.interfaces( arguments[0], 'widget', wdgt );

                return wdgt;
            },

            /**
             * @method                              cream.kit
             * @description                         Kit Factory
             *                                      (review Kit constructor docs)
             *
             *
             * @returns {Cream.Kit}
             *
            */
            kit         : function kit ()
            {
                var kt = Kit.apply( this, arguments );

                container.interfaces( arguments[0], 'kit', kt );

                return kt;
            },

            /**
             * @method                              cream.sandbox
             * @description                         Sandbox Factory
             *                                      (review Sandbox constructor docs)
             *
             *
             * @returns {Cream.Sandbox}
             *
             */
            sandbox     : function sandbox ()
            {
                var sb = Sandbox.apply( this, arguments );

                container.interfaces( arguments[0], 'sandbox', sb );

                return sb;
            },

            /**
             * @method                              cream.plugin
             * @description                         Plugin Factory
             *                                      (review Plugin constructor docs)
             *
             *
             * @returns {Cream.Plugin}
             *
             */
            plugin      : function plugin ()
            {
                var plugn = Plugin.apply( this, arguments );

                container.interfaces( arguments[0], 'plugin', plugn );

                return plugn;
            },

            /**
             * @method                              cream.get
             * @description                         Returns a cream service instance. If type and
             *                                      alias are empty, returns a service.
             *
             * @param {string}      name            Class short name
             * @param {string}      [type]          Class type => widget, plugin, ...
             * @param {string}      [alias]         Class alias to identify it
             *
             * @returns {(Cream.Service|Cream.Widget|Cream.Plugin|Cream.Kit|Cream.Repository)}
             *
            */
            get         : function get ( name, type, alias )
            {
                var caps =  cream.capitalize( name );

                if ( cream.isUndefined( type ) && cream.isUndefined( alias ) )
                {
                    return container.service( caps );
                }

                return container.get( caps, type, alias );
            },

            /**
             * @method                              cream.add
             * @description                         Add instance by type to the container
             *
             * @param {object}      instance        Class instance
             * @param {string}      type            Class type

             *
             *
            */
            add         : function add ( instance, type )
            {
                container.add( instance, type );
            },

            /**
             * @method                              cream.remove
             * @description                         Remove instance from the container
             *
             * @param {object}       instance       Instance to remove
             *
             *
            */
            remove      : function remove ( instance )
            {
                container.remove( instance );
            }
        }
    );
}
);
