define( [
    'framework',
    'class'
], function pluginClass (
    cream,
    Class
)
{

    /**
     * @class           Cream.Plugin
     * @parent          Cream
     * @inherits        Cream.Class
     * @augments        Cream.Class
     *
     * @classdesc       Cream Plugin Constructor
     *
     * @author          Fernando Coca <fernando@doCream.com>
     *
     */
    return Class(

        'Cream.Plugin',

        {

            /**
             * @name                    Cream.Plugin.parent
             * @description             The parent kit or plugin
             *
             * @type {(object|Cream.Kit)}
             *
             */
            parent      : {},

            /**
             * @name                    Cream.Plugin.view
             * @description             Pre-configured plugin view
             *
             * @type {(string|View)}             View object
             *
             */
            view        : '',

            cls         : '',

            /**
             * @name                    Cream.Plugin.place
             * @description             Place to initialize the plugin:
             *                          - place.value: jquery element or selector to before and
             *                          after place-types;
             *                          - place.type: (over|before|after|prepend|append)
             * @type {object}
             *
             */
            place       : {},

            /**
             * @name                    Cream.Plugin.selectors
             * @description             Pre-configured selectors to cache
             *
             * @type {object}
             *
             */
            selectors    : {},

            /**
             * @name                    Cream.Plugin.elements
             * @description             Pre-cached element selectors from classes attr
             *
             * @type {object}
             *
             */
            elements    : {},

            entity      : false,

            field       : false,

            cache       : false,

            widget     : {}

        },

        /**
         * @name                        Cream.Plugin.self
         * @description                 Private methods
         *
         * @type {Object}
         *
         */
        {

        },

        {
            /**
             * @name                                  Cream.Plugin.element
             * @description                           Plugin html root element
             *
             * @type {object}
            */
            element : {},

            /**
             * @name                                  Cream.Plugin.alias
             * @description                           Plugin alias
             *
             * @type {string}
            */
            alias   : '',

            /**
             * @name                                  Cream.Plugin.selector
             * @description                           CSS style selector where the plugin
             *                                        was initialized
             *
             * @type {string}
             */
            selector   : '',

            /**
             * @name                                  Cream.Plugin.id
             * @description                           Plugin id
             *
             * @type {integer}
            */
            id         : undefined,

            /**
             * @method                                 Cream.Plugin.setup
             * @description                            Plugin constructor.
             *                                         Don't override. If is necessary, remember
             *                                         call to this._super setup
             *
             * @param {string}      selector           Target DOM selector
             * @param {object}      [options]          Options to create the plugin
             * @param {string}      [alias]            Plugin alias
             *
            */
            setup           : function setup ( selector, options, alias )
            {

                if ( cream.isUndefined( options ) )
                {
                    options = {};
                }

                this.attr   = cream.extend( cream.extend( true, {}, this.attr ), options );
                this.alias  = cream.notUndefined( alias ) ? alias : '';

                cream.get( 'interface' )
                    .create( this, selector, 'plugin' );

                return this._super.apply( this, arguments );
            },

            /**
             * @method                                 Cream.Plugin.init
             * @description                            Called after setup() finishes
             *
             * @param {string}      selector           Target DOM selector
             * @param {object}      [options]            Options to create the plugin
             * @param {string}      [alias]           Plugin alias
             *
            */
            init            : function init ( selector, options, alias )
            {

            },

            /**
             * @method                                 Cream.Plugin.parent
             * @description                            Returns kit parent instance
             *
             * @returns {(undefined|Cream.Kit)}
             *
            */
            parent          : function getParent ()
            {
                return this.get( 'parent' );
            },

            /**
             * @method                                  Cream.Plugin.service
             * @description                             Return Cream.Service by name
             *
             * @param {string}              name        Service name
             *
             * @returns {(undefined|Cream.Service)}
             *
            */
            service         : function getService ( name )
            {
                return cream.get( name );
            },

            /**
             * @method                                  Cream.Plugin.publish
             * @description                             Trigger the event to the kit/plugin only
             *                                          over the actual plugin instance.
             *                                          If alias plugin is defined, this function
             *                                              find and trigger 'Plugin:alias' event in
             *                                              kit/plugin events.
             *                                          Else find and trigger 'Plugin' event
             *
             * @param {string}              evt         Event name
             * @param {object}              [opts]      Event options
             *
             * @returns {*}                             Result of the defined event function
             *
            */
            publish         : function publish ( evt, opts )
            {
                return cream.get( 'interface' )
                            .publish( this, evt, opts );
            },

            /**
             * @method                                  Cream.Plugin.publishAll
             * @description                             Find and trigger kit/plugin event by plugin
             *                                          name => 'Plugin', if alias is defined or not.
             *                                          Don't trigger events like 'Plugin:alias'
             *
             * @param {string}             evt          Event name
             * @param {object}             [opts]       Event options
             *
             * @returns {*}                             Result of the defined event function
             *
            */
            publishAll      : function publishAll ( evt, opts )
            {
                return cream.get( 'interface' )
                            .publishAll( this, evt, opts );
            },

            /**
             * @method                                  Cream.Plugin.destroy
             * @description                             Destroy plugin instance
             *
             * @returns {boolean}
             *
            */
            destroy         : function destroy ()
            {
                return cream.get( 'interface' )
                            .destroy( this );
            },

            /**
             * @method                                  Cream.Plugin.remove
             * @description                             Destroy instance and remove the element
             *                                          from the DOM
             *
             * @returns {boolean}
             *
            */
            remove          : function remove ()
            {
                return cream.get( 'interface' )
                            .remove( this );
            },

            /**
             * @method                                  Cream.Plugin.selectors
             * @description                             Return pre-cached dom element
             *
             * @returns {(undefined|object)}
             *
            */
            selectors       : function selectors ( name )
            {
                return this.get( 'elements' )[ name ];
            },

            /**
             * @method                                  Cream.Plugin.update
             * @description                             Reinitialize the plugin with actual
             *                                          parameters
             *
             * @returns {object}
             *
            */
            update          : function update ()
            {
                return cream.get( 'interface' )
                            .update( this );
            },

            /**
             * @method                                  Cream.Plugin.addWidget
             * @description                             Create a widget, the widget class  has
             *                                          to be preloaded
             *                                          (review widget constructor docs)
             *
             * @param {string}          name            Widget short name
             * @param {string}          target          Css tyle selector where initialize the widget
             * @param {object}          opts            Widget options
             * @param {string}          [alias]         Widget alias to identify it
             *
             * @returns {Cream.Widget}
             *
            */
            addWidget       : function addWidget ( name, target, opts, alias )
            {
                var options = cream.isUndefined( opts )
                                ? {}
                                : opts;

                options.parent = this;

                return cream.get( 'interface' )
                            .add( name, target, options, 'widget', alias );
            },

            /**
             * @method                                  Cream.Plugin.removeWidget
             * @description                             Returns widgets created by plugin
             *
             *
             * @param {string}          name            Widget short name
             * @param {string}          [alias]         Widget alias
             *
             * @returns {(Cream.Widget|Cream.Widget[])}
             *
            */
            removeWidget    : function removeWidget ( name, alias )
            {
                return cream.get( 'interface' )
                            .findAndRemove( this.attr.widget, name, alias );
            },

            /**
             * @method                                  Cream.Plugin.widgets
             * @description                             Returns widgets created by plugin
             *
             *
             * @param {string}          name            Widget short name
             * @param {string}          [alias]         Widget alias
             *
             * @returns {(Cream.Widget|Cream.Widget[])}
             *
            */
            widgets          : function widgets ( name, alias )
            {
                return cream.get( 'interface' )
                            .find( this.attr.widget, name, alias );
            }

        },

        /**
         * Catch DOM events for plugin
         *
         * @name Cream.Plugin.controls
         * @type {object}
        */
        {
            name                    : 'controls',

            '.cream-remove click'   : function creamRemove ()
            {
                this.remove();
            },

            '.cream-update click'   : function creamUpdate ()
            {
                this.update();
            }
        },

        /**
         * Catch widgets events
         *
         * @name Cream.Plugin.events,
         * @type {object}
         *
         *
        */
        {
            name: 'events'
        },

        /**
         * Catch services events
         *
         * @name Cream.Plugin.services
         * @type {object}
         *
         *
        */
        {
            name: 'services'
        }


    );

} );
