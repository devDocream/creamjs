define( [
    'framework',
    'class'
], function sandboxClass (
    cream,
    Class
)
{

    /**
     * @class           Cream.Sandbox
     * @parent          Cream
     * @inherits        Cream.Class
     * @augments        Cream.Class
     *
     * @classdesc       Cream Sandbox Constructor
     *
     * @author          Fernando Coca <fernando@doCream.com>
     *
     */
    return Class(

        'Cream.Sandbox',

        {
            cls         : '',

            kit         : {},

            /**
             * @name                    Cream.Kit.refresh
             * @description             Preconfigured subscribers to auto refresh collections
             *
             * @type {object}
             *
            */
            refresh     : {}

        },

        /**
         * @name                        Cream.Sandbox.self
         * @description                 Private methods
         *
         * @type {object}
         *
         */
        {

        },

        {
            /**
             * @name                                  Cream.Sandbox.element
             * @description                           Sandbox html root element
             *
             * @type {object}
             */
            element : {},

            /**
             * @name                                  Cream.Sandbox.alias
             * @description                           Sandbox alias
             *
             * @type {string}
             */
            alias   : '',

            /**
             * @name                                  Cream.Sandbox.selector
             * @description                           CSS style selector where the sandbox
             *                                        was initialized
             *
             * @type {string}
             */
            selector   : '',

            /**
             * @name                                  Cream.Sandbox.id
             * @description                           Sandbox id
             *
             * @type {integer}
             */
            id         : undefined,

            /**
             * @method                                 Cream.Sandbox.setup
             * @description                            Sandbox constructor.
             *                                         Don't override. If is necessary, remember
             *                                         call to this._super setup
             *
             * @param {object}      [options]           Options to create the sandbox
             *
             */
            setup           : function setup ( options )
            {

                var selector = 'body';

                if ( cream.isUndefined( options ) )
                {
                    options = {};
                }

                this.attr   = cream.extend( cream.extend( true, {}, this.attr ), options );
                this.alias  = 'sandbox';

                cream.get( 'interface' )
                    .create( this, selector, 'sandbox' );

                return this._super.apply( this, arguments );
            },

            /**
             * @method                                 Cream.Sandbox.init
             * @description                            Called after setup() finishes
             *
             * @param {object}      [options]          Options to create the sandbox
             *
             */
            init            : function init ( options )
            {

            },

            /**
             * @method                                  Cream.Sandbox.service
             * @description                             Return Cream.Service by name
             *
             * @param {string}              name        Service name
             *
             * @returns {(undefined|Cream.Service)}
             *
             */
            service         : function getService ( name )
            {
                return cream.get( name );
            },

            /**
             * @method                                  Cream.Sandbox.repository
             * @description                             Return Cream.Repository by name
             *
             * @param {string}              name        Repository name
             *
             * @returns {(undefined|Cream.Repository)}
             *
             */
            service         : function getRepository ( name )
            {
                return cream.get( name, 'repository' );
            },


            /**
             * @method                                  Cream.Sandbox.destroy
             * @description                             Destroy sandbox instance
             *
             * @returns {boolean}
             *
             */
            destroy         : function destroy ()
            {
                return cream.get( 'interface' )
                    .destroy( this );
            },

            /**
             * @method                                  Cream.Sandbox.remove
             * @description                             Destroy instance and remove the element
             *                                          from the DOM
             *
             * @returns {boolean}
             *
             */
            remove          : function remove ()
            {
                return cream.get( 'interface' )
                    .remove( this );
            },

            /**
             * @method                                  Cream.Sandbox.update
             * @description                             Reinitialize the sandbox with actual
             *                                          parameters
             *
             * @returns {object}
             *
             */
            update          : function update ()
            {
                return cream.get( 'interface' )
                    .update( this );
            },

            /**
             * @method                                  Cream.Sandbox.addKit
             * @description                             Create a kit, the kit class  has
             *                                          to be preloaded
             *                                          (review kit constructor docs)
             *
             * @param {string}          name            Kit short name
             * @param {string}          target          Css style selector where initialize the kit
             * @param {object}          opts            Kit options
             * @param {string}          [alias]         Kit alias to identify it
             *
             * @returns {Cream.Kit}
             *
             */
            addKit       : function addKit ( name, target, opts, alias )
            {
                var options = cream.isUndefined( opts )
                    ? {}
                    : opts;

                options.parent = this;

                return cream.get( 'interface' )
                    .add( name, target, options, 'kit', alias );
            },

            /**
             * @method                                  Cream.Sandbox.removeKit
             * @description                             Returns kits created by sandbox
             *
             *
             * @param {string}          name            Kit short name
             * @param {string}          [alias]         Kit alias
             *
             */
            removeKit    : function removeKit ( name, alias )
            {
                return cream.get( 'interface' )
                    .findAndRemove( this.attr.kit, name, alias );
            },

            /**
             * @method                                  Cream.Sandbox.kits
             * @description                             Returns kits created by sandbox
             *
             *
             * @param {string}          name            Kit short name
             * @param {string}          [alias]         Kit alias
             *
             * @returns {(Cream.Kit|Cream.Kit[])}
             *
             */
            kits          : function kits ( name, alias )
            {
                return cream.get( 'interface' )
                    .find( this.attr.widget, name, alias );
            },

            /**
             * @method                                  Cream.Sandbox.collection
             * @description                             Returns cached result of a collection
             *
             *
             * @param {string}          name            Collection name
             * @param {boolean}         [refresh=false] If true, force refresh collection before return
             * @param {object}          [options]       Object with options to pass collection function

             *
             * @returns {*}
             *
            */
            collection      : function collection ( name, refresh, options )
            {
                return cream.get( 'interface' )
                    .collection( this, name, refresh, options );
            },

            /**
             * @method                                  Cream.Sandbox.refreshCollection
             * @description                             Refresh collection cached result and returns
             *                                          it
             *
             *
             * @param {string}          name            Collection name
             * @param {object}          [options]       Object with options to pass collection function
             *
             * @returns {*}
             *
            */
            refreshCollection      : function refreshCollection ( name, options )
            {
                return cream.get( 'interface' )
                    .refreshCollection( this, name, options );
            }

        },

        /**
         * Catch kits events
         *
         * @name Cream.Sandbox.events
         * @type {object}
         *
         *
         */
        {
            name    : 'events'
        },

        /**
         * Catch services events
         *
         * @name Cream.Sandbox.services
         * @type {object}
         *
         *
         */
        {
            name    : 'services'
        },

        /**
         * Collections functions
         *
         * @name Cream.Sandbox.collections
         * @type {object}
         *
         *
         */
        {
            name    : 'collections'
        }


    );

} );
