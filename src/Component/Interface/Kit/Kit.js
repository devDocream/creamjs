define( [
    'framework',
    'class'
], function kitClass (
    cream,
    Class
)
{

    /**
     * @class           Cream.Kit
     * @parent          Cream
     * @inherits        Cream.Class
     * @augments        Cream.Class
     *
     * @classdesc       Cream Kit Constructor
     *
     * @author          Fernando Coca <fernando@doCream.com>
     *
     */
    return Class(

        'Cream.Kit',

        {

            /**
             * @name                    Cream.Kit.parent
             * @description             The parent sandbox
             *
             * @type {(object|Cream.Sandbox)}
             *
             */
            parent      : {},

            /**
             * @name                    Cream.Kit.view
             * @description             Pre-configured kit view
             *
             * @type {(string|View)}             View object
             *
             */
            view        : '',

            cls         : '',

            /**
             * @name                    Cream.Kit.place
             * @description             Place to initialize the kit:
             *                          - place.value: jquery element or selector to before and
             *                          after place-types;
             *                          - place.type: (over|before|after|prepend|append)
             * @type {object}
             *
             */
            place       : {},

            /**
             * @name                    Cream.Kit.selectors
             * @description             Pre-configured selectors to cache
             *
             * @type {object}
             *
             */
            selectors    : {},

            /**
             * @name                    Cream.Kit.elements
             * @description             Pre-cached element selectors from classes attr
             *
             * @type {object}
             *
             */
            elements    : {},

            widget      : {},

            plugin      : {},

            /**
             * @name                    Cream.Kit.refresh
             * @description             Pre-configured subscribers to auto refresh collections
             *
             * @type {object}
             *
            */
            refresh     : {}

        },

        /**
         * @name                        Cream.Kit.self
         * @description                 Private methods
         *
         * @type {object}
         *
         */
        {

        },

        {
            /**
             * @name                                  Cream.Kit.element
             * @description                           Kit html root element
             *
             * @type {object}
             */
            element : {},

            /**
             * @name                                  Cream.Kit.alias
             * @description                           Kit alias
             *
             * @type {string}
            */
            alias   : '',

            /**
             * @name                                  Cream.Kit.selector
             * @description                           CSS style selector where the kit
             *                                        was initialized
             *
             * @type {string}
            */
            selector   : '',

            /**
             * @name                                  Cream.Kit.id
             * @description                           Kit id
             *
             * @type {integer}
            */
            id         : undefined,

            /**
             * @method                                 Cream.Kit.setup
             * @description                            Kit constructor.
             *                                         Don't override. If is necessary, remember
             *                                         call to this._super setup
             *
             * @param {string}      selector           Target DOM selector
             * @param {object}      [options]            Options to create the kit
             * @param {string}      [alias]            Kit alias
             *
            */
            setup           : function setup ( selector, options, alias )
            {

                if ( cream.isUndefined( options ) )
                {
                    options = {};
                }

                this.attr   = cream.extend( cream.extend( true, {}, this.attr ), options );
                this.alias  = cream.notUndefined( alias ) ? alias : '';

                cream.get( 'interface' )
                    .create( this, selector, 'kit' );

                return this._super.apply( this, arguments );
            },

            /**
             * @method                                 Cream.Kit.init
             * @description                            Called after setup() finishes
             *
             * @param {string}      selector           Target DOM selector
             * @param {object}      [options]          Options to create the kit
             * @param {string}      [alias]            Kit alias
             *
            */
            init            : function init ( selector, options, alias )
            {

            },

            /**
             * @method                                 Cream.Kit.parent
             * @description                            Returns kit parent instance
             *
             * @returns {(undefined|Cream.Kit)}
             *
            */
            parent          : function getParent ()
            {
                return this.get( 'parent' );
            },

            /**
             * @method                                  Cream.Kit.service
             * @description                             Return Cream.Service by name
             *
             * @param {string}              name        Service name
             *
             * @returns {(undefined|Cream.Service)}
             *
            */
            service         : function getService ( name )
            {
                return cream.get( name );
            },

            /**
             * @method                                  Cream.Kit.publish
             * @description                             Trigger the event to the kit/kit only
             *                                          over the actual kit instance.
             *                                          If alias kit is defined, this function
             *                                              find and trigger 'Kit:alias' event in
             *                                              kit/kit events.
             *                                          Else find and trigger 'Kit' event
             *
             * @param {string}              evt         Event name
             * @param {object}             [opts]        Event options
             *
             * @returns {*}                             Result of the defined event function
             *
            */
            publish         : function publish ( evt, opts )
            {
                return cream.get( 'interface' )
                            .publish( this, evt, opts );
            },

            /**
             * @method                                  Cream.Kit.publishAll
             * @description                             Find and trigger kit/kit event by kit
             *                                          name => 'Kit', if alias is defined or not.
             *                                          Don't trigger events like 'Kit:alias'
             *
             * @param {string}             evt          Event name
             * @param {object}            [opts]        Event options
             *
             * @returns {*}                             Result of the defined event function
             *
            */
            publishAll      : function publishAll ( evt, opts )
            {
                return cream.get( 'interface' )
                            .publishAll( this, evt, opts );
            },

            /**
             * @method                                  Cream.Kit.destroy
             * @description                             Destroy kit instance
             *
             * @returns {boolean}
             *
            */
            destroy         : function destroy ()
            {
                return cream.get( 'interface' )
                            .destroy( this );
            },

            /**
             * @method                                  Cream.Kit.remove
             * @description                             Destroy instance and remove the element
             *                                          from the DOM
             *
             * @returns {boolean}
             *
            */
            remove          : function remove ()
            {
                return cream.get( 'interface' )
                            .remove( this );
            },

            /**
             * @method                                  Cream.Kit.selectors
             * @description                             Return pre-cached dom element
             *
             * @returns {(undefined|object)}
             *
            */
            selectors       : function selectors ( name )
            {
                return this.get( 'elements' )[ name ];
            },

            /**
             * @method                                  Cream.Kit.update
             * @description                             Reinitialize the kit with actual
             *                                          parameters
             *
             * @returns {object}
             *
            */
            update          : function update ()
            {
                return cream.get( 'interface' )
                            .update( this );
            },

            /**
             * @method                                  Cream.Kit.addWidget
             * @description                             Create a widget, the widget class  has
             *                                          to be preloaded
             *                                          (review widget constructor docs)
             *
             * @param {string}          name            Widget short name
             * @param {string}          target          Css tyle selector where initialize the widget
             * @param {object}          opts            Widget options
             * @param {string}          [alias]         Widget alias to identify it
             *
             * @returns {Cream.Widget}
             *
            */
            addWidget       : function addWidget ( name, target, opts, alias )
            {
                var options = cream.isUndefined( opts )
                    ? {}
                    : opts;

                options.parent = this;

                return cream.get( 'interface' )
                            .add( name, target, options, 'widget', alias );
            },

            /**
             * @method                                  Cream.Kit.removeWidget
             * @description                             Returns widgets created by kit
             *
             *
             * @param {string}          name            Widget short name
             * @param {string}          [alias]         Widget alias
             *
            */
            removeWidget    : function removeWidget ( name, alias )
            {
                return cream.get( 'interface' )
                            .findAndRemove( this.attr.widget, name, alias );
            },

            /**
             * @method                                  Cream.Kit.widgets
             * @description                             Returns widgets created by kit
             *
             *
             * @param {string}          name            Widget short name
             * @param {string}          [alias]         Widget alias
             *
             * @returns {(Cream.Widget|Cream.Widget[])}
             *
            */
            widgets          : function widgets ( name, alias )
            {
                return cream.get( 'interface' )
                            .find( this.attr.widget, name, alias );
            },

            /**
             * @method                                  Cream.Kit.addPlugin
             * @description                             Create a plugin, the plugin class  has
             *                                          to be preloaded
             *                                          (review plugin constructor docs)
             *
             * @param {string}          name            Plugin short name
             * @param {string}          target          Css style selector where initialize the plugin
             * @param {object}          opts            Plugin options
             * @param {string}          [alias]         Plugin alias to identify it
             *
             * @returns {Cream.Plugin}
             *
            */
            addPlugin       : function addPlugin ( name, target, opts, alias )
            {
                var options = cream.isUndefined( opts )
                    ? {}
                    : opts;

                options.parent = this;

                return cream.get( 'interface' )
                            .add( name, target, options, 'plugin', alias );
            },

            /**
             * @method                                  Cream.Kit.removePlugin
             * @description                             Returns plugins created by kit
             *
             *
             * @param {string}          name            Plugin short name
             * @param {string}          [alias]         Plugin alias
             *
             */
            removePlugin    : function removePlugin ( name, alias )
            {
                return cream.get( 'interface' )
                            .findAndRemove( this.attr.plugin, name, alias );
            },

            /**
             * @method                                  Cream.Kit.plugins
             * @description                             Returns plugins created by kit
             *
             *
             * @param {string}          name            Plugin short name
             * @param {string}          [alias]         Plugin alias
             *
             * @returns {(Cream.Plugin|Cream.Plugin[])}
             *
            */
            plugins          : function plugins ( name, alias )
            {
                return cream.get( 'interface' )
                            .find( this.attr.widget, name, alias );
            },

            /**
             * @method                                  Cream.Kit.collection
             * @description                             Returns cached result of a collection
             *
             *
             * @param {string}          name            Collection name
             * @param {boolean}         [refresh=false] If true, force refresh collection before return
             * @param {object}          [options]       Object with options to pass collection function

             *
             * @returns {*}
             *
            */
            collection      : function collection ( name, refresh, options )
            {
                return cream.get( 'interface' )
                            .collection( this, name, refresh, options );
            },

            /**
             * @method                                  Cream.Kit.refreshCollection
             * @description                             Refresh collection cached result and returns
             *                                          it
             *
             *
             * @param {string}          name            Collection name
             * @param {object}          [options]       Object with options to pass collection function
             *
             * @returns {*}
             *
            */
            refreshCollection      : function refreshCollection ( name, options )
            {
                return cream.get( 'interface' )
                            .refreshCollection( this, name, options );
            }

        },

        /**
         * Catch DOM events for kit
         *
         * @name Cream.Kit.controls
         * @type {object}
         */
        {
            name    : 'controls'
        },

        /**
         * Catch widgets and plugins events
         *
         * @name Cream.Kit.events
         * @type {object}
         *
         *
         */
        {
            name    : 'events'
        },

        /**
         * Catch services events
         *
         * @name Cream.Kit.services
         * @type {object}
         *
         *
         */
        {
            name    : 'services'
        },

        /**
         * Collections functions
         *
         * @name Cream.Kit.collections
         * @type {object}
         *
         *
         */
        {
            name    : 'collections'
        }


    );

} );
