define( [
    'framework',
    'class'
], function widget (
    cream,
    Class
)
{

    /**
     * @class           Cream.Widget
     * @parent          Cream
     * @inherits        Cream.Class
     * @augments        Cream.Class
     *
     * @classdesc       Cream Widget Constructor
     *
     * @author          Fernando Coca <fernando@doCream.com>
     *
    */
    return  Class(

        'Cream.Widget',

        {

            /**
             * @name                    Cream.Widget.parent
             * @description             The parent kit or plugin
             *
             * @type {(Cream.Kit|Cream.Plugin|object)}
             *
            */
            parent      : {},

            /**
             * @name                    Cream.Widget.view
             * @description             Pre-configured widget view
             *
             * @type {(string|View)}             View object
             *
            */
            view        : '',

            cls         : '',

            /**
             * @name                    Cream.Widget.place
             * @description             Place to initialize the widget:
             *                          - place.value: jquery element or selector to before and
             *                          after place-types;
             *                          - place.type: (over|before|after|prepend|append)
             * @type {object}
             *
            */
            place       : {},

            /**
             * @name                    Cream.Widget.selectors
             * @description             Pre-configured selectors to cache
             *
             * @type {object}
             *
            */
            selectors    : {},

            /**
             * @name                    Cream.Widget.elements
             * @description             Pre-cached element selectors from classes attr
             *
             * @type {object}
             *
            */
            elements    : {},

            entity      : false,

            field       : false,

            cache       : false

        },

        /**
         * @name                        Cream.Widget.self
         * @description                 Private methods
         *
         * @type {object}
         *
        */
        {

        },

        {
            /**
             * @name                                  Cream.Widget.element
             * @description                           Widget html root element
             *
             * @type {object}
             */
            element : {},

            /**
             * @name                                  Cream.Widget.alias
             * @description                           Widget alias
             *
             * @type {string}
            */
            alias   : '',

            /**
             * @name                                  Cream.Widget.selector
             * @description                           CSS style selector where the widget
             *                                        was initialized
             *
             * @type {string}
            */
            selector   : '',

            /**
             * @name                                  Cream.Widget.id
             * @description                           Widget id
             *
             * @type {integer}
             */
             id         : undefined,

            /**
             * @method                                 Cream.Widget.setup
             * @description                            Widget constructor.
             *                                         Don't override. If is necessary, remember
             *                                         call to this._super setup
             *
             * @param {string}      selector           Target DOM selector
             * @param {object}      [options]          Options to create the widget
             * @param {string}      [alias]            Widget alias
             *
            */
            setup       : function setup ( selector, options, alias )
            {
                if ( cream.isUndefined( options ) )
                {
                    options = {};
                }

                this.attr   = cream.extend( cream.extend( true, {}, this.attr ), options );
                this.alias  = cream.notUndefined( alias ) ? alias : '';

                cream.get( 'interface' )
                    .create( this, selector, 'widget' );

                return this._super.apply( this, arguments );
            },

            /**
             * @method                                 Cream.Widget.init
             * @description                            Called after setup() finishes
             *
             * @param {string}      selector           Target DOM selector
             * @param {object}      [options]          Options to create the widget
             * @param {string}      [alias]            Widget alias
             *
            */
            init        : function init ( selector, options, alias )
            {

            },

            /**
             * @method                                 Cream.Widget.parent
             * @description                            Returns widget's parent Interface instance
             *
             * @returns {(undefined|Cream.Kit|Cream.Plugin)}
             *
            */
            parent      : function getParent ()
            {
                return this.get( 'parent' );
            },

            /**
             * @method                                  Cream.Widget.service
             * @description                             Return Cream.Service by name
             *
             * @param {string}              name        Service name
             *
             * @returns {(undefined|Cream.Service)}
             *
            */
            service     : function getService ( name )
            {
                return cream.get( name );
            },

            /**
             * @method                                  Cream.Widget.publish
             * @description                             Trigger the event to the kit/plugin only
             *                                          over the actual widget instance.
             *                                          If alias widget is defined, this function
             *                                              find and trigger 'Widget:alias' event in
             *                                              kit/plugin events.
             *                                          Else find and trigger 'Widget' event
             *
             * @param {string}              evt         Event name
             * @param {object}  [opts]      Event options
             *
             * @returns {*}                             Result of the defined event function
             *
            */
            publish     : function publish ( evt, opts )
            {
                return cream.get( 'interface' ).publish( this, evt, opts );
            },

            /**
             * @method                                  Cream.Widget.publishAll
             * @description                             Find and trigger kit/plugin event by widget
             *                                          name => 'Widget', if alias is defined or not.
             *                                          Don't trigger events like 'Widget:alias'
             *
             * @param {string}             evt          Event name
             * @param {object}             [opts]       Event options
             *
             * @returns {*}                             Result of the defined event function
             *
            */
            publishAll  : function publishAll ( evt, opts )
            {
                return cream.get( 'interface' ).publishAll( this, evt, opts );
            },

            /**
             * @method                                  Cream.Widget.destroy
             * @description                             Destroy widget instance
             *
             * @returns {boolean}
             *
            */
            destroy     : function destroy ()
            {
                return cream.get( 'interface' ).destroy( this );
            },

            /**
             * @method                                  Cream.Widget.remove
             * @description                             Destroy instance and remove the element
             *                                          from the DOM
             *
             * @returns {boolean}
             *
            */
            remove      : function remove ()
            {
                return cream.get( 'interface' ).remove( this );
            },

            /**
             * @method                                  Cream.Widget.selectors
             * @description                             Return pre-cached dom element
             *
             * @returns {(undefined|object)}
             *
            */
            selectors   : function selectors ( name )
            {
                return this.get( 'elements' )[ name ];
            },

            /**
             * @method                                  Cream.Widget.update
             * @description                             Reinitialize the widget with actual parameters
             *
             * @returns {object}
             *
            */
            update      : function update ()
            {
                return cream.get( 'interface' ).update( this );
            }


        },

        /**
         * Catch DOM events for widget
         *
         * @name Cream.Widget.controls
         * @type {object}
        */
        {
            name                    : 'controls',

            '.cream-remove click'   : function creamRemove ()
            {
                this.remove();
            },

            '.cream-update click'   : function creamUpdate ()
            {
                this.update();
            }
        },

        /**
         * Catch services events
         *
         * @name Cream.Widget.services
         * @type {object}
         *
         *
        */
        {
            name: 'services'
        }


    );

} );
