define( [
        'framework',
        'class'
],
function creamService ( cream, Class )
{

    /**
     * @class           Cream.Service
     * @parent          Cream
     * @inherits        Cream.Class
     * @augments        Cream.Class
     *
     * @classdesc       Cream Service Constructor
     *
     * @author          Fernando Coca <fernando@doCream.com>
     *
    */
    Class(
        'Cream.Service',
        {

        },
        {

        },
        {

            setup       : function setup ( options )
            {
                if ( cream.isUndefined( options ) )
                {
                    options = {};
                }

                this.attr   = cream.extend( cream.extend( true, {}, this.attr ), options );

                return this._super.apply( this, arguments );
            },

            /**
             * @method                                  Cream.Service.publish
             * @description                             Trigger the event to all instances subscribed
             *                                          to the event
             *
             * @param {string}              evt         Event name
             * @param {(object|undefined)}  opts        Event options
             *
             * @returns {*}                             Result of the defined event function
             *
            */
            publish     : function publish ( evt, opts )
            {
                return cream.get( 'interface' ).publish( this, evt, opts );
            }
        }
    );

    return Cream.Service;
}
);
