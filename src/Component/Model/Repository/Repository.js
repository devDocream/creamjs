define( [
    'framework',
    'class'
],
function creamRepository ( cream, Class )
{

    /**
     * @class           Cream.Repository
     * @parent          Cream
     * @inherits        Cream.Class
     * @augments        Cream.Class
     *
     * @classdesc       Cream Repository Constructor
     *
     * @author          Fernando Coca <fernando@doCream.com>
     *
     */
    Class(
        'Cream.Repository',
        {

        },
        {

        },
        {

            setup       : function setup ( options )
            {
                if ( cream.isUndefined( options ) )
                {
                    options = {};
                }

                this.attr   = cream.extend( cream.extend( true, {}, this.attr ), options );

                return this._super.apply( this, arguments );
            },

            /**
             * @method                                      Cream.Repository.PUT
             * @description                                 Ajax request method PUT
             *
             * @param {string}      url                     Query url
             * @param {string}      parameters              Query parameters
             * @param {string}      ajaxConfig              Query config, ajax service
             *
             * @returns {(Cream.Entity|array|undefined)}
             *
            */
            PUT         : function PUT ( url, parameters, ajaxConfig )
            {
                return cream.get( 'ajax' )
                            .put( url , parameters, ajaxConfig );
            },

            /**
             * @method                                      Cream.Repository.GET
             * @description                                 Ajax request method GET
             *
             * @param {string}      url                     Query url
             * @param {string}      parameters              Query parameters
             * @param {string}      ajaxConfig              Query config, ajax service
             *
             * @returns {(Cream.Entity|array|undefined)}
             *
            */
            GET         : function GET ( url, parameters, ajaxConfig  )
            {
                return cream.get( 'ajax' )
                            .get( url , parameters, ajaxConfig );
            },

            /**
             * @method                                      Cream.Repository.POST
             * @description                                 Ajax request method POST
             *
             * @param {string}      url                     Query url
             * @param {string}      parameters              Query parameters
             * @param {string}      ajaxConfig              Query config, ajax service
             *
             * @returns {(Cream.Entity|array|undefined)}
             *
            */
            POST        : function POST ( url, parameters, ajaxConfig )
            {
                return cream.get( 'ajax' )
                            .post( url , parameters, ajaxConfig );
            },

            /**
             * @method                                      Cream.Repository.DELETE
             * @description                                 Ajax request method DELETE
             *
             * @param {string}      url                     Query url
             * @param {string}      parameters              Query parameters
             * @param {string}      ajaxConfig              Query config, ajax service
             *
             * @returns {(Cream.Entity|array|undefined)}
             *
            */
            DELETE: function( url , parameters, ajaxConfig )
            {
                return cream.get( 'ajax' )
                            .remove( url, parameters, ajaxConfig );
            }
        }
    );

    return Cream.Repository;
}
);
