define( [
    'jquery'
],
function jquery ( $ )
{
    /**
     * @class cream.Adapter.Library.jQuery
     * @classdesc Creamjs jQuery Adapter
     *
     * @author Fernando Coca <fernando@docream.com>
     *
     */

    window.cream = {
        extend   : $.extend,
        when     : $.when,
        trim     : $.trim,
        Deferred : $.Deferred,
        proxy    : $.proxy,
        $        : $,
        param    : $.param

    };

    return window.cream;
}
);
