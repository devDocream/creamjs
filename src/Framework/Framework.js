define( [
    'util/cream.util!'
],
function framework ( cream )
{

    /**
     * @namespace Cream.Framework
     * @global
     *
     * @author Fernando Coca <fernando@docream.com>
     *
    */
    return cream;
}
);
