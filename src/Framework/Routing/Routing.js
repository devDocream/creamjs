define(
function routing( )
{
    /**
     * @class           Cream.Framework.Routing
     *
     * @parent          Cream.Framework
     * @augments        Cream.Class
     *
     * @classdesc       Find right route over actual location url from defined routes
     *
     * @author          Fernando Coca <fernando@docream.com>
     *
    */
    var Routing =
    {
        /**
         * @name                                        Cream.Framework.Routing.routes
         * @description                                 Application routes
         *                                              [ '/cream/{id}' => 'vendor:bunch:sandbox']
         *
         * @type {object}
        */
        routes: {},

        /**
         * @name                                        Cream.Framework.Routing.path
         * @description                                 Actual path
         *
         * @type {string}
         */
        path: '',


        /**
         * @typedef                                     RouteInfo
         * @description                                 Route info object
         *
         * @type    {Object}
         *
         * @property {object}       path                Part of actual url to match (/cream)
         * @property {object}       route               Matched route name (/cream/{id})
         * @property {object}       value               Sandbox path (vendor:bunch:sandbox)
         * @property {object}       params              Obtained params from url ( id  ->  0)
         * @property {string}       hash                URL hash
         * @property {string}       url                 Actual url
         *
        */


        /**
         * @method                                      Cream.Framework.Routing._generateData
         * @description                                 Wraps actual route info
         *
         * @param {string}          route               Matched route path
         * @param {string}          value               Sandbox path
         * @param {object}          params              Found params
         * @param{string}           hash                URL hash
         * @param {string}          url                 Actual url
         * @param {object}          vars                Found vars in url
         *
         * @returns {RouteInfo}                         Wrapper of actual route info
         * @private
        */
        _generateData   : function _generateData ( route, value, params, url,  hash, vars  )
        {
            var url         = window.location.href,
                splitParam  = url.split( '?' ),
                splitHash   = splitParam[0].split( '#' );

            return {
                route   : route,
                path    : this.path,
                value   : value,
                params  : params,
                hash    : hash,
                vars    : vars,
                url     : splitHash[0].replace( route, '' )
            };
        },

        /**
         * @method                                      Cream.Framework.Routing._getParamName
         * @description                                 Get route parameter name for given part of url
         *
         * @param   {string}        value               Sandbox long name (vendor:bunch:sandbox)
         *
         * @returns {string}                            Route param name
         *
         * @private
        */
        _getParamName   : function _getParamName ( value )
        {
            return value.replace( '{', '' )
                        .replace( '}', '' );
        },

        /**
         * @method                                      Cream.Framework.Routing.realPath
         * @description                                 Remove base path from url
         *
         * @param   {string}        [url]               Force actual location url
         *
         * @returns {string}
         *
        */
        realPath        : function path ( url )
        {
            var pathAux  = url || window.location.href;

            return '/' + pathAux.replace( System.baseURL, '' );
        },

        /**
         * @method                                      Cream.Framework.Routing.getPath
         * @description                                 Actual location path to find (/cream)
         *
         * @param   {string}        path
         *
         * @returns {string}
         *
        */
        getPath            : function getPath( path )
        {
            var p = System.baseURL + path;

            return p.replace( '/' + path, path );
        },

        /**
         * @method                                      Cream.Framework.Routing.match
         * @description                                 Get route and parameters (from configured
         *                                              routes) for actual url (given by Router
         *                                              service)
         *
         * @returns {(RouteInfo|{})}                    Matched route for actual path
         */
        match           : function match ( )
        {
            var routes          = this.routes,
                keys            = Object.keys( routes ),
                nRoutes         = keys.length,
                path            = this.path,
                splitParams     = path.split( '?' ),
                splitHash       = splitParams[0].split( '#' ),
                splitPath       = splitHash[0].split( '/' ),
                result          = {},
                i = 0, route,
                splitRoute, length, iOf,
                count, params, found, routeFound, removeBrac, nSplit;

            if ( window.System.Cream === undefined )
            {
                window.System.Cream = {};
            }

            routeFound = false;

            while ( !routeFound && i < nRoutes )
            {
                route      = keys[ i ];
                splitRoute = route.split( '/' );
                length     = splitRoute.length;
                iOf        = -1;
                count      = 0;
                params     = {};
                found      = false;

                removeBrac = '';

                while ( !found  && count < length )
                {
                    iOf = splitRoute[count].indexOf( '{' );

                    if ( splitPath[count] !== undefined &&
                         splitRoute[ count ] !== splitPath[ count ] )
                    {
                        if ( iOf > -1 )
                        {
                            removeBrac  = this._getParamName( splitRoute[ count ] );
                            nSplit      = removeBrac.split( ':' );

                            params[ nSplit[ 0 ] ] = splitPath[ count ];

                            if ( count === length - 1 && length === splitPath.length )
                            {
                                routeFound = true;
                            }

                            count = count + 1;
                        }
                        else
                        {
                            found = true;
                        }
                    }
                    else
                    {
                        if ( splitPath[count] === undefined )
                        {
                            if ( iOf > -1 )
                            {
                                removeBrac = this._getParamName( splitRoute[ count ] );
                                nSplit = removeBrac.split( ':' );
                                params[ nSplit[ 0 ] ] = nSplit[ 1 ];
                            }
                            else
                            {
                                found = true;
                            }
                        }
                        if ( count === length - 1 && length === splitPath.length )
                        {
                            routeFound = true;
                        }

                        count = count + 1;
                    }
                }


                i++;
            }

            if ( routeFound === true )
            {
                window.System.Cream.route =  this._generateData(
                    route,
                    routes[route],
                    splitParams[1],
                    splitHash[0],
                    splitHash[1],
                    params );

                return window.System.Cream.route;
            }
            window.System.Cream.route = result;

            return result;
        },

        /**
         * @method                                      Cream.Framework.Routing.setRoutes
         * @description                                 Set app configured routes
         *
         * @param   {object}        routes              Defined routes
         * @returns {Cream.Framework.Routing}
        */
        setRoutes       : function setRoutes ( routes )
        {
            this.routes = routes;

            return this;
        },

        /**
         * @method Cream.Framework.Routing.setPath
         * @description                                 Set  path to match
         *
         * @param   {string}        path                The path to find in routes (/cream)
         *
         * @returns {Cream.Framework.Routing}
        */
        setPath         : function setPath ( path )
        {
            this.path =  path;

            return this;
        }
    };


    return Routing;

} );
