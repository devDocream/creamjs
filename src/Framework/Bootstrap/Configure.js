require( 'vendor/creamjs/src/Framework/Bootstrap/cream.bunch!' );

var compiler = {

    extendConfigs       : function configs ( d, c )
    {
        var config          = {};

        config.lib          = this.extendLib( d.lib, c.lib );

        config.utils        = this.extendGeneral( d.utils, c.utils );
        config.sandbox      = this.extendSandbox( d.sandbox, c.sandbox );
        config.services     = this.extendServices( d.services, c.services );
        config.repositories = this.extendGeneral( d.repositories, c.repositories );

        return config;
    },

    extend              : function extend ( defaults, options )
    {
        var extended = {}, prop;

        for ( prop in defaults )
        {
            if ( Object.prototype.hasOwnProperty.call( defaults, prop ) )
            {
                extended[prop] = defaults[prop];
            }
        }

        for ( prop in options )
        {
            if ( Object.prototype.hasOwnProperty.call( options, prop ) )
            {
                extended[prop] = options[prop];
            }
        }
        return extended;
    },

    extendLib           : function lib ( f, s )
    {
        var libraries = System.defaults.libraries;

        return s !== undefined && libraries.indexOf( s ) > -1
            ? s
            : f;
    },

    extendServices      : function services ( f, s )
    {
        var service =  this.extend( f, s );
        if ( s === undefined )
        {
            s = {};
        }
        service.View       = this.extendView( f.View, s.View );
        service.Validator  = this.extendValidator( f.Validator, s.Validator );
        service.Router     = this.extendGeneral( f.Router, s.Router );
        service.Translator = this.extendGeneral( f.Translator, s.Translator );
        service.Asset      = this.extendGeneral( f.Asset, s.Asset );

        return service;
    },

    extendGeneral       : function general ( f, s )
    {
        if ( s === false )
        {
            return false;
        }

        if ( s !== undefined && s !== false )
        {
            return this.extend( f, s );
        }

        return f;
    },

    extendView          : function view ( f, s )
    {
        var viewConfig = {};

        if ( s === false )
        {
            return false;
        }

        if ( s !== undefined && s !== false )
        {
            viewConfig          = this.extend( f, s );
            viewConfig.helpers  = f.helpers;
            viewConfig.filters  = f.filters;

            if ( s.helpers !== undefined  )
            {
                viewConfig.helpers =  this.extend( f.helpers, s.helpers );
            }

            if ( s.filters !== undefined  )
            {
                viewConfig.filters =  this.extend( f.filters, s.filters );
            }

            return viewConfig;
        }

        return f;
    },

    extendValidator     : function validator ( f, s )
    {
        var validatorConfig = {};

        if ( s === false )
        {
            return false;
        }

        s           = s === undefined ? {} : s;
        f           = f === undefined ? {} : f;
        f.asserts   = f.asserts === undefined ? {} : f.asserts;
        f.types     = f.types === undefined ? {} : f.types;
        s.asserts   = s.asserts === undefined ? {} : s.asserts;
        f.types     = f.types === undefined ? {} : f.types;

        if ( s !== undefined && s !== false )
        {
            validatorConfig         = this.extend( f, s );
            validatorConfig.asserts = f.asserts;
            validatorConfig.types   = f.types;

            if ( s.asserts !== undefined  )
            {
                validatorConfig.asserts =  this.extend( f.asserts, s.asserts );
            }

            if ( s.types !== undefined  )
            {
                validatorConfig.types   =  this.extend( f.types , s.types );
            }
            return validatorConfig;
        }

        return f;

    },

    extendSandbox       : function sb ( f, s )
    {
        var sbData = {};

        if ( s !== undefined && s.data !== undefined )
        {
            f = f !== undefined && f.data !== undefined ? f : { data: {} };

            sbData.data = this.extend( f.data, s.data );
            return sbData;
        }

        if ( f !== undefined && f.data !== undefined )
        {
            return f;
        }

        return { data : {} };
    },

    _initDDSpaces       : function initDDSpaces ( type, name, alias )
    {
        if ( System.Cream.DD[ type ] === undefined )
        {
            System.Cream.DD[ type ] = {};
        }

        if ( System.Cream.DD[ type ][ name ] === undefined )
        {
            System.Cream.DD[ type ][ name ] = {};
            System.Cream.DD[ type ][ name ].deps = {};
        }

        if ( alias !== '' && System.Cream.DD[ type ][ name ][ alias ] === undefined )
        {
            System.Cream.DD[ type ][ name ][ alias ] = {};
        }
    },

    dataDependency      : function dataDependency ( sandboxData )
    {
        var keys    = Object.keys( sandboxData ),
            length  = keys.length,
            length2, j, dependencies,
            key, i, names, name, parts, attr, type, alias;

        if ( System.Cream.DD === undefined )
        {
            System.Cream.DD = {};
        }

        for ( i = 0; i < length; i++ )
        {
            key             = keys[ i ];
            dependencies    = typeof sandboxData[ key ] === 'string'
                            ? [sandboxData[ key ]]
                            : sandboxData[ key ];
            length2         = dependencies.length;

            for ( j = 0; j < length2; j++ )
            {
                parts    = dependencies[ j ].split( '.' );
                names    = typeof parts !== 'string' ? parts[0].split( ':' ) : parts.split( ':' );

                if ( names.length > 1 )
                {
                    attr     = parts.length === 2 ? parts[ 1 ] : '';
                    type     = names[0].toLowerCase();
                    name     = names[1].toLowerCase();
                    alias    = names.length === 3 ? names[2] : '';

                    this._initDDSpaces( type, name, alias );

                    if ( alias !== '' && attr !== '' )
                    {
                        System.Cream.DD[ type ][ name][ alias ][ attr ] = keys[ i ];
                    }
                    if ( alias === '' && attr !== '' )
                    {
                        System.Cream.DD[ type ][ name ].deps[ attr ] = keys[ i ];
                    }
                    if ( alias === '' && attr === '' )
                    {
                        System.Cream.DD[ type ][ name ] = keys[ i ];
                    }
                }

            }
        }

    },

    loadData            : function loadData ()
    {
        var route       = System.Cream.route,
            route       = route !== undefined ? route.value : {},
            load        = route !== undefined ? route.load : {},
            url         = load !== undefined && load.url !== undefined ? load.url : '',
            callback    = load !== undefined && load.callback !== undefined ? load.callback : '',
            request, data;

        if ( url !== '' )
        {
            url = url.indexOf( 'http' ) === -1 ?  url : System.baseURL + url;

            request = new XMLHttpRequest();
            request.open( 'GET', System.baseURL + url, false );
            request.send( null );

            if ( request.status === 200 ) {
                try
                {
                   data = JSON.parse( request.response );

                }
                catch( e )
                {
                    return e.message;
                }

                System.Cream.data = data;
                if ( typeof callback === 'function' )
                {
                    callback.apply( cream, [ System.Cream.data ] );
                }
            }
        }

    },

    compile             : function compile ()
    {
        var vendor   = System.Cream.vendor,
            bunch    = System.Cream.bunch,
            extended = {},
            keys, length, i, key;

        if ( bunch !== undefined )
        {
            extended = bunch.Cream !== undefined ? bunch.Cream : {};

            if ( bunch[ bunch.name ] !== undefined )
            {
                extended =  this.extendConfigs( extended, bunch[ bunch.name ] );
            }
        }

        if ( vendor !== undefined && vendor.Cream !== undefined  )
        {
            extended =  this.extendConfigs( vendor.Cream, extended );
        }

        extended =  this.extendConfigs( System.Cream, extended );
        extended =  this.extendConfigs( System.defaults, extended );

        this.dataDependency( extended.sandbox.data );
        this.loadData();

        keys     = Object.keys( extended );
        length   = keys.length;

        for ( i = 0; i < length; i++ )
        {
            key                 = keys[ i ];
            System.Cream[ key ] = extended[ key ];
        }

        return extended;
    }

};

compiler.compile();
