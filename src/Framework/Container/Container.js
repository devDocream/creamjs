define( [
    'framework',
    'class'
], function container ( cream, Class )
{
    Class(
        'Cream.Container',
        {
            sandbox : {},
            widget  : {},
            kit     : {},
            plugin  : {},
            service : {},
            classes : {
                widget : {}
            }
        },
        {
            add         : function add ( instance, type )
            {
                var idAlias = cream.notBlank( instance.alias )
                                ? instance.alias
                                : instance.id;

                if ( cream.notUndefined( idAlias ) )
                {
                    if ( cream.isUndefined( this.attr[ type ] ) )
                    {
                        this.attr[ type ] = {};
                    }


                    if ( cream.isUndefined( this.attr[ type ][ instance._shortName ] ) )
                    {
                        this.attr[ type ][ instance._shortName ] = [];
                    }

                    this.attr[ type ][ instance._shortName ][ idAlias ] = instance;

                    if ( cream.notUndefined( instance.attr.parent ) &&
                        cream.notUndefined( instance.attr.parent.id ) )
                    {
                        if ( cream.isUndefined( instance.attr.parent.attr[ type ][ instance._shortName ] ) )
                        {
                            instance.attr.parent.attr[ type ][ instance._shortName ] = [];
                        }

                        instance.attr.parent.attr[ type ][ instance._shortName ][ idAlias ] = instance;
                    }
                    return instance;
                }

                this.attr[ type ][ instance._shortName ] = instance;

                if ( cream.notUndefined( instance.attr.parent ) &&
                     cream.notUndefined( instance.attr.parent.id ) )
                {
                    instance.attr.parent.attr[ type ][ instance._shortName ] = instance;
                }
                return instance;
            },

            get         : function get ( name, type, alias )
            {
                var objectToFind = alias === 'classes'
                                    ? this.attr.classes
                                    : this.attr;

                if ( cream.notUndefined( objectToFind[ type ] ) )
                {
                    if ( cream.notUndefined( objectToFind[ type ][ name ] ) )
                    {
                        return cream.notUndefined( alias ) && alias !== 'classes'
                            ? objectToFind[ type ][ name ][ alias ]
                            : objectToFind[ type ][ name ];
                    }
                }

                return;
            },

            remove      : function remove ( instance )
            {
                var name            = instance._shortName,
                    type            = instance._type.replace( 'cream-', '' ),
                    removeFromParent = false;

                if ( cream.notUndefined( this.attr[ type ] ) )
                {
                    if ( cream.notUndefined( this.attr[ type ][ name ] ) )
                    {
                        if ( cream.notUndefined( instance.attr.parent ) &&
                             cream.notUndefined( instance.attr.parent.id ) )
                        {
                            removeFromParent = true;
                        }

                        if ( cream.notBlank( instance.alias ) )
                        {
                           delete this.attr[ type ][ name ][ instance.alias ];

                            if ( removeFromParent )
                            {
                                delete  instance.attr.parent.attr[ type ][ name ][ instance.alias ];
                            }
                        }
                        else
                        {
                            delete this.attr[ type ][ name ][ instance.id ];
                            if ( removeFromParent )
                            {
                                delete  instance.attr.parent.attr[ type ][ name ][ instance.id ];
                            }
                        }
                    }
                }
            },

            addClass    : function addClass ( name, type, clas )
            {
                var parts     =  name.split( '.' ),
                    shortName = parts[ parts.length - 1 ];


                if ( cream.isUndefined( this.attr.classes[ type ] ) )
                {
                    this.attr.classes[ type ] = {};
                }

                this.attr.classes[ type ][ shortName ] = clas;
            }
        },
        {

            service     : function service ( instance )
            {
                if ( cream.isString( instance ) )
                {
                    return this.self.get( instance, 'service' );
                }

                this.self.add( instance, 'service' );
            },

            repository  : function service ( instance )
            {
                if ( cream.isString( instance ) )
                {
                    return this.self.get( instance, 'repository' );
                }

                this.self.add( instance, 'repository' );
            },


            get         : function get ( name, type, alias )
            {

                return this.self.get( name, type, alias );

            },

            interfaces  : function interfaces ( name, type, clas )
            {
                this.self.addClass( name, type, clas );
            },

            add         : function add ( instance, type )
            {
                this.self.add( instance, type );
            },

            remove      : function remove ( instance )
            {
                this.self.remove( instance );
            }
        }
    );

    System.Cream.Container = new Cream.Container();

    return System.Cream.Container;
} );
