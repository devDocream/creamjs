define( [
        'util/String'
],
function objUtil ( StringUtil )
{
    var objects =
        {
            /**
             * @method                              cream.keys
             * @description                         Get array with all object keys
             *
             * @param   {object} obj                Object for obtain keys
             *
             * @returns {array}
             *
            */
            keys            : function keys ( obj )
            {
                if ( obj !== undefined && obj !== null )
                {
                    return  Object.keys( obj );
                }

                return [];
            },

            /**
             * @method                              cream.keysLength
             * @description                         Gets the number of keys of an object
             *
             * @param   {object} obj                Object for obtain keys
             *
             * @returns {integer}
             *
            */
            keysLength      : function keysLength ( obj )
            {
                var keys = this.keys( obj );
                return keys.length;
            },

            /**
             * @method                              cream.getGlobal
             * @description                         Returns and/or create a window object
             *
             * @param   {string} str                Nested object name => 'Example', 'Example.Hello'
             * @param   {mixed}  [put={}]           Value for assign to the object
             *
             * @returns {object}                    => window.Example, window.Example.Hello
             *
            */
            getGlobal       : function getGlobal ( str, put )
            {

                if ( typeof str === 'string' && str !== '' )
                {
                    var parts = str.split( '.' ),
                        aux = window,
                        pl = parts.length,
                        i;

                    put = put === undefined ? {} : put;

                    for ( i = 0; i < pl; i++ )
                    {

                        if ( typeof aux[parts[ i ]] === 'undefined' )
                        {
                            aux[ parts [ i ] ] = i === pl - 1 ? put : {};
                        }

                        aux  =  aux[ parts [  i ] ];
                    }

                    return  put !== undefined  ? put : aux ;
                }

                return {};
            },

            /**
             * @method                              cream.getClassName
             * @description                         Generate css style class name => example-hello
             *
             * @param   {string} str                Class name => 'Example', 'Example.Hello'
             *
             * @returns {object}
             *
            */
            getClassName    : function getClassName ( str )
            {
                var g;

                if ( typeof str === 'string' && str !== '' )
                {
                    g =  StringUtil.replace( str, '.', '-' );
                    g = g.toLowerCase();
                    return g;
                }

                return '';
            }
        };

    return objects;

}
);
