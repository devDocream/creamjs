define( [
        'util/Date'
],
function Asserts ( date )
{


    var _upperLower =  function upperLower ( input, type )
    {
        var length      = input !== undefined && input !== null ? input.length : 0,
            i           = 0,
            character   = '',
            err         = false;

        if ( length > 0 && input !== undefined  && input !== null && typeof  input === 'string' )
        {
            while ( i <= length && err === false )
            {
                character = input.charAt( i );

                if ( typeof character === 'string' )
                {
                    if ( type === 'upper' && character !== character.toUpperCase()
                         || type === 'lower' && character !== character.toLowerCase() )
                    {
                        err = true;
                    }
                }
                else
                {
                    err = true;
                }
                i++;
            }

            if ( err === false )
            {
                return true;
            }

        }

        return false;
    },

    assertions =
    {
        /**
         * @method                              cream.isBlank
         * @description                         Check if the input is a string and is empty
         *
         * @param   {*}     input               Input to check
         *
         * @returns {boolean}
         *
         */
        isBlank             : function isBlank ( input )
        {
            return this.isString( input ) && input === '';
        },

        /**
         * @method                              cream.notBlank
         * @description                         Check if the input is a string and is not empty
         *
         * @param   {*}     input               Input to check
         *
         * @returns {boolean}
         *
        */
        notBlank            : function notBlank ( input )
        {
            return this.isString( input ) && input !== '';
        },

        /**
         * @method                              cream.isNull
         * @description                         Check if the input is null
         *
         * @param   {*}     input               Input to check
         *
         * @returns {boolean}
         *
        */
        isNull              : function isNull ( input )
        {
            //check undefined && null with null
            return  input === null;
        },

        /**
         * @method                              cream.notNull
         * @description                         Check if the input is not null
         *
         * @param   {*}     input               Input to check
         *
         * @returns {boolean}
         *
        */
        notNull             : function notNull ( input )
        {
            //check undefined && null with null
            return  input !== null;
        },

        /**
         * @method                              cream.isFalse
         * @description                         Check if the input is a boolean and is false
         *
         * @param   {*}     input               Input to check
         *
         * @returns {boolean}
         *
        */
        isFalse             : function isFalse ( input )
        {
            var isBool = this.isBoolean( input );

            if ( isBool === false )
            {
                input = input === 'true'
                    ? true
                    : input === 'false'
                            ? false
                            : input;
            }

            return input === false;
        },

        /**
         * @method                              cream.isTrue
         * @description                         Check if the input is a boolean and is true
         *
         * @param   {*}     input               Input to check
         *
         * @returns {boolean}
         *
        */
        isTrue              : function isTrue ( input )
        {
            var isBool = this.isBoolean( input );

            if ( isBool === false )
            {
                input = input === 'true'
                    ? true
                    : input === 'false'
                            ? false
                            : input;
            }

            return input === true;
        },

        /**
         * @method                              cream.isNumber
         * @description                         Check if the input is a integer or a float
         *
         * @param   {*}     input               Input to check
         *
         * @returns {boolean}
         *
        */
        isNumber            : function isNumber ( input )
        {
            return typeof input === 'number'  && input !== null;
        },

        /**
         * @method                              cream.isString
         * @description                         Check if the input is a string
         *
         * @param   {*}     input               Input to check
         *
         * @returns {boolean}
         *
         */
        isString            : function isString ( input )
        {
            return typeof input === 'string' && input !== null;
        },

        /**
         * @method                              cream.isInteger
         * @description                         Check if the input is a integer or a integer string
         *                                      representation
         *
         * @param   {*}     input               Input to check
         * @param   {boolean} [strict]          Force type to integer if true
         *
         * @returns {boolean}
         *
         */
        isInteger           : function isInteger1 ( input, strict )
        {
            var isNum  = this.isNumber( input ),
                isBool = this.isBoolean( input );

            if ( strict === true )
            {
                if ( isNum === true && ( input + '' ).indexOf( '.' ) === -1 )
                {
                    return true;
                }

                return false;
            }

            input = isNum === false  && this.isString( input )
                      && input.indexOf( '.' ) === -1
                            ? parseInt( input )
                            : input;

            return isBool
                        ? false
                        : Math.floor( input ) === input;
        },

        /**
         * @method                              cream.isDecimal
         * @description                         Check if the input is a string representation of a
         *                                      decimal number => 143,23
         *
         * @param   {*}     input               Input to check
         *
         * @returns {boolean}
         *
        */
        isDecimal           : function isDecimal ( input )
        {
            var parts = this.isString( input )
                            ? input.split( ',' )
                            : [];

            if ( parts.length > 1 )
            {
                return this.isInteger( parts[ 0 ] ) && this.isInteger( parts[ 1 ] );
            }

            return false;
        },

        /**
         * @method                              cream.hasValue
         * @description                         Check if the input is defined and has a value other
         *                                      than empty
         *
         * @param   {*}     input               Input to check
         *
         * @returns {boolean}
         *
        */
        hasValue            : function hasValue ( input )
        {
            return this.notUndefined( input ) && this.notNull( input ) && input !== '';
        },

        /**
         * @method                              cream.isFunction
         * @description                         Check if the input is a function
         *
         * @param   {*}     input               Input to check
         *
         * @returns {boolean}
         *
        */
        isFunction          : function isFunction ( input )
        {
            return typeof input === 'function' && input !== null;
        },

        /**
         * @method                              cream.isFloat
         * @description                         Check if the input is a float or a float string
         *                                      representation
         *
         * @param   {*}     input               Input to check
         * @param   {boolean} [strict]          If true force type to only float
         *
         * @returns {boolean}
         *
        */
        isFloat             : function isFloat ( input, strict )
        {
            var isNum = this.isNumber( input );

            if ( strict === true )
            {
                if ( isNum === true && ( input + '' ).indexOf( '.' ) > -1 )
                {
                    return true;
                }
                return false;
            }
            input = isNum === false
                        ? parseFloat( input )
                        : input;

            return input === +input && input !== ( input | 0 );
        },

        /**
         * @method                              cream.isBoolean
         * @description                         Check if the input is a boolean
         *
         * @param   {*}     input               Input to check
         *
         * @returns {boolean}
         *
        */
        isBoolean           : function isBoolean ( input )
        {
            return this.is( input, 'Boolean' );
        },

        /**
         * @method                              cream.isBooleanText
         * @description                         Check if the input is a boolean string representation
         *
         * @param   {*}       input             Input to check
         *
         * @returns {boolean}
         *
        */
        isBooleanText       : function isBooleanText ( input )
        {
            return input === 'false' || input === 'true';
        },

        /**
         * @method                              cream.isArray
         * @description                         Check if the input is an array
         *
         * @param   {*}       input             Input to check
         *
         * @returns {boolean}
         *
        */
        isArray             : function isArray ( input )
        {
            return typeof input === 'array' || this.is( input, 'Array' );
        },

        /**
         * @method                              cream.isObject
         * @description                         Check if the input is a object
         *
         * @param   {*}       input             Input to check
         *
         * @returns {boolean}
         *
        */
        isObject            : function isObject ( input )
        {
            return typeof input === 'object' && this.is( input, 'Object' );
        },

        /**
         * @method                              cream.isUndefined
         * @description                         Check if the input is undefined
         *
         * @param   {*}       input             Input to check
         *
         * @returns {boolean}
         *
        */
        isUndefined         : function isUndefined ( input )
        {
            return typeof input === 'undefined';
        },

        /**
         * @method                              cream.notUndefined
         * @description                         Check if the input is not undefined
         *
         * @param   {*}       input             Input to check
         *
         * @returns {boolean}
         *
        */
        notUndefined        : function notUndefined ( input )
        {
            return typeof input !== 'undefined';
        },

        /**
         * @method                              cream.isLower
         * @description                         Check if the input is a lowercase string
         *
         * @param   {*}       input             Input to check
         *
         * @returns {boolean}
         *
        */
        isLower             : function isLower ( input )
        {
            return _upperLower( input, 'lower' );
        },

        /**
         * @method                              cream.isUpper
         * @description                         Check if the input is a uppercase string
         *
         * @param   {*}       input             Input to check
         *
         * @returns {boolean}
         *
        */
        isUpper             : function isUpper ( input )
        {
            return _upperLower( input, 'upper' );
        },

        /**
         * @method                              cream.isEmail
         * @description                         Check if the input is a valid email string
         *
         * @param   {*}       input             Input to check
         *
         * @returns {boolean}
         *
        */
        isEmail             : function isEmail ( input )
        {
            var re;

            if ( this.isString( input ) )
            {
                re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                return this.testRegex( input, re );
            }

            return false;
        },

        /**
         * @method                                  cream.inLength
         * @description                             Check if the input string length is between min
         *                                          and max values.
         *                                          If only have min value, check if length is equal
         *                                          to min value.
         *                                          if msg is true and not inLength, return a string
         *                                          indicating the point where the condition failed:
         *                                          - 'min'   : length < min value
         *                                          - 'max'   : length > max value
         *                                          - 'exact' : min > length < max,and min != length
         *
         * @param   {string}            input       Input to check
         * @param   {(integer|float)}   min         Minimum range value
         * @param   {(integer|float)}   [max]       Maximun range value
         * @param   {boolean}           [msg]       If true and not inLength, return string, not boolean
         *
         * @returns {(boolean|string)}
         *
         */
        inLength            : function inLength ( input, min, max, msg )
        {

            if ( this.isString( input ) && this.isNumber( min ) )
            {
                if ( msg === true )
                {
                    if ( this.isNumber( max ) )
                    {
                        if ( input.length <= max !== true )
                        {
                            return 'max';
                        }

                        if ( input.length >= min !== true )
                        {
                            return 'min';
                        }

                        return true;
                    }

                    return  input.length === parseInt( min )
                                ? true
                                : 'exact';
                }

                return this.isNumber( max )
                    ? input.length >= min && input.length <= max
                    : input.length === parseInt( min );

            }

            return  false;
        },

        /**
         * @method                              cream.isURL
         * @description                         Check if the input is a valid URL string.
         *                                      Accept https|http, sub-domains,...
         *
         * @param   {*}       input             Input to check
         *
         * @returns {boolean}
         *
        */
        isURL               : function isURL ( input )
        {
            var re =  new RegExp( '^(https?:\\/\\/)?' + // protocol
                                 '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
                                 '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
                                 '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
                                 '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
                                 '(\\#[-a-z\\d_]*)?$', 'i' );

            return this.testRegex( input, re );

        },

        /**
         * @method                              cream.isIP
         * @description                         Check if the input is a valid IP string
         *
         * @param   {*}       input             Input to check
         *
         * @returns {boolean}
         *
        */
        isIP                : function isIP ( input )
        {
            var re = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
            return this.testRegex( input, re );
        },

        /**
         * @method                              cream.testRegex
         * @description                         Check if the input satisfies the regular expression
         *
         * @param   {*}       input             Input to check
         * @param   {RegExp}  regex             Regular expression against which to check
         *
         * @returns {boolean}
         *
         */
        testRegex           : function testRegex( input, regex )
        {
            return regex.test( input );
        },

        /**
         * @method                                  cream.inRange
         * @description                             Check if the input string is between min
         *                                          and max values.
         *                                          if msg is true and not inRange, return a string
         *                                          indicating the point where the condition failed:
         *                                          - 'min'   : length < min value
         *                                          - 'max'   : length > max value
         *                                          - 'invalid' : incorrect input
         *
         * @param   {(string|date|float|integer)}  input       Input to check
         * @param   {(string|date|float|integer)}  min         Minimum range value
         * @param   {(string|date|float|integer)}  max         Maximum range value
         * @param   {boolean}                      [msg]       If true and not inLength, return string, not boolean
         *
         * @returns {(boolean|string)}
         *
        */
        inRange             : function inRange( input, min, max, msg )
        {
            //date,number,string
            var checkDates  = this.isDate( input ) && this.isDate( min ) && this.isDate( max ),
                checkNumber = this.isNumber( input ) && this.isNumber( min ) && this.isNumber( max ),
                checkString = this.isString( input ) && this.isString( min ) && this.isString( max );

            if (  checkDates || checkNumber || checkString )
            {
                if ( msg === true )
                {
                    if ( input <= max !== true )
                    {
                        return 'max';
                    }

                    if ( input  >= min !== true )
                    {
                        return 'min';
                    }

                    return true;
                }

                return input >= min && input <= max;
            }

            return msg === true
                        ? 'invalid'
                        : false;
        },

        /**
         * @method                              cream.equalTo
         * @description                         Check if the input is equal to compare param, does
         *                                      not consider var type
         *
         * @param   {(date|number|string)}  input       Input to check
         * @param   {(date|number|string)}  compare     Value against which to compare
         *
         * @returns {boolean}
         *
        */
        equalTo             : function equalTo ( input, compare )
        {
            return input + '' === compare + '';
        },

        /**
         * @method                              cream.identicalTo
         * @description                         Check if the input is identical, checking value
         *                                      and type.
         *                                      Does not work withObjects, arrays and functions
         *                                      with content.
         *
         * @param   {(date|number|string)}  input       Input to check
         * @param   {(date|number|string)}  compare     Value against which to compare
         *
         * @returns {boolean}
         *
        */
        identicalTo         : function identicalTo ( input, compare )
        {
            return input === compare;
        },

        /**
         * @method                              cream.isDate
         * @description                         Check if the input is a date object
         *
         * @param   {*}       input             Input to check
         *
         * @returns {boolean}
         *
        */
        isDate              : function isDate ( input )
        {
            return this.is( input, 'Date' );
        },

        /**
         * @method                              cream.isDateText
         * @description                         Check if the input is a string and a date string
         *                                      (review cream.checkFormatDate documentation)
         *
         * @param   {*}       input             Input to check
         *
         * @returns {boolean}
         *
        */
        isDateText          : function isDateText ( input )
        {
            return this.isString( input ) && date.checkFormatDate( input );
        },

        /**
         * @method                              cream.isTimeText
         * @description                         Check if the input is a string and a time string
         *                                      (review cream.checkFormatTime documentation)
         *
         * @param   {*}       input             Input to check
         *
         * @returns {boolean}
         *
        */
        isTimeText          : function isTimeText ( input )
        {
            return this.isString( input ) && date.checkFormatTime( input );
        },

        /**
         * @method                              cream.isDateTimeText
         * @description                         Check if the input is a string and a date time string
         *                                      (review cream.checkFormatDateTime documentation)
         *
         * @param   {*}       input             Input to check
         *
         * @returns {boolean}
         *
        */
        isDateTimeText      : function isDateTimeText ( input )
        {
            return date.checkFormatDateTime( input );
        },

        /**
         * @method                              cream.isValidDateTimeText
         * @description                         Check if the input is a string and a valid date time
         *                                      string
         *                                      (review cream.checkValidFormatDateTime documentation)
         *
         * @param   {*}       input             Input to check
         *
         * @returns {boolean}
         *
        */
        isValidDateTimeText : function isValidDateTimeText ( input )
        {
            return date.checkValidFormatDateTime( input );
        },

        /**
         * @method                              cream.isValidDate
         * @description                         Check if the input is a valid date object
         *
         * @param   {*}       input             Input to check
         *
         * @returns {boolean}
         *
        */
        isValidDate         : function isValidDate ( input )
        {
            if ( this.isDate( input ) )
            {
                return !isNaN( input.getMonth() );
            }

            return false;
        },

        /**
         * @method                              cream.is
         * @description                         Check if the input is of any passed type
         *                                      (Date, Object, Array, ...)
         *
         * @param   {*}       input             Input to check
         * @param   {string}  type              Type name to compare
         *
         * @returns {boolean}
         *
        */
        is                  : function is ( input, type )
        {
            var clas = Object.prototype.toString.call( input ).slice( 8, -1 );
            return input !== undefined && input !== null && clas.toLowerCase() === type.toLowerCase();
        },

        /**
         * @method                              cream.lt
         * @description                         Check if the input is less than compare param
         *
         * @param   {*}       input             Input to check
         * @param   {string}  compare           Param to compare against
         *
         * @returns {boolean}
         *
        */
        lt                  : function lt ( input, compare )
        {
            return input < compare;
        },

        /**
         * @method                              cream.gt
         * @description                         Check if the input is greater than compare param
         *
         * @param   {*}       input             Input to check
         * @param   {string}  compare           Param to compare against
         *
         * @returns {boolean}
         *
         */
        gt                  : function gt ( input, compare )
        {
            return input > compare;
        },

        /**
         * @method                              cream.gte
         * @description                         Check if the input is greater or equal than compare
         *                                      param
         *
         * @param   {*}       input             Input to check
         * @param   {string}  compare           Param to compare against
         *
         * @returns {boolean}
         *
        */
        gte                 : function gte ( input, compare )
        {
            return input >= compare ;
        },

        /**
         * @method                              cream.lte
         * @description                         Check if the input is less or equal than compare
         *                                      param
         *
         * @param   {*}       input             Input to check
         * @param   {string}  compare           Param to compare against
         *
         * @returns {boolean}
         *
        */
        lte                 : function lte ( input, compare )
        {
            return input <= compare ;
        },

        /**
         * @method                                  cream.inCount
         * @description                             Check if the input array items number is between
         *                                          min and max values.
         *                                          If only have min value, check if length is equal
         *                                          to min value.
         *                                          if msg is true and not inCount, return a string
         *                                          indicating the point where the condition failed:
         *                                          - 'min'   : length < min value
         *                                          - 'max'   : length > max value
         *                                          - 'exact' : min > length < max,and min != length
         *
         * @param   {array}             input       Input to check
         * @param   {(integer|float)}   min         Minimum range value
         * @param   {(integer|float)}   [max]       Maximum range value
         * @param   {boolean}           [msg]       If true and not inCount, return string, not boolean
         *
         * @returns {(boolean|string)}
         *
         */
        inCount             : function inCount ( input, min, max, msg )
        {
            var length;

            if ( this.isArray( input ) && this.isInteger( min ) )
            {
                length = input.length;

                if ( msg === true )
                {
                    if ( this.isInteger( max ) )
                    {
                        if ( length <= max  !== true )
                        {
                            return 'max';
                        }

                        if ( length >= min !== true )
                        {
                            return 'min';
                        }

                        return true;
                    }

                    return length === min
                                ? true
                                : 'exact';
                }
                return this.isInteger( max )
                            ? length >= min && length <= max
                            : length === min;
            }
            return  false;
        },

        /**
         * @method                                          cream.inChoices
         * @description                                     Check if the input is in the choices
         *                                                  array
         *
         * @param   {(string|float|integer)}    input       Input to check
         * @param   {array}                     choices     Array on which search
         *
         * @returns {boolean}
         *
        */
        inChoices           : function inChoices ( input, choices )
        {
            if ( this.notUndefined( input ) && this.isArray( choices ) )
            {
                return choices.indexOf( input ) > -1;
            }

            return  false;
        },

        /**
         * @method                          cream.isIban
         * @description                     Check if the input is iban code
         *                                  *** not tested ***
         *
         * @param   {string}    input       Input to check
         *
         * @returns {boolean}
         *
        */
        isIban              : function isIban ( value )
        {

            // remove spaces and to upper case
            var iban            = value.replace( / /g, "" ).toUpperCase(),
                ibancheckdigits = '',
                leadingZeroes   = true,
                cRest           = '',
                cOperator       = '',
                countrycode, ibancheck, charAt, cChar, bbanpattern, bbancountrypatterns, ibanregexp, i, p;
            if ( !/^([a-zA-Z0-9]{4} ){2,8}[a-zA-Z0-9]{1,4}|[a-zA-Z0-9]{12,34}$/.test( iban ) )
            {
                return false;
            }
            // check the country code and find the country specific format
            countrycode = iban.substring( 0, 2 );
            bbancountrypatterns = {
                "AL": "\\d{8}[\\dA-Z]{16}",
                "AD": "\\d{8}[\\dA-Z]{12}",
                "AT": "\\d{16}",
                "AZ": "[\\dA-Z]{4}\\d{20}",
                "BE": "\\d{12}",
                "BH": "[A-Z]{4}[\\dA-Z]{14}",
                "BA": "\\d{16}",
                "BR": "\\d{23}[A-Z][\\dA-Z]",
                "BG": "[A-Z]{4}\\d{6}[\\dA-Z]{8}",
                "CR": "\\d{17}",
                "HR": "\\d{17}",
                "CY": "\\d{8}[\\dA-Z]{16}",
                "CZ": "\\d{20}",
                "DK": "\\d{14}",
                "DO": "[A-Z]{4}\\d{20}",
                "EE": "\\d{16}",
                "FO": "\\d{14}",
                "FI": "\\d{14}",
                "FR": "\\d{10}[\\dA-Z]{11}\\d{2}",
                "GE": "[\\dA-Z]{2}\\d{16}",
                "DE": "\\d{18}",
                "GI": "[A-Z]{4}[\\dA-Z]{15}",
                "GR": "\\d{7}[\\dA-Z]{16}",
                "GL": "\\d{14}",
                "GT": "[\\dA-Z]{4}[\\dA-Z]{20}",
                "HU": "\\d{24}",
                "IS": "\\d{22}",
                "IE": "[\\dA-Z]{4}\\d{14}",
                "IL": "\\d{19}",
                "IT": "[A-Z]\\d{10}[\\dA-Z]{12}",
                "KZ": "\\d{3}[\\dA-Z]{13}",
                "KW": "[A-Z]{4}[\\dA-Z]{22}",
                "LV": "[A-Z]{4}[\\dA-Z]{13}",
                "LB": "\\d{4}[\\dA-Z]{20}",
                "LI": "\\d{5}[\\dA-Z]{12}",
                "LT": "\\d{16}",
                "LU": "\\d{3}[\\dA-Z]{13}",
                "MK": "\\d{3}[\\dA-Z]{10}\\d{2}",
                "MT": "[A-Z]{4}\\d{5}[\\dA-Z]{18}",
                "MR": "\\d{23}",
                "MU": "[A-Z]{4}\\d{19}[A-Z]{3}",
                "MC": "\\d{10}[\\dA-Z]{11}\\d{2}",
                "MD": "[\\dA-Z]{2}\\d{18}",
                "ME": "\\d{18}",
                "NL": "[A-Z]{4}\\d{10}",
                "NO": "\\d{11}",
                "PK": "[\\dA-Z]{4}\\d{16}",
                "PS": "[\\dA-Z]{4}\\d{21}",
                "PL": "\\d{24}",
                "PT": "\\d{21}",
                "RO": "[A-Z]{4}[\\dA-Z]{16}",
                "SM": "[A-Z]\\d{10}[\\dA-Z]{12}",
                "SA": "\\d{2}[\\dA-Z]{18}",
                "RS": "\\d{18}",
                "SK": "\\d{20}",
                "SI": "\\d{15}",
                "ES": "\\d{20}",
                "SE": "\\d{20}",
                "CH": "\\d{5}[\\dA-Z]{12}",
                "TN": "\\d{20}",
                "TR": "\\d{5}[\\dA-Z]{17}",
                "AE": "\\d{3}\\d{16}",
                "GB": "[A-Z]{4}\\d{14}",
                "VG": "[\\dA-Z]{4}\\d{16}"
            };
            bbanpattern = bbancountrypatterns[countrycode];
            // As new countries will start using IBAN in the
            // future, we only check if the countrycode is known.
            // This prevents false negatives, while almost all
            // false positives introduced by this, will be caught
            // by the checksum validation below anyway.
            // Strict checking should return FALSE for unknown
            // countries.
            if ( typeof bbanpattern !== "undefined" )
            {
                ibanregexp = new RegExp( "^[A-Z]{2}\\d{2}" + bbanpattern + "$", "" );
                if ( !ibanregexp.test( iban ) )
                {
                    return false; // invalid country specific format
                }
            }
            // now check the checksum, first convert to digits
            ibancheck = iban.substring( 4, iban.length ) + iban.substring( 0, 4 );
            for ( i = 0; i < ibancheck.length; i++ )
            {
                charAt = ibancheck.charAt( i );
                if ( charAt !== "0" )
                {
                    leadingZeroes = false;
                }
                if ( !leadingZeroes )
                {
                    ibancheckdigits += "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf( charAt );
                }
            }
            // calculate the result of: ibancheckdigits % 97
            for ( p = 0; p < ibancheckdigits.length; p++ )
            {
                cChar = ibancheckdigits.charAt( p );
                cOperator = "" + cRest + "" + cChar;
                cRest = cOperator % 97;
            }
            return cRest === 1;

        },

        /**
         * @method                          cream.isIsban
         * @description                     Check if the input is isban code
         *                                  *** not tested ***
         *
         * @param   {string}    input       Input to check
         *
         * @returns {boolean}
         *
         */
        isIsbn              : function isIsbn ( isbn )
        {
            var isbn = isbn.replace(/[^\dX]/gi, '');
            if(isbn.length == 10) {
                var chars = isbn.split('');
                if(chars[9].toUpperCase() == 'X') {
                    chars[9] = 10;
                }
                var sum = 0;
                for(var i = 0; i < chars.length; i++) {
                    sum += ((10-i) * parseInt(chars[i]));
                }
                return (sum % 11 == 0);
            } else if(isbn.length == 13) {
                var chars = isbn.split('');
                var sum = 0;
                for (var i = 0; i < chars.length; i++) {
                    if(i % 2 == 0) {
                        sum += parseInt(chars[i]);
                    } else {
                        sum += parseInt(chars[i]) * 3;
                    }
                }
                return (sum % 10 == 0);
            } else {
                return false;
            }

        },

        /**
         * @method                          cream.isIban
         * @description                     Check if the input is issn code
         *                                  *** not tested ***
         *
         * @param   {string}    input       Input to check
         *
         * @returns {boolean}
         *
        */
        isIssn              : function isIssn ( issn )
        {
            var issn = issn.replace(/[^\dX]/gi, '');
            if(issn.length != 8){
                return false;
            }
            var chars = issn.split('');
            if(chars[7].toUpperCase() == 'X'){
                chars[7] = 10;
            }
            var sum = 0;
            for (var i = 0; i < chars.length; i++) {
                sum += ((8-i) * parseInt(chars[i]));
            };
            return ((sum % 11) == 0);

        },

        /**
         * @method                          cream.isCreditCard
         * @description                     Check if the input is credit card number
         *                                  Valid Types: mastercard, visa, amex, dinersclub,
         *                                  enroute, discover, jcb, unknown
         *
         *                                  *** not tested ***
         *
         * @param   {string}    input       Input to check
         * @param   {object}    param       Object with type to check in key and true value
         *
         * @returns {boolean}
         *
         */
        isCreditCard        : function isCreditCard( value, param )
        {
            if (/[^0-9\-]+/.test(value)) {
                return false;
            }
            value = value.replace(/\D/g, "");
            var validTypes = 0x0000;
            if (param.mastercard) {
                validTypes |= 0x0001;
            }
            if (param.visa) {
                validTypes |= 0x0002;
            }
            if (param.amex) {
                validTypes |= 0x0004;
            }
            if (param.dinersclub) {
                validTypes |= 0x0008;
            }
            if (param.enroute) {
                validTypes |= 0x0010;
            }
            if (param.discover) {
                validTypes |= 0x0020;
            }
            if (param.jcb) {
                validTypes |= 0x0040;
            }
            if (param.unknown) {
                validTypes |= 0x0080;
            }
            if (param.all) {
                validTypes = 0x0001 | 0x0002 | 0x0004 | 0x0008 | 0x0010 | 0x0020 | 0x0040 | 0x0080;
            }
            if (validTypes & 0x0001 && /^(5[12345])/.test(value)) { //mastercard
                return value.length === 16;
            }
            if (validTypes & 0x0002 && /^(4)/.test(value)) { //visa
                return value.length === 16;
            }
            if (validTypes & 0x0004 && /^(3[47])/.test(value)) { //amex
                return value.length === 15;
            }
            if (validTypes & 0x0008 && /^(3(0[012345]|[68]))/.test(value)) { //dinersclub
                return value.length === 14;
            }
            if (validTypes & 0x0010 && /^(2(014|149))/.test(value)) { //enroute
                return value.length === 15;
            }
            if (validTypes & 0x0020 && /^(6011)/.test(value)) { //discover
                return value.length === 16;
            }
            if (validTypes & 0x0040 && /^(3)/.test(value)) { //jcb
                return value.length === 16;
            }
            if (validTypes & 0x0040 && /^(2131|1800)/.test(value)) { //jcb
                return value.length === 15;
            }
            if (validTypes & 0x0080) { //unknown
                return true;
            }
            return false;
        }


    };

    return assertions;

} );
