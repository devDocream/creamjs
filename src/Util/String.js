define( [
],
function stringUtil ( )
{
    var strings = {


            /**
             * @method                          cream.capitalize
             * @description                     Capitalize a string
             *
             * @param   {string} str            String to capitalize
             *
             * @returns {string}
             *
            */
            capitalize      : function capitalize ( str )
            {
                if ( typeof str === 'string' )
                {
                    return str[0].toUpperCase() + str.substring( 1 );
                }

                return '';
            },

            /**
             * @method                          cream.escape
             * @description                     Escape a string
             *
             * @param   {string} str            String to escape
             *
             * @returns {string}
             *
            */
            escape          : function escapeRegExp ( str )
            {
                return str.replace( /([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1" );
            },

            /**
             * @method                          cream.replace
             * @description                     Replace all coincidences in a string with replacement
             *                                  string
             *
             * @param   {string} str            String where the substitution is made
             * @param   {string} search         String to replace in original string
             * @param   {string} replacement    The search string is replaced whit this
             *
             * @returns {string}
             *
            */
            replace         : function replace( str, search, replacement )
            {
                if ( typeof str === 'string' && typeof search === 'string'
                    && typeof replacement === 'string' )
                {
                    return str.replace( new RegExp( this.escape( search ), 'g' ), replacement );
                }

                return '';
            },

            /**
             * @method                              cream.underscorize
             * @description                         Replace all separator coincidences with '_'
             *
             * @param   {string} str                String to transform
             * @param   {string} [separator='.']    String to replace in original string
             *
             * @returns {string}                    Lowered and underscorized
             *
            */
            underscorize    : function underscorize ( str, separator )
            {
                var search = separator !== undefined && typeof separator === 'string'
                        ? separator
                        : '.',
                    replaced;

                if ( typeof str === 'string' )
                {
                    replaced = this.replace( str, search, '_' );

                    return replaced.toLowerCase();
                }

                return '';
            },

            /**
             * @method                              cream.dasherize
             * @description                         Replace all separator coincidences with '-'
             *
             * @param   {string} str                String to transform
             * @param   {string} [separator='.']    String to replace in original string
             *
             * @returns {string}                    Lowered and dasherized
             *
            */
            dasherize       : function dasherize ( str, separator )
            {
                var search = separator !== undefined && typeof separator === 'string'
                        ? separator
                        : '.',
                    replaced;

                if ( typeof str === 'string' )
                {
                    replaced = this.replace( str, search, '-' );

                    return replaced.toLowerCase();
                }

                return '';
            }

        };

    return strings;
}
);
