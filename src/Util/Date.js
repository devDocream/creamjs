define( [

],
function dateUtil ( )
{
    var dates = {

        /**
         * @method                              cream.checkFormatDate
         * @description                         Check if a string has a valid date format
         *                                      Valid Formats:
         *                                          - 'yyyy-mm-dd' (js standard) or 'yyyy/mm/dd'
         *                                          - 'dd-mm-yyyy' or 'dd/mm/yyyy'
         *
         * @param   {string} input              String to check the format date
         * @param   {string} [separator]        'standard' to force check valid standard format
         *
         * @returns {boolean}
         *
        */
        checkFormatDate             : function checkFormatDate ( input , separator )
        {
            if( input !== '' )
            {
                var minYear     = 1902,
                    maxYear     = ( new Date() ).getFullYear(),
                    errorMsg    = "",
                    aux         = [],
                    re          = ( separator === 'standard' || input.indexOf( '-' ) > -1 )
                        ? /^(\d{4})-(\d{1,2})-(\d{1,2})$/
                        : /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/,
                    regs        = input.match( re );

                if ( regs === null )
                {

                    re = ( separator == 'standard' || input.indexOf( '-' ) > -1 )
                        ? /^(\d{1,2})-(\d{1,2})-(\d{4})$/
                        : /^(\d{4})\/(\d{1,2})\/(\d{1,2})$/;

                    regs =  input.match( re );
                }

                if ( separator === 'standard' && regs &&
                   ( regs[1].length !== 4 || regs[2].length !== 2 || regs[3].length !== 2 ) )
                {
                    return false;
                }

                if ( regs && regs[1].length == 4 )
                {
                    if (regs[3].length < 2 ||  regs[2].length < 2  )
                    {
                        return false;
                    }

                    aux[ 1 ]= parseInt( regs[ 3 ] );
                    aux[ 2 ]= parseInt( regs[ 2 ] );
                    aux[ 3 ]= parseInt( regs[ 1 ] );

                }
                else if ( regs && regs[3].length == 4 )
                {
                    if  (regs[1].length < 2 ||  regs[2].length < 2 )
                    {
                        return false;
                    }

                    aux[ 1 ]= parseInt( regs[ 1 ] );
                    aux[ 2 ]= parseInt( regs[ 2 ] );
                    aux[ 3 ]= parseInt( regs[ 3 ] );

                }
                else
                {
                    return false;
                }


                if( regs )
                {
                    if( aux[1] < 1 || aux[1] > 31 )
                    {
                        errorMsg = "Invalid value for day: " + aux[1];
                    }
                    else if( aux[2] < 1 || aux[2] > 12 )
                    {
                        errorMsg = "Invalid value for month: " + aux[2];
                    }
                    else if( aux[3] < minYear || aux[3] > maxYear )
                    {
                        errorMsg = "Invalid value for year: " + aux[3] +
                                               " - must be between " + minYear + " and " + maxYear;
                    }
                }
                else {
                    errorMsg = "Invalid date format: " + input;
                }

                return ( errorMsg == "" );
            }

            return false;
        },

        /**
         * @method                              cream.checkValidFormatDate
         * @description                         Check if a string has a valid date format.
         *                                      Force js standard => 'yyyy-mm-dd'
         *
         * @param   {string} input              String to check the format date
         *
         * @returns {boolean}
         *
        */
        checkValidFormatDate        : function( input )
        {
            return this.checkFormatDate( input , 'standard')
        },

        /**
         * @method                              cream.checkValidFormatDateTime
         * @description                         Check if a string has a valid date time format.
         *                                      Force js standard => 'yyyy-mm-dd h[h]:mm[:ss] [pm|am]'
         *
         * @param   {string} input              String to check the format date time
         *
         * @returns {boolean}
         *
        */
        checkValidFormatDateTime    : function( input )
        {
            return this.checkFormatDateTime( input , 'standard')
        },

        /**
         * @method                              cream.checkFormatTime
         * @description                         Check if a string has a valid time format.
         *                                      Valid formats:
         *                                          - 'h[h]:mm[:ss] [pm|am]' (h <= 12 )
         *                                          - 'h[h]:mm[:ss]'
         *
         * @param   {string} input              String to check the format time
         *
         * @returns {boolean}
         *
        */
        checkFormatTime             : function ( input )
        {
            var errorMsg    = "",
                re          = /^(\d{1,2}):(\d{2})(:\d{2})?(\s[ap]m)?$/,
                regs;

            if( input !== '' )
            {
                if( regs = input.match( re ) )
                {
                    regs[ 3 ] = ( regs[3] )
                        ? regs[3].replace( ':','' )
                        :  0 ;

                    if( regs[4] )
                    {
                        // 12-hour time format with am/pm
                        if( regs[1] < 1 || regs[1] > 12 )
                        {
                            errorMsg = "Invalid value for hours: " + regs[1];
                        }
                    }
                    else
                    {
                        // 24-hour time format
                        if( regs[1] > 23 )
                        {
                            errorMsg = "Invalid value for hours: " + regs[1];
                        }
                    }
                    if( !errorMsg && regs[2] > 59 )
                    {
                        errorMsg = "Invalid value for minutes: " + regs[2];
                    }

                    if( !errorMsg && regs[3] && regs[3] > 59 )
                    {
                        errorMsg = "Invalid value for minutes: " + regs[2];
                    }

                }
                else
                {
                    errorMsg = "Invalid time format: " + input;
                }

                return ( errorMsg == "" );
            }

            return false;
        },

        /**
         * @method                              cream.checkFormatDateTime
         * @description                         Check if a string has a valid date time format.
         *                                      Valid formats:
         *                                          - 'yyyy-mm-dd|yyyy/mm/dd|dd-mm-yyyy|dd/mm/yyyy h[h]:mm[:ss] [pm|am]' (h <= 12 )
         *                                          - 'yyyy-mm-dd|yyyy/mm/dd|dd-mm-yyyy|dd/mm/yyyy h[h]:mm[:ss]'
         *
         * @param   {string} input              String to check the format date time
         *
         * @returns {boolean}
         *
        */
        checkFormatDateTime             : function( str , standard )
        {
            var parts      = str.split(' '),
                dat        = parts[ 0 ],
                tim        = ( parts.length > 1 ) ? parts[1] : '',
                validDate  = this.checkFormatDate( dat, standard ),
                validTime  = this.checkFormatTime( tim );

            if ( tim == '')
            {
                return ( validDate === true );
            }

            return ( validDate === true && validTime === true);
        }


    };

    return dates;
}
);
